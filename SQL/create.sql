/* databese 作成 */
create database FGO_DB DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

/* 鯖使用宣言 */
use FGO_DB;


/* テーブル作成 - servant */
create table servant (
id serial primary key unique not null,
name varchar(255) not null,
class varchar(255) not null,
hp int not null,
atk int not null,
rare int not null,
cost int,
np_id int unique,
mnp_id int unique,
skill1_id int,
skill2_id int,
skill3_id int,
mskill1_id int,
mskill2_id int,
mskill3_id int,
card varchar(255) not null,
b_hit int,
a_hit int,
q_hit int,
ex_hit int,
n_hit int,
np_eff float,
np_damage float,
star float,
star_atsumare float,
illustrator varchar(255),
cv varchar(255),
gacha varchar(255),
tenchijin varchar(255),
sex varchar(255),
chara varchar(255),
c_skill_1 int,
c_skill_2 int,
c_skill_3 int,
c_skill_4 int,
c_skill_5 int
);



/* テーブル作成 - servant_skill */
create table servant_skill (
id serial primary key auto_increment unique,
name varchar(255) not null,
ct int not null
);



/* テーブル作成 - servant_skill_effect */
create table servant_skill_effect (
id serial primary key auto_increment unique,
skill_id int not null,
effect_text varchar(255) not null,
effect_genre int not null,
turn int not null,
bord int not null,
effect_para_1 float not null,
effect_para_2 float not null,
effect_para_3 float not null,
effect_para_4 float not null,
effect_para_5 float not null,
effect_para_6 float not null,
effect_para_7 float not null,
effect_para_8 float not null,
effect_para_9 float not null,
effect_para_10 float not null
);



/* テーブル作成 - class_skill */
create table class_skill (
id serial primary key unique auto_increment not null,
name varchar(255) not null
);



/* テーブル作成 - class_skill_effect */
create table class_skill_effect (
id serial primary key unique auto_increment not null,
class_skill_id int not null,
effect_text varchar(255) not null,
effect_genre int not null,
bord int not null,
effect_para float not null
);



/* テーブル作成 - noblephantasm */
create table noblephantasm (
id serial primary key unique auto_increment not null,
name varchar(255) not null,
ruby varchar(255) not null,
rank varchar(255) not null,
type varchar(255) not null
);



/* テーブル作成 - np_color */
create table np_color (
id serial primary key auto_increment unique not null,
np_id int not null,
color varchar(255) not null,
np_bairitsu_1 float not null,
np_bairitsu_2 float not null,
np_bairitsu_3 float not null,
np_bairitsu_4 float not null,
np_bairitsu_5 float not null
);



/* テーブル作成 - np_effect */
create table np_effect (
id serial primary key auto_increment unique not null,
np_id int not null,
effect_text varchar(255) not null,
effect_genre int not null,
turn int not null,
bord int not null,
effect_para1 float not null,
effect_para2 float not null,
effect_para3 float not null,
effect_para4 float not null,
effect_para5 float not null
);



/* テーブル作成 - target */
create table target (
id serial primary key auto_increment unique not null,
targeting int not null,
target_chara varchar(255)
);



/* テーブル作成 - skill_relation */
create table skill_relation (
id serial primary key auto_increment unique not null,
target_id int not null,
skill_effect_id int not null
);



/* テーブル作成 - c_skill_relation */
create table c_skill_relation (
id serial primary key auto_increment unique not null,
target_id int not null,
c_skill_effect_id int not null
);



/* テーブル作成 - np_relation */
create table np_relation (
id serial primary key auto_increment unique not null,
target_id int not null,
np_effect_id int not null
);

/* databese 作成 */
create database FGO_DB DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

/* 鯖使用宣言 */
use FGO_DB;



/* テーブル作成 - servant */
create table servant (
id serial primary key unique not null,
name varchar(255) not null,
class_id int(255) not null,
rare_id int not null,
hp int not null,
atk int not null,
card varchar(255) not null,
b_hit int,
a_hit int,
q_hit int,
ex_hit int,
n_hit int,
np_eff float,
np_damage float,
star float,
star_atsumare float,
illustrator varchar(255),
cv varchar(255),
gacha varchar(255),
tenchijin varchar(255),
sex varchar(255)
);


/* テーブル作成 - class */
create table class (
id serial primary key auto_increment unique,
name varchar(255) not null,
in_value float not null
);


/* テーブル作成 - rare */
create table rare (
id serial primary key auto_increment unique,
rare int not null,
cost int not null,
gacha varchar(255) not null
);


/* テーブル作成 - state_relation */
create table state_relation (
id serial primary key auto_increment unique,
servant_id int not null,
state_id int not null
);


/* テーブル作成 - c_skill_relation */
create table c_skill_relation (
id serial primary key auto_increment unique,
servant_id int not null,
c_skill_id int not null
);


/* テーブル作成 - skill_relation */
create table skill_relation (
id serial primary key auto_increment unique,
servant_id int not null,
skill_id int not null
);


/* テーブル作成 - np_relation */
create table np_relation (
id serial primary key auto_increment unique,
servant_id int not null,
np_id int not null
);


/* テーブル作成 - state */
create table state (
id serial primary key auto_increment unique,
state_text varchar(255) not null
);


/* テーブル作成 - target */
create table target (
id serial primary key auto_increment unique,
state_id int not null
);


/* テーブル作成 - class_skill */
create table class_skill (
id serial primary key auto_increment unique,
name varchar(255) not null,
img_id varchar(255) not null,
junban int not null
);


/* テーブル作成 - class_skill_effect */
create table class_skill_effect (
id serial primary key auto_increment unique,
class_skill_id int not null,
effect_text varchar(255) not null,
effect_genre int,
bord int,
effect_para float,
target_id int
);


/* テーブル作成 - servant_skill */
create table servant_skill (
id serial primary key auto_increment unique,
name varchar(255) not null,
img_id varchar(255) not null,
junban int not null
);


/* テーブル作成 - servant_skill_effect */
create table servant_skill_effect (
id serial primary key auto_increment unique,
skill_id int not null,
effect_text varchar(255) not null,
effect_genre int not null,
turn int not null,
bord int not null,
fore int not null,
target_id int not null,
effect_para_1 float,
effect_para_2 float,
effect_para_3 float,
effect_para_4 float,
effect_para_5 float,
effect_para_6 float,
effect_para_7 float,
effect_para_8 float,
effect_para_9 float,
effect_para_10 float not null
);


/* テーブル作成 - noblephantasm */
create table noblephantasm (
id serial primary key auto_increment unique,
name varchar(255) not null,
ruby varchar(255) not null,
type varchar(255) not null,
damage_text varchar(255) not null,
junban int not null,
bairitsu_id int not null
);


/* テーブル作成 - np_effect */
create table np_effect (
id serial primary key auto_increment unique,
np_id int not null,
effect_text varchar(255) not null,
effect_genre int not null,
turn int,
bord int not null,
fore int not null,
beaf int not null,
fast int not null,
taget_id int not null,
effect_para_1 float not null,
effect_para_2 float not null,
effect_para_3 float not null,
effect_para_4 float not null,
effect_para_5 float not null
);


/* テーブル作成 - np_bairitsu */
create table np_bairitsu (
id serial primary key auto_increment unique,
color_id int not null,
bairtsu1 float not null,
bairtsu2 float not null,
bairtsu3 float not null,
bairtsu4 float not null,
bairtsu5 float not null
);


/* テーブル作成 - color */
create table color (
id serial primary key auto_increment unique,
color varchar(255) not null,
in_value float not null
);


/* テーブル作成 - s_tenchijin */
create table s_tenchijin (
id serial primary key auto_increment unique,
name varchar(255) not null
);


/* テーブル作成 - s_sex */
create table s_sex (
id serial primary key auto_increment unique,
name varchar(255) not null
);


/* テーブル作成 - s_seikaku */
create table s_seikaku (
id serial primary key auto_increment unique,
name varchar(255) not null
);


/* テーブル作成 - s_houshin */
create table s_houshin (
id serial primary key auto_increment unique,
name varchar(255) not null
);


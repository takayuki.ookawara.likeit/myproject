-- MySQL dump 10.13  Distrib 5.7.26, for Win64 (x86_64)
--
-- Host: localhost    Database: fgo_db
-- ------------------------------------------------------
-- Server version	5.7.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `c_skill_relation`
--

DROP TABLE IF EXISTS `c_skill_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `c_skill_relation` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `servant_id` int(11) NOT NULL,
  `c_skill_id` int(11) NOT NULL,
  `junban` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `c_skill_relation`
--

LOCK TABLES `c_skill_relation` WRITE;
/*!40000 ALTER TABLE `c_skill_relation` DISABLE KEYS */;
INSERT INTO `c_skill_relation` VALUES (1,1,118,1),(2,1,7,2),(3,2,118,1),(4,2,10,2),(5,3,116,1),(6,4,116,1),(7,4,7,2),(8,5,116,1),(9,5,10,2),(10,6,10,1),(11,7,113,1),(12,7,10,2),(13,7,95,3),(14,8,116,1),(15,8,12,2),(16,8,101,3),(17,9,116,1),(18,9,10,2),(19,9,45,3),(20,10,113,1),(21,10,10,2),(22,11,110,1),(23,11,55,2),(24,12,108,1),(25,12,58,2),(26,12,101,3),(27,13,110,1),(28,13,57,2),(29,14,110,1),(30,14,57,2),(31,15,118,1),(32,15,58,2),(33,15,125,3),(34,16,113,1),(35,16,52,2),(36,17,113,1),(37,17,101,2),(38,18,118,1),(39,18,25,2),(40,19,114,1),(41,20,113,1),(42,20,101,2),(43,21,113,1),(44,22,116,1),(45,23,116,1),(46,23,13,2),(47,23,58,3),(48,23,92,4),(49,24,118,1),(50,24,10,2),(51,25,108,1),(52,26,110,1),(53,26,12,2),(54,27,113,1),(55,27,13,2),(56,28,110,1),(57,28,13,2),(58,28,93,3),(59,29,113,1),(60,29,13,2),(61,30,118,1),(62,30,14,2),(63,30,98,3),(64,31,27,1),(65,31,87,2);
/*!40000 ALTER TABLE `c_skill_relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `class_skill`
--

DROP TABLE IF EXISTS `class_skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_skill` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `img_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=177 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class_skill`
--

LOCK TABLES `class_skill` WRITE;
/*!40000 ALTER TABLE `class_skill` DISABLE KEYS */;
INSERT INTO `class_skill` VALUES (1,'騎乗E-','riding'),(2,'騎乗E','riding'),(3,'騎乗E+','riding'),(4,'騎乗D','riding'),(5,'騎乗D+','riding'),(6,'騎乗C-','riding'),(7,'騎乗C','riding'),(8,'騎乗C+','riding'),(9,'騎乗B-','riding'),(10,'騎乗B','riding'),(11,'騎乗B+','riding'),(12,'騎乗A','riding'),(13,'騎乗A+','riding'),(14,'騎乗A++','riding'),(15,'騎乗EX','riding'),(16,'陣地作成E-','band'),(17,'陣地作成E','band'),(18,'陣地作成E+','band'),(19,'陣地作成D','band'),(20,'陣地作成D+','band'),(21,'陣地作成C-','band'),(22,'陣地作成C','band'),(23,'陣地作成C+','band'),(24,'陣地作成B-','band'),(25,'陣地作成B','band'),(26,'陣地作成B+','band'),(27,'陣地作成A','band'),(28,'陣地作成A+','band'),(29,'陣地作成A++','band'),(30,'陣地作成EX','band'),(31,'狂化E-','berserk'),(32,'狂化E','berserk'),(33,'狂化E+','berserk'),(34,'狂化D','berserk'),(35,'狂化D+','berserk'),(36,'狂化C-','berserk'),(37,'狂化C','berserk'),(38,'狂化C+','berserk'),(39,'狂化B-','berserk'),(40,'狂化B','berserk'),(41,'狂化B+','berserk'),(42,'狂化A','berserk'),(43,'狂化A+','berserk'),(44,'狂化A++','berserk'),(45,'狂化EX','berserk'),(46,'単独行動E-','alone_move'),(47,'単独行動E','alone_move'),(48,'単独行動E+','alone_move'),(49,'単独行動D','alone_move'),(50,'単独行動D+','alone_move'),(51,'単独行動C-','alone_move'),(52,'単独行動C','alone_move'),(53,'単独行動C+','alone_move'),(54,'単独行動B-','alone_move'),(55,'単独行動B','alone_move'),(56,'単独行動B+','alone_move'),(57,'単独行動A','alone_move'),(58,'単独行動A+','alone_move'),(59,'単独行動A++','alone_move'),(60,'単独行動EX','alone_move'),(61,'気配遮断E-','invis'),(62,'気配遮断E','invis'),(63,'気配遮断E+','invis'),(64,'気配遮断D','invis'),(65,'気配遮断D+','invis'),(66,'気配遮断C-','invis'),(67,'気配遮断C','invis'),(68,'気配遮断C+','invis'),(69,'気配遮断B-','invis'),(70,'気配遮断B','invis'),(71,'気配遮断B+','invis'),(72,'気配遮断A','invis'),(73,'気配遮断A+','invis'),(74,'気配遮断A++','invis'),(75,'気配遮断EX','invis'),(76,'道具作成E-','create_tool'),(77,'道具作成E','create_tool'),(78,'道具作成E+','create_tool'),(79,'道具作成D','create_tool'),(80,'道具作成D+','create_tool'),(81,'道具作成C-','create_tool'),(82,'道具作成C','create_tool'),(83,'道具作成C+','create_tool'),(84,'道具作成B-','create_tool'),(85,'道具作成B','create_tool'),(86,'道具作成B+','create_tool'),(87,'道具作成A','create_tool'),(88,'道具作成A+','create_tool'),(89,'道具作成A++','create_tool'),(90,'道具作成EX','create_tool'),(91,'道具作成（偽）A','create_tool'),(92,'神性E-','divinity'),(93,'神性E','divinity'),(94,'神性E+','divinity'),(95,'神性D','divinity'),(96,'神性D+','divinity'),(97,'神性C-','divinity'),(98,'神性C','divinity'),(99,'神性C+','divinity'),(100,'神性B-','divinity'),(101,'神性B','divinity'),(102,'神性B+','divinity'),(103,'神性A','divinity'),(104,'神性A+','divinity'),(105,'神性A++','divinity'),(106,'神性EX','divinity'),(107,'対魔力E-','anti_magic'),(108,'対魔力E','anti_magic'),(109,'対魔力E+','anti_magic'),(110,'対魔力D','anti_magic'),(111,'対魔力D+','anti_magic'),(112,'対魔力C-','anti_magic'),(113,'対魔力C','anti_magic'),(114,'対魔力C+','anti_magic'),(115,'対魔力B-','anti_magic'),(116,'対魔力B','anti_magic'),(117,'対魔力B+','anti_magic'),(118,'対魔力A','anti_magic'),(119,'対魔力A+','anti_magic'),(120,'対魔力A++','anti_magic'),(121,'対魔力EX','anti_magic'),(122,'女神の神核C','female_goddes'),(123,'女神の神核B','female_goddes'),(124,'女神の神核A','female_goddes'),(125,'女神の神核EX','female_goddes'),(126,'コスモリアクターB','star'),(127,'コスモリアクターA','star'),(128,'単独顕現E','alone_move'),(129,'単独顕現C','alone_move'),(130,'根源接続A','special'),(131,'復讐者C','avenger'),(132,'復讐者B-','avenger'),(133,'復讐者B','avenger'),(134,'復讐者A','avenger'),(135,'忘却補正E','ovlivion'),(136,'忘却補正C','ovlivion'),(137,'忘却補正B','ovlivion'),(138,'忘却補正A','ovlivion'),(139,'自己回復（魔力）E','recoveryNP'),(140,'自己回復（魔力）D','recoveryNP'),(141,'自己回復（魔力）C','recoveryNP'),(142,'自己回復（魔力）B','recoveryNP'),(143,'自己回復（魔力）A','recoveryNP'),(144,'自己回復（魔力）A+','recoveryNP'),(145,'サーフィンA','Arts'),(146,'無限の魔力供給C','npget'),(147,'無限の魔力供給B','npget'),(148,'境界にてA','soul'),(149,'混血EX','npget'),(150,'オルトリアクターA','prayer'),(151,'無頼漢A','crit'),(152,'獣の権能D','crit'),(153,'ロゴスイーターC','defence'),(154,'ネガセイヴァーA','special'),(155,'道具作成（奇）EX','create_tool'),(156,'気配遮断（陰）B','invis'),(157,'領域外の生命D','foriner'),(158,'領域外の生命B','foriner'),(159,'領域外の生命EX','foriner'),(160,'狂気B','Buster'),(161,'文明浸食EX','crit'),(162,'獣化B','berserk'),(163,'妖精契約A','prayer'),(164,'ホムンクルスC+','Arts'),(165,'単独行動（セレブ）EX','alone_move'),(166,'大地を飲むものEX','prayer'),(167,'凶化A+','berserk'),(168,'お宿作成B','Arts'),(169,'腹話術EX','invalid'),(170,'老練A+','Arts'),(171,'愛神の神核B','female_goddes'),(172,'王の映し身A','npup'),(173,'憤怒の化身EX','Buster'),(174,'ダブルクラスE','non'),(175,'ハイ・サーヴァントA','non'),(176,'ハイ・サーヴァントEX','non');
/*!40000 ALTER TABLE `class_skill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `class_skill_effect`
--

DROP TABLE IF EXISTS `class_skill_effect`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_skill_effect` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `class_skill_id` int(11) NOT NULL,
  `effect_text` varchar(255) NOT NULL,
  `effect_genre` int(11) DEFAULT NULL,
  `bord` int(11) DEFAULT NULL,
  `effect_para` float DEFAULT NULL,
  `target_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=209 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class_skill_effect`
--

LOCK TABLES `class_skill_effect` WRITE;
/*!40000 ALTER TABLE `class_skill_effect` DISABLE KEYS */;
INSERT INTO `class_skill_effect` VALUES (1,1,'自身のQuickカードの性能をアップ',4,1,NULL,1),(2,2,'自身のQuickカードの性能をアップ',4,1,2,1),(3,3,'自身のQuickカードの性能をアップ',4,1,NULL,1),(4,4,'自身のQuickカードの性能をアップ',4,1,4,1),(5,5,'自身のQuickカードの性能をアップ',4,1,NULL,1),(6,6,'自身のQuickカードの性能をアップ',4,1,NULL,1),(7,7,'自身のQuickカードの性能をアップ',4,1,6,1),(8,8,'自身のQuickカードの性能をアップ',4,1,7,1),(9,9,'自身のQuickカードの性能をアップ',4,1,NULL,1),(10,10,'自身のQuickカードの性能をアップ',4,1,8,1),(11,11,'自身のQuickカードの性能をアップ',4,1,NULL,1),(12,12,'自身のQuickカードの性能をアップ',4,1,10,1),(13,13,'自身のQuickカードの性能をアップ',4,1,11,1),(14,14,'自身のQuickカードの性能をアップ',4,1,11.5,1),(15,15,'自身のQuickカードの性能をアップ',4,1,12,1),(16,16,'自身のArtsカードの性能をアップ',3,1,NULL,1),(17,17,'自身のArtsカードの性能をアップ',3,1,2,1),(18,18,'自身のArtsカードの性能をアップ',3,1,NULL,1),(19,19,'自身のArtsカードの性能をアップ',3,1,4,1),(20,20,'自身のArtsカードの性能をアップ',3,1,NULL,1),(21,21,'自身のArtsカードの性能をアップ',3,1,NULL,1),(22,22,'自身のArtsカードの性能をアップ',3,1,6,1),(23,23,'自身のArtsカードの性能をアップ',3,1,7,1),(24,24,'自身のArtsカードの性能をアップ',3,1,NULL,1),(25,25,'自身のArtsカードの性能をアップ',3,1,8,1),(26,26,'自身のArtsカードの性能をアップ',3,1,NULL,1),(27,27,'自身のArtsカードの性能をアップ',3,1,10,1),(28,28,'自身のArtsカードの性能をアップ',3,1,11,1),(29,29,'自身のArtsカードの性能をアップ',3,1,11.5,1),(30,30,'自身のArtsカードの性能をアップ',3,1,12,1),(31,31,'自身のBusterカードの性能をアップ',3,1,1,1),(32,32,'自身のBusterカードの性能をアップ',3,1,2,1),(33,33,'自身のBusterカードの性能をアップ',3,1,3,1),(34,34,'自身のBusterカードの性能をアップ',3,1,4,1),(35,35,'自身のBusterカードの性能をアップ',3,1,5,1),(36,36,'自身のBusterカードの性能をアップ',3,1,NULL,1),(37,37,'自身のBusterカードの性能をアップ',3,1,6,1),(38,38,'自身のBusterカードの性能をアップ',3,1,NULL,1),(39,39,'自身のBusterカードの性能をアップ',3,1,NULL,1),(40,40,'自身のBusterカードの性能をアップ',3,1,8,1),(41,41,'自身のBusterカードの性能をアップ',3,1,NULL,1),(42,42,'自身のBusterカードの性能をアップ',3,1,10,1),(43,43,'自身のBusterカードの性能をアップ',3,1,11,1),(44,44,'自身のBusterカードの性能をアップ',3,1,NULL,1),(45,45,'自身のBusterカードの性能をアップ',3,1,12,1),(46,46,'自身のクリティカル威力をアップ',9,1,NULL,1),(47,47,'自身のクリティカル威力をアップ',9,1,2,1),(48,48,'自身のクリティカル威力をアップ',9,1,NULL,1),(49,49,'自身のクリティカル威力をアップ',9,1,4,1),(50,50,'自身のクリティカル威力をアップ',9,1,NULL,1),(51,51,'自身のクリティカル威力をアップ',9,1,NULL,1),(52,52,'自身のクリティカル威力をアップ',9,1,6,1),(53,53,'自身のクリティカル威力をアップ',9,1,NULL,1),(54,54,'自身のクリティカル威力をアップ',9,1,NULL,1),(55,55,'自身のクリティカル威力をアップ',9,1,8,1),(56,56,'自身のクリティカル威力をアップ',9,1,NULL,1),(57,57,'自身のクリティカル威力をアップ',9,1,10,1),(58,58,'自身のクリティカル威力をアップ',9,1,11,1),(59,59,'自身のクリティカル威力をアップ',9,1,NULL,1),(60,60,'自身のクリティカル威力をアップ',9,1,12,1),(61,61,'自身のスター発生率をアップ',10,1,NULL,1),(62,62,'自身のスター発生率をアップ',10,1,2,1),(63,63,'自身のスター発生率をアップ',10,1,NULL,1),(64,64,'自身のスター発生率をアップ',10,1,4,1),(65,65,'自身のスター発生率をアップ',10,1,NULL,1),(66,66,'自身のスター発生率をアップ',10,1,5.5,1),(67,67,'自身のスター発生率をアップ',10,1,6,1),(68,68,'自身のスター発生率をアップ',10,1,6.5,1),(69,69,'自身のスター発生率をアップ',10,1,NULL,1),(70,70,'自身のスター発生率をアップ',10,1,8,1),(71,71,'自身のスター発生率をアップ',10,1,NULL,1),(72,72,'自身のスター発生率をアップ',10,1,10,1),(73,73,'自身のスター発生率をアップ',10,1,10.5,1),(74,74,'自身のスター発生率をアップ',10,1,NULL,1),(75,75,'自身のスター発生率をアップ',10,1,12,1),(76,76,'自身の弱体付与成功率をアップ',10,1,NULL,1),(77,77,'自身の弱体付与成功率をアップ',10,1,2,1),(78,78,'自身の弱体付与成功率をアップ',10,1,NULL,1),(79,79,'自身の弱体付与成功率をアップ',10,1,4,1),(80,80,'自身の弱体付与成功率をアップ',10,1,NULL,1),(81,81,'自身の弱体付与成功率をアップ',10,1,NULL,1),(82,82,'自身の弱体付与成功率をアップ',10,1,6,1),(83,83,'自身の弱体付与成功率をアップ',10,1,NULL,1),(84,84,'自身の弱体付与成功率をアップ',10,1,NULL,1),(85,85,'自身の弱体付与成功率をアップ',10,1,8,1),(86,86,'自身の弱体付与成功率をアップ',10,1,9,1),(87,87,'自身の弱体付与成功率をアップ',10,1,10,1),(88,88,'自身の弱体付与成功率をアップ',10,1,NULL,1),(89,89,'自身の弱体付与成功率をアップ',10,1,NULL,1),(90,90,'自身の弱体付与成功率をアップ',10,1,12,1),(91,91,'自身の弱体付与成功率をアップ',10,1,10,1),(92,92,'自身に与ダメージプラス状態を付与',6,1,95,1),(93,93,'自身に与ダメージプラス状態を付与',6,1,100,1),(94,94,'自身に与ダメージプラス状態を付与',6,1,NULL,1),(95,95,'自身に与ダメージプラス状態を付与',6,1,125,1),(96,96,'自身に与ダメージプラス状態を付与',6,1,NULL,1),(97,97,'自身に与ダメージプラス状態を付与',6,1,NULL,1),(98,98,'自身に与ダメージプラス状態を付与',6,1,150,1),(99,99,'自身に与ダメージプラス状態を付与',6,1,NULL,1),(100,100,'自身に与ダメージプラス状態を付与',6,1,170,1),(101,101,'自身に与ダメージプラス状態を付与',6,1,175,1),(102,102,'自身に与ダメージプラス状態を付与',6,1,185,1),(103,103,'自身に与ダメージプラス状態を付与',6,1,200,1),(104,104,'自身に与ダメージプラス状態を付与',6,1,210,1),(105,105,'自身に与ダメージプラス状態を付与',6,1,230,1),(106,106,'自身に与ダメージプラス状態を付与',6,1,250,1),(107,107,'自身の弱体耐性をアップ',13,1,NULL,1),(108,108,'自身の弱体耐性をアップ',13,1,10,1),(109,109,'自身の弱体耐性をアップ',13,1,NULL,1),(110,110,'自身の弱体耐性をアップ',13,1,12.5,1),(111,111,'自身の弱体耐性をアップ',13,1,13,1),(112,112,'自身の弱体耐性をアップ',13,1,NULL,1),(113,113,'自身の弱体耐性をアップ',13,1,15,1),(114,114,'自身の弱体耐性をアップ',13,1,NULL,1),(115,115,'自身の弱体耐性をアップ',13,1,NULL,1),(116,116,'自身の弱体耐性をアップ',13,1,17.5,1),(117,117,'自身の弱体耐性をアップ',13,1,18,1),(118,118,'自身の弱体耐性をアップ',13,1,20,1),(119,119,'自身の弱体耐性をアップ',13,1,NULL,1),(120,120,'自身の弱体耐性をアップ',13,1,NULL,1),(121,121,'自身の弱体耐性をアップ',13,1,25,1),(122,122,'自身に与ダメージプラス状態を付与',6,1,200,1),(123,123,'自身に与ダメージプラス状態を付与',6,1,225,1),(124,124,'自身に与ダメージプラス状態を付与',6,1,250,1),(125,125,'自身に与ダメージプラス状態を付与',6,1,300,1),(126,122,'＆弱体耐性をアップ',13,1,20,1),(127,123,'＆弱体耐性をアップ',13,1,22.5,1),(128,124,'＆弱体耐性をアップ',13,1,25,1),(129,125,'＆弱体耐性をアップ',13,1,30,1),(130,126,'自身のスター発生率をアップ',10,1,8,1),(131,127,'自身のスター発生率をアップ',10,1,10,1),(132,128,'自身のクリティカル威力をアップ',9,1,2,1),(133,129,'自身のクリティカル威力をアップ',9,1,6,1),(134,128,'＆即死耐性をアップ',35,1,2,1),(135,129,'＆即死耐性をアップ',35,1,6,1),(136,128,'＆精神異常耐性をアップ',34,1,2,1),(137,129,'＆精神異常耐性をアップ',34,1,6,1),(138,130,'自身のBusterカードの性能をアップ',2,1,6,1),(139,130,'＆Artsカードの性能をアップ',3,1,6,1),(140,130,'＆Quickカードの性能をアップ',4,1,6,1),(141,131,'自身の被ダメージ時に獲得するNPアップ',11,1,16,1),(142,132,'自身の被ダメージ時に獲得するNPアップ',11,1,17.5,1),(143,133,'自身の被ダメージ時に獲得するNPアップ',11,1,18,1),(144,134,'自身の被ダメージ時に獲得するNPアップ',11,1,20,1),(145,131,'＆自身を除く味方全体<控え含む>の弱体耐性をダウン【デメリット】',13,2,6,4),(146,132,'＆自身を除く味方全体<控え含む>の弱体耐性をダウン【デメリット】',13,2,7.5,4),(147,133,'＆自身を除く味方全体<控え含む>の弱体耐性をダウン【デメリット】',13,2,8,4),(148,134,'＆自身を除く味方全体<控え含む>の弱体耐性をダウン【デメリット】',13,2,10,4),(149,135,'自身のクリティカル威力をアップ',9,1,2,1),(150,136,'自身のクリティカル威力をアップ',9,1,6,1),(151,137,'自身のクリティカル威力をアップ',9,1,8,1),(152,138,'自身のクリティカル威力をアップ',9,1,10,1),(153,139,'自身に毎ターンNP獲得状態を付与',12,1,2,1),(154,140,'自身に毎ターンNP獲得状態を付与',12,1,3,1),(156,141,'自身に毎ターンNP獲得状態を付与',12,1,3.3,1),(157,142,'自身に毎ターンNP獲得状態を付与',12,1,3.5,1),(158,143,'自身に毎ターンNP獲得状態を付与',12,1,3.8,1),(159,144,'自身に毎ターンNP獲得状態を付与',12,1,4,1),(160,145,'自身のArtsカード性能をアップ',3,1,5,1),(161,145,'＆スター発生率をアップ',10,1,5,1),(162,146,'自身に毎ターンNP獲得状態を付与',12,1,3,1),(163,147,'自身に毎ターンNP獲得状態を付与',12,1,4,1),(164,148,'自身に即死無効状態を付与',35,1,NULL,1),(165,148,'＆強力な魅了耐性を付与',17,1,100,1),(166,148,'＆通常攻撃時に極低確率で即死効果が発生する状態を付与',31,1,5,1),(167,149,'自身に毎ターンNP獲得状態を付与',12,1,5,1),(168,150,'自身の弱体耐性をアップ',13,1,20,1),(169,151,'自身のQucikカード性能をアップ',4,1,5,1),(170,151,'＆クリティカル威力をアップ',9,1,5,1),(171,152,'自身のクリティカル威力をアップ',9,1,8,1),(172,153,'自身の〔人型〕の敵からの攻撃に対する防御力をアップ',28,1,16,1),(173,154,'自身に〔ルーラー〕クラスへの特攻状態を付与',29,1,150,1),(174,155,'自身のHP回復量をアップ',26,1,10,1),(175,156,'自身のスター発生率をアップ',10,1,8,1),(176,156,'＆弱体耐性をダウン【デメリット】',13,2,10,1),(177,157,'自身に毎ターンスター獲得状態を付与',20,1,2,1),(178,158,'自身に毎ターンスター獲得状態を付与',20,1,2,1),(179,159,'自身に毎ターンスター獲得状態を付与',20,1,2,1),(180,157,'＆弱体耐性をアップ',13,1,4,1),(181,158,'＆弱体耐性をアップ',13,1,8,1),(182,159,'＆弱体耐性をアップ',13,1,12,1),(183,160,'自身のBusterカードの性能をアップ',2,1,8,1),(184,161,'自身のクリティカル威力をアップ',9,1,10,1),(185,162,'自身のBusterカードの性能をアップ',2,1,8,1),(186,162,'＆スター発生率をアップ',10,1,8,1),(187,163,'自身の弱体付与成功率をアップ',21,1,10,1),(188,163,'＆弱体耐性をアップ',13,1,10,1),(189,164,'自身のArtsカード性能をアップ',3,1,6.5,1),(190,164,'＆弱体耐性をアップ',13,1,6.5,1),(191,165,'自身のクリティカル威力をアップ',9,1,10,1),(192,165,'＆〔水辺〕のフィールドにおいて自身に毎ターンNP獲得状態を付与',12,1,3,1),(193,166,'自身にやけど無効状態を付与',14,1,NULL,1),(194,167,'自身のBusterカードの性能をアップ',2,1,8,1),(195,167,'＆クリティカル威力をアップ',9,1,8,1),(196,168,'自身のArtsカードの性能アップ',3,1,10,1),(197,169,'自身にスキル封印無効状態を付与',3,1,NULL,1),(198,170,'自身のArtsカードの性能アップ',3,1,8,1),(199,170,'＆スター発生率をアップ',10,1,8,1),(200,171,'自身に与ダメージプラス状態を付与',6,1,225,1),(201,171,'＆魅了無効状態を付与',14,1,NULL,1),(202,172,'自身のNP獲得量をアップ',11,1,10,1),(203,173,'自身のBusterカード性能を少しアップ',2,1,5,1),(204,173,'自身のBusterカード性能を少しアップ',9,1,10,1),(205,173,'＆精神異常無効状態を付与',34,1,NULL,1),(206,174,'効果なし',NULL,NULL,NULL,NULL),(207,175,'効果なし',NULL,NULL,NULL,NULL),(208,176,'効果なし',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `class_skill_effect` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `color`
--

DROP TABLE IF EXISTS `color`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `color` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `color` varchar(255) NOT NULL,
  `in_value` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `color`
--

LOCK TABLES `color` WRITE;
/*!40000 ALTER TABLE `color` DISABLE KEYS */;
INSERT INTO `color` VALUES (1,'B',1.5),(2,'A',1),(3,'Q',0.8);
/*!40000 ALTER TABLE `color` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `noblephantasm`
--

DROP TABLE IF EXISTS `noblephantasm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `noblephantasm` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `ruby` varchar(255) NOT NULL,
  `rank` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `damage_text` varchar(255) DEFAULT NULL,
  `bairitsu_id` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `noblephantasm`
--

LOCK TABLES `noblephantasm` WRITE;
/*!40000 ALTER TABLE `noblephantasm` DISABLE KEYS */;
INSERT INTO `noblephantasm` VALUES (1,'仮想宝具 疑似展開／人理の礎','ロード・カルデアス','D','対人宝具',NULL,19,'補助'),(2,'いまは遙か理想の城','ロード・キャメロット','B+++','対悪宝具',NULL,19,'補助'),(3,'いまは脆き夢想の城','モールド・キャメロット','D','対人宝具',NULL,19,'補助'),(5,'約束された勝利の剣','エクスカリバー','A','対城宝具','敵全体に強力な攻撃[Lv]',1,'全体'),(6,'約束された勝利の剣','エクスカリバー','A++','対城宝具','敵全体に強力な攻撃[Lv]',2,'全体'),(7,'約束された勝利の剣','エクスカリバー・モルガン','A++','対城宝具','敵全体にとても強力な攻撃[Lv]',1,'全体'),(8,'勝利すべき黄金の剣','カリバーン','B','対人宝具','敵全体に強力な攻撃[Lv]',1,'全体'),(9,'勝利すべき黄金の剣','カリバーン','B+','対人宝具','敵全体に強力な攻撃[Lv]',2,'全体'),(10,'童女謳う華の帝政','ラウス・セント・クラウディウス','B-','対陣宝具','敵全体に強力な防御力無視攻撃[Lv]',3,'全体'),(11,'童女謳う華の帝政','ラウス・セント・クラウディウス','B','対陣宝具','敵全体に強力な防御力無視攻撃[Lv]',4,'全体'),(12,'幻想大剣・天魔失墜','バルムンク','A+','対軍宝具','敵全体に強力な攻撃[Lv]',1,'全体'),(13,'幻想大剣・天魔失墜','バルムンク','EX','対軍宝具','敵全体に強力な攻撃[Lv]',2,'全体'),(14,'黄の死','クロケア・モース','B+','対人宝具','敵単体に超強力な攻撃[Lv]',10,'単体'),(15,'軍神の剣','フォトン・レイ','A-','対軍宝具','敵全体に強力な攻撃[Lv]',1,'全体'),(16,'軍神の剣','フォトン・レイ','A','対軍宝具','敵全体に強力な攻撃[Lv]',2,'全体'),(17,'神聖たる旗に集いて吼えよ','セイント・ウォーオーダー','B','対人宝具',NULL,19,'補助'),(18,'百合の花咲く豪華絢爛','フルール・ド・リリス','C','対軍宝具',NULL,19,'補助'),(19,'百合の花咲く豪華絢爛','フルール・ド・リリス','C+','対軍宝具',NULL,19,'補助'),(20,'無限の剣製','アンリミテッドブレイドワークス','E～A','？？？？','敵全体に強力な防御力無視攻撃[Lv]',1,'全体'),(21,'天地乖離す開闢の星','エヌマ・エリシュ','A++','対界宝具','敵全体に強力な攻撃[Lv]',1,'全体'),(22,'天地乖離す開闢の星','エヌマ・エリシュ','EX','対界宝具','＋敵全体に強力な攻撃[Lv]',2,'全体'),(23,'祈りの弓','イー・バウ','D','対人宝具','敵単体に超強力な攻撃[Lv]',9,'単体'),(24,'訴状の矢文','ポイボス・カタストロフェ','B','対軍宝具','敵全体に強力な攻撃[Lv]',5,'全体'),(25,'訴状の矢文','ポイボス・カタストロフェ','B+','対軍宝具','敵全体に強力な攻撃[Lv]',6,'全体'),(26,'女神の視線','アイ・オブ・ザ・エウリュアレ','B-','対人宝具','敵単体に超強力な攻撃[Lv]',15,'単体'),(27,'女神の視線','アイ・オブ・ザ・エウリュアレ','B','対人宝具','敵単体に超強力な攻撃[Lv]',16,'単体'),(28,'流星一条','ステラ','B++','対軍宝具','敵全体に超強力な攻撃[Lv]',24,'全体'),(29,'流星一条','ステラ','A','対軍宝具','敵全体に超強力な攻撃[Lv]',25,'全体'),(30,'刺し穿つ死棘の槍','ゲイ・ボルグ','B','対人宝具','敵単体に超強力な攻撃[Lv]',5,'単体'),(31,'刺し穿つ死棘の槍','ゲイ・ボルグ','B+','対人宝具','＋敵単体に超強力な攻撃[Lv]',6,'単体'),(32,'鮮血魔嬢','バートリ・エルジェーベト','E-','対人宝具','敵全体に強力な防御力無視攻撃[Lv]',7,'全体'),(33,'鮮血魔嬢','バートリ・エルジェーベト','E+','対人宝具','敵全体に強力な防御力無視攻撃[Lv]',8,'全体'),(34,'五百羅漢補陀落渡海','ごひゃくらかんふだらくとかい','EX','対軍宝具',NULL,19,'補助'),(35,'穿ちの朱槍','ゲイ・ボルグ','B','対人宝具','敵単体に超強力な攻撃[Lv]',5,'単体'),(36,'炎門の守護者','テルモピュライ・エノモタイア','B','対軍宝具',NULL,19,'補助'),(37,'すべては我が槍に通ずる','マグナ・ウォルイッセ・マグヌム','A++','対軍宝具','敵全体に強力な攻撃[Lv]',1,'全体'),(38,'騎英の手綱','ベルレフォーン','A+','対軍宝具','敵全体に強力な攻撃[Lv]',5,'全体'),(39,'騎英の手綱','ベルレフォーン','A+','対軍宝具','敵全体に強力な攻撃[Lv]',6,'全体'),(40,'力屠る祝福の剣','アスカロン','B','対人宝具','＆超強力な攻撃[Lv]',9,'単体'),(41,'アン女王の復讐','クイーンアンズ・リベンジ','C++','対軍宝具','敵全体に強力な攻撃[Lv]',1,'全体'),(42,'アン女王の復讐','クイーンアンズ・リベンジ','C+++','対軍宝具','敵全体に強力な攻撃[Lv]',2,'全体'),(43,'約束されざる守護の車輪','チャリオット・オブ・ブディカ','B+','対軍宝具',NULL,19,'補助'),(44,'約束されざる守護の車輪','チャリオット・オブ・ブディカ','B++','対人宝具',NULL,19,'補助'),(45,'壇ノ浦・八艘跳','だんのうら・はっそうとび','D','対人奥義','敵単体に超強力な攻撃[Lv]',5,'単体'),(46,'壇ノ浦・八艘跳','だんのうら・はっそうとび','C','対人奥義','敵単体に超強力な攻撃[Lv]',6,'単体'),(47,'始まりの蹂躙制覇','ブケファラス','B+','対軍宝具','敵全体に強力な攻撃[Lv]',11,'全体'),(48,'始まりの蹂躙制覇','ブケファラス','B++','対軍宝具','敵全体に強力な攻撃[Lv]',12,'全体'),(49,'百合の王冠に栄光あれ','ギロチン・ブレイカー','A','対軍宝具','敵全体に強力な攻撃[Lv]',11,'全体'),(50,'百合の王冠に栄光あれ','ギロチン・ブレイカー','A+','対人宝具','敵全体に強力な攻撃[Lv]',12,'全体'),(51,'愛知らぬ哀しき竜よ','タラスク','A+','対軍宝具','敵全体に強力な攻撃[Lv]',7,'全体'),(52,'愛知らぬ哀しき竜よ','タラスク','EX','対軍宝具','敵全体に強力な攻撃[Lv]',8,'全体'),(53,'破戒すべき全ての符','ルールブレイカー','C','対魔術宝具','敵単体に強力な攻撃[Lv]',22,'単体'),(54,'破戒すべき全ての符','ルールブレイカー','C+','対魔術宝具','敵単体に強力な攻撃[Lv]',23,'単体'),(55,'無限の剣製','アンリミテッドブレイドワークス','E～A','？？？？','敵全体に強力な防御力無視攻撃[Lv]',2,'全体');
/*!40000 ALTER TABLE `noblephantasm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `np_bairitsu`
--

DROP TABLE IF EXISTS `np_bairitsu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `np_bairitsu` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `np_bairitsu_1` float DEFAULT NULL,
  `np_bairitsu_2` float DEFAULT NULL,
  `np_bairitsu_3` float DEFAULT NULL,
  `np_bairitsu_4` float DEFAULT NULL,
  `np_bairitsu_5` float DEFAULT NULL,
  `biko` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `np_bairitsu`
--

LOCK TABLES `np_bairitsu` WRITE;
/*!40000 ALTER TABLE `np_bairitsu` DISABLE KEYS */;
INSERT INTO `np_bairitsu` VALUES (1,300,400,450,475,500,'強化前全体 B'),(2,400,500,550,575,600,'強化後全体 B'),(3,450,600,675,712.5,750,'強化前全体 A'),(4,600,750,825,862.5,900,'強化後全体 A'),(5,600,800,900,950,1000,'強化前全体 Q'),(6,800,1000,1100,1150,1200,'強化後全体 Q'),(7,600,800,900,950,1000,'強化前単体 B'),(8,800,1000,1100,1150,1200,'強化後単体 B'),(9,900,1200,1300,1425,1500,'強化前単体 A'),(10,1200,1500,1650,1725,1800,'強化後単体 A'),(11,1200,1600,1800,1900,2000,'強化前単体 Q'),(12,1600,2000,2200,2300,2400,'強化前単体 Q'),(13,450,550,600,625,650,'オルタ倍率'),(14,700,900,1000,1050,1100,'ベオウルフ強化後'),(15,900,900,900,900,900,'エウリュアレ強化前'),(16,1200,1200,1200,1200,1200,'エウリュアレ強化後'),(17,600,800,900,950,1000,'ヴラド強化前'),(18,900,1200,1350,1425,1500,'ヴラド強化後'),(19,NULL,NULL,NULL,NULL,NULL,'補助宝具用'),(20,NULL,NULL,NULL,NULL,NULL,'欠番'),(21,NULL,NULL,NULL,NULL,NULL,'欠番'),(22,450,600,675,712.5,750,'メディア強化前'),(23,600,750,825,862.5,900,'メディア強化後'),(24,600,800,900,950,1000,'アーラシュ強化前'),(25,800,1000,1100,1150,1200,'アーラシュ強化後');
/*!40000 ALTER TABLE `np_bairitsu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `np_effect`
--

DROP TABLE IF EXISTS `np_effect`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `np_effect` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `np_id` int(11) NOT NULL,
  `effect_text` varchar(255) NOT NULL,
  `effect_genre` int(11) NOT NULL,
  `turn` int(11) DEFAULT NULL,
  `bord` int(11) NOT NULL,
  `effect_para1` float DEFAULT NULL,
  `effect_para2` float DEFAULT NULL,
  `effect_para3` float DEFAULT NULL,
  `effect_para4` float DEFAULT NULL,
  `effect_para5` float DEFAULT NULL,
  `beaf` int(11) DEFAULT NULL,
  `target_id` int(11) DEFAULT NULL,
  `fast` int(11) DEFAULT NULL,
  `fore` int(11) DEFAULT NULL,
  `oc` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `np_effect`
--

LOCK TABLES `np_effect` WRITE;
/*!40000 ALTER TABLE `np_effect` DISABLE KEYS */;
INSERT INTO `np_effect` VALUES (1,1,'味方全体の防御力をアップ＜OC＞',28,3,1,30,35,40,45,50,2,3,NULL,1,1),(2,1,'＆被ダメージカット状態を付与[Lv]',30,3,1,100,550,775,888,1000,2,3,NULL,1,NULL),(3,2,'味方全体の防御力をアップ＜OC＞',28,3,1,30,35,40,45,50,2,3,NULL,1,1),(4,2,'＆被ダメージカット状態を付与[Lv]',30,3,1,100,550,775,888,1000,2,3,NULL,1,NULL),(5,2,'＋自身を除く味方全体の攻撃力をアップ',1,3,1,30,30,30,30,30,2,4,NULL,1,NULL),(6,3,'味方全体の防御力をアップ＜OC＞',28,3,1,30,35,40,45,50,2,3,NULL,1,1),(7,3,'＆被ダメージカット状態を付与[Lv]',30,3,1,100,550,775,888,1000,2,3,NULL,1,NULL),(8,5,'＋ 自身のNPをリチャージ＜OC＞',12,NULL,1,20,27.5,35,42.5,50,2,1,NULL,1,1),(9,6,'＋ 自身のNPをリチャージ＜OC＞',12,NULL,1,20,27.5,35,42.5,50,2,1,NULL,1,1),(10,7,'＋ 自身のNPをリチャージ＜OC＞',12,NULL,1,10,15,20,25,30,2,1,NULL,1,1),(11,8,'＋ 自身のHPを大回復＜OC＞',25,NULL,1,1000,2000,3000,4000,5000,2,1,NULL,1,1),(12,9,'＋ 自身のHPを大回復＜OC＞',25,NULL,1,2000,3000,4000,5000,6000,2,1,NULL,1,1),(13,10,'＆防御力をダウン＜OC＞',28,1,2,20,25,30,35,40,2,3,NULL,2,1),(14,11,'＆防御力をダウン＜OC＞',28,1,2,20,25,30,35,40,2,3,NULL,2,1),(15,12,'＆〔竜〕特攻＜OC＞',7,NULL,1,150,162.5,175,187.5,200,1,1,NULL,1,1),(16,13,'＆〔竜〕特攻＜OC＞',7,NULL,1,150,162.5,175,187.5,200,1,1,NULL,1,1),(17,13,'＋ 自身のNP獲得量をアップ',11,3,1,20,20,20,20,20,2,1,NULL,1,NULL),(18,14,'＋ スターを獲得＜OC＞',20,NULL,1,5,10,15,20,25,2,3,NULL,1,1),(19,15,'＆防御力をダウン＜OC＞',28,3,2,10,15,20,25,30,2,3,NULL,2,1),(20,16,'＆防御力をダウン＜OC＞',28,3,2,20,25,30,35,40,2,3,NULL,2,1),(21,17,'自身の攻撃力を大アップ[Lv]',1,2,1,50,75,87.5,93.8,100,2,1,NULL,1,NULL),(22,17,'＆防御力を大ダウン【デメリット】',28,3,2,50,50,50,50,50,2,1,NULL,1,NULL),(23,17,'＋スターを獲得＜OC＞',20,NULL,1,5,10,15,20,25,2,3,NULL,1,1),(24,18,'敵全体に確率で魅了状態を付与＜OC＞',17,1,2,10,20,30,40,50,2,1,NULL,2,1),(25,18,'＆攻撃力をダウン[Lv]',1,2,2,10,20,25,27.5,30,2,3,NULL,2,NULL),(26,18,'＆防御力をダウン[Lv]',28,2,2,10,20,25,27.5,30,2,3,NULL,2,NULL),(27,19,'敵全体に確率で魅了状態を付与＜OC＞',17,1,2,30,40,50,60,70,2,3,NULL,2,1),(28,19,'＆攻撃力をダウン[Lv]',1,3,2,10,20,25,27.5,30,2,3,NULL,2,NULL),(29,19,'＆防御力をダウン[Lv]',28,3,2,10,20,25,27.5,30,2,3,NULL,2,NULL),(30,20,'＆攻撃力をダウン＜OC＞',1,3,2,10,15,20,25,30,2,3,NULL,2,1),(31,55,'＆攻撃力をダウン＜OC＞',1,3,2,10,15,20,25,30,2,3,NULL,2,1),(32,21,'＆〔サーヴァント〕特攻＜OC＞',7,NULL,1,150,162.5,175,187.5,200,1,1,NULL,1,1),(33,22,'自身の宝具威力をアップ',5,1,1,30,30,30,30,30,1,1,1,1,NULL),(34,22,'＆〔サーヴァント〕特攻＜OC＞',7,NULL,1,150,162.5,175,187.5,200,1,1,NULL,1,1),(35,23,'＆〔毒〕特攻＜OC＞',8,NULL,1,200,212.5,225,237.5,250,1,1,NULL,1,1),(36,24,'＋スターを獲得＜OC＞',20,NULL,1,10,15,20,25,30,2,3,NULL,1,1),(37,25,'＋スターを大量獲得＜OC＞',20,NULL,1,15,20,25,30,35,2,3,NULL,1,1),(38,26,'＆〔男性〕特攻[Lv]',7,NULL,1,150,200,225,238,250,1,1,NULL,1,NULL),(39,26,'＆高確率で魅了状態を付与[男性限定]',17,1,2,100,125,150,175,200,2,5,NULL,2,NULL),(40,26,'＆攻撃力をダウン',1,3,2,20,20,20,20,20,2,2,NULL,2,NULL),(41,27,'＆〔男性〕特攻[Lv]',7,NULL,1,150,200,225,238,250,1,1,NULL,1,NULL),(42,27,'＆高確率で魅了状態を付与[男性限定]',17,1,2,100,125,150,175,200,2,5,NULL,2,NULL),(43,27,'＆攻撃力をダウン',1,3,2,20,20,20,20,20,2,2,NULL,2,NULL),(44,28,'＆威力アップ＜OC＞',5,NULL,1,NULL,100,200,300,400,1,1,NULL,1,1),(45,28,'＋自身に即死効果【デメリット】',31,NULL,2,NULL,NULL,NULL,NULL,NULL,2,1,NULL,1,NULL),(46,29,'＆威力アップ＜OC＞',5,NULL,1,NULL,200,400,600,800,1,1,NULL,1,1),(47,29,'＋自身に即死効果【デメリット】',31,NULL,2,NULL,NULL,NULL,NULL,NULL,2,1,NULL,1,NULL),(48,30,'＆防御力をダウン＜OC＞',28,3,2,10,15,20,25,30,2,2,NULL,2,1),(49,30,'＆中確率で即死効果＜OC＞',28,NULL,2,50,62.5,75,87.5,100,2,2,NULL,2,1),(50,31,'自身に必中状態を付与',18,1,1,NULL,NULL,NULL,NULL,NULL,1,1,1,1,NULL),(51,31,'＆防御力をダウン＜OC＞',28,3,2,10,15,20,25,30,2,2,NULL,2,1),(52,31,'＆中確率で即死効果＜OC＞',28,NULL,2,60,70,80,90,100,2,2,NULL,2,1),(53,32,'＆呪い状態を付与＜OC＞',15,3,2,500,1000,1500,2000,2500,2,3,NULL,2,1),(54,33,'＆呪い状態を付与＜OC＞',15,3,2,500,1000,1500,2000,2500,2,3,NULL,2,1),(55,33,'＆強化成功率をダウン',21,3,2,20,20,20,20,20,2,3,NULL,2,NULL),(56,34,'敵全体に確率でスタン状態を付与[Lv]',17,1,2,50,65,72.5,76.3,80,2,3,NULL,2,NULL),(57,34,'＆呪い状態を付与＜OC＞',15,3,2,500,1000,1500,2000,2500,2,3,NULL,2,1),(58,35,'＆防御力をダウン＜OC＞',28,3,2,10,15,20,25,30,2,2,NULL,2,1),(59,35,'＆中確率で即死効果＜OC＞',31,NULL,2,50,63,75,88,100,2,2,NULL,2,1),(60,36,'自身の防御力を大アップ＜OC＞',28,3,1,30,35,40,45,50,2,1,NULL,1,1),(61,36,'＆ターゲット集中状態を付与＜OC＞',23,3,1,100,200,300,400,500,2,1,NULL,1,1),(62,36,'＋スターを獲得[Lv]',20,NULL,1,5,15,20,23,25,2,3,NULL,1,NULL),(63,37,'＋味方全体の攻撃力をアップ＜OC＞',1,3,1,10,15,20,25,30,2,3,NULL,1,1),(64,38,'＋味方全体のスター発生率をアップ＜OC＞',10,3,1,20,30,40,50,60,2,3,NULL,1,1),(65,39,'＆クリティカル発生率をダウン',10,3,2,20,20,20,20,20,2,3,NULL,2,NULL),(66,39,'＋味方全体のスター発生率をアップ＜OC＞',10,3,1,50,62.5,72,87.5,100,2,3,NULL,1,1),(67,40,'敵単体に〔竜〕特性を付与',29,3,2,NULL,NULL,NULL,NULL,NULL,1,2,1,2,NULL),(68,40,'＋自身の防御力を大アップ＜OC＞',28,1,1,20,25,30,35,40,2,1,NULL,1,1),(69,41,'＆低確率でチャージを減らす＜OC＞',11,NULL,2,30,40,50,60,70,2,3,NULL,2,1),(70,42,'＆低確率でチャージを減らす＜OC＞',11,NULL,2,40,50,60,70,80,2,3,NULL,2,1),(71,42,'＋スターを獲得',20,NULL,1,10,10,10,10,10,2,3,NULL,1,NULL),(72,43,'味方全体の防御力をアップ[Lv]',28,3,1,10,15,17.5,18.8,20,2,3,NULL,1,NULL),(73,43,'＆防御力を大アップ＜OC＞',28,1,1,20,25,30,35,40,2,3,NULL,1,1),(74,44,'味方全体の防御力をアップ[Lv]',28,3,1,10,15,17.5,18.8,20,2,3,NULL,1,NULL),(75,44,'＆防御力を大アップ＜OC＞',28,1,1,20,25,30,35,40,2,3,NULL,1,1),(76,44,'＆攻撃力をアップ＜OC＞',1,3,1,20,25,30,35,40,2,3,NULL,1,1),(77,45,'＋自身のスター発生率を大アップ＜OC＞',10,3,1,50,60,70,80,90,2,1,NULL,1,1),(78,46,'＋自身のスター発生率を大アップ＜OC＞',10,3,1,50,60,70,80,90,2,1,NULL,1,1),(79,46,'＋スターを獲得＜OC＞',20,NULL,1,5,10,15,20,25,2,3,NULL,1,1),(80,48,'＋スターを大量獲得＜OC＞',20,NULL,1,15,20,25,30,35,2,3,NULL,1,1),(81,49,'＋味方全体のHPを回復＜OC＞',25,NULL,1,500,1000,1500,2000,2500,2,3,NULL,1,1),(82,49,'＆弱体解除',33,NULL,1,NULL,NULL,NULL,NULL,NULL,2,3,NULL,1,NULL),(83,50,'＋味方全体のHPを回復＜OC＞',25,NULL,1,1000,1500,2000,2500,3000,2,3,NULL,1,1),(84,50,'＆弱体解除',33,NULL,1,NULL,NULL,NULL,NULL,NULL,2,3,NULL,1,NULL),(85,50,'＆クリティカル威力をアップ',9,3,1,20,20,20,20,20,2,3,NULL,1,NULL),(86,51,'＆防御力大ダウン＜OC＞',28,3,2,20,25,30,35,40,2,3,NULL,2,1),(87,52,'自身のBusterカード性能をアップ',2,1,1,20,20,20,20,20,1,1,1,1,NULL),(88,52,'＆防御力大ダウン＜OC＞',28,3,2,20,25,30,35,40,2,3,NULL,2,1),(89,53,'＆強化全解除',16,NULL,2,NULL,NULL,NULL,NULL,NULL,2,2,NULL,2,NULL),(90,53,'＋自身のNPをリチャージ＜OC＞',12,NULL,1,20,40,60,80,100,2,1,NULL,1,1),(91,54,'＆強化全解除',16,NULL,2,NULL,NULL,NULL,NULL,NULL,2,2,NULL,2,NULL),(92,54,'＋自身のNPをリチャージ＜OC＞',12,NULL,1,20,40,60,80,100,2,1,NULL,1,1);
/*!40000 ALTER TABLE `np_effect` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `np_relation`
--

DROP TABLE IF EXISTS `np_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `np_relation` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `servant_id` int(11) NOT NULL,
  `np_id` int(11) NOT NULL,
  `junban` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `np_relation`
--

LOCK TABLES `np_relation` WRITE;
/*!40000 ALTER TABLE `np_relation` DISABLE KEYS */;
INSERT INTO `np_relation` VALUES (1,1,1,1),(2,1,2,2),(3,1,3,3),(4,2,5,1),(5,2,6,2),(6,3,7,1),(7,4,8,1),(8,4,9,2),(9,5,10,1),(10,5,11,2),(11,6,12,1),(12,6,13,2),(13,7,14,1),(14,8,15,1),(15,8,16,2),(16,9,17,1),(17,10,18,1),(18,10,19,2),(19,11,20,1),(20,11,55,2),(21,12,21,1),(22,12,22,2),(23,13,23,1),(24,14,24,1),(25,14,25,2),(26,15,26,1),(27,15,27,2),(28,16,28,1),(29,16,29,2),(30,17,30,1),(31,17,31,2),(32,18,32,1),(33,18,33,2),(34,19,34,1),(35,20,35,1),(36,21,36,1),(37,22,37,1),(38,23,38,1),(39,23,39,2),(40,24,40,1),(41,25,41,1),(42,25,42,2),(43,26,43,1),(44,26,44,2),(45,27,45,1),(46,27,46,2),(47,28,47,1),(48,28,48,2),(49,29,49,1),(50,29,50,2),(51,30,51,1),(52,30,52,2),(53,31,53,1),(54,31,54,2);
/*!40000 ALTER TABLE `np_relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `s_class`
--

DROP TABLE IF EXISTS `s_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_class` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `in_value` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `s_class`
--

LOCK TABLES `s_class` WRITE;
/*!40000 ALTER TABLE `s_class` DISABLE KEYS */;
INSERT INTO `s_class` VALUES (1,'セイバー',1),(2,'アーチャー',0.95),(3,'ランサー',1.05),(4,'アサシン',0.9),(5,'ライダー',1),(6,'キャスター',0.9),(7,'バーサーカー',1.1),(8,'ルーラー',1.1),(9,'アヴェンジャー',1.1),(10,'アルターエゴ',1),(11,'ムーンキャンサー',1),(12,'フォーリナー',1),(13,'シールダー',1),(14,'ビースト',0);
/*!40000 ALTER TABLE `s_class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `s_houshin`
--

DROP TABLE IF EXISTS `s_houshin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_houshin` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `s_houshin`
--

LOCK TABLES `s_houshin` WRITE;
/*!40000 ALTER TABLE `s_houshin` DISABLE KEYS */;
INSERT INTO `s_houshin` VALUES (1,'秩序'),(2,'混沌'),(3,'中立');
/*!40000 ALTER TABLE `s_houshin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `s_rare`
--

DROP TABLE IF EXISTS `s_rare`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_rare` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `rare` int(11) NOT NULL,
  `cost` int(11) NOT NULL,
  `gacha` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `s_rare`
--

LOCK TABLES `s_rare` WRITE;
/*!40000 ALTER TABLE `s_rare` DISABLE KEYS */;
INSERT INTO `s_rare` VALUES (1,0,4,'フレポ'),(2,1,3,'フレポ'),(3,2,4,'フレポ'),(4,3,7,'フレポ/恒常'),(5,4,12,'恒常'),(6,5,16,'恒常'),(7,1,3,'イベント'),(8,2,4,'イベント'),(9,3,7,'イベント'),(10,4,12,'イベント'),(11,5,16,'イベント'),(12,3,7,'スト限'),(13,4,12,'スト限'),(14,5,16,'スト限'),(15,3,7,'期間限定'),(16,4,12,'期間限定'),(17,5,16,'期間限定'),(18,3,0,'初期'),(19,4,0,'初期'),(20,5,0,'-');
/*!40000 ALTER TABLE `s_rare` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `s_seikaku`
--

DROP TABLE IF EXISTS `s_seikaku`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_seikaku` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `s_seikaku`
--

LOCK TABLES `s_seikaku` WRITE;
/*!40000 ALTER TABLE `s_seikaku` DISABLE KEYS */;
INSERT INTO `s_seikaku` VALUES (1,'善'),(2,'悪'),(3,'中庸'),(4,'狂'),(5,'夏'),(6,'花嫁'),(7,'善/悪');
/*!40000 ALTER TABLE `s_seikaku` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `s_sex`
--

DROP TABLE IF EXISTS `s_sex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_sex` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `s_sex`
--

LOCK TABLES `s_sex` WRITE;
/*!40000 ALTER TABLE `s_sex` DISABLE KEYS */;
INSERT INTO `s_sex` VALUES (1,'男'),(2,'女'),(3,'不明'),(4,'無');
/*!40000 ALTER TABLE `s_sex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `s_state`
--

DROP TABLE IF EXISTS `s_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_state` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `state_text` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `s_state`
--

LOCK TABLES `s_state` WRITE;
/*!40000 ALTER TABLE `s_state` DISABLE KEYS */;
INSERT INTO `s_state` VALUES (1,'神性'),(2,'人型'),(3,'人間'),(4,'竜'),(5,'王'),(6,'騎乗'),(7,'アーサー'),(8,'アルトリア顔'),(9,'ローマ'),(10,'猛獣'),(11,'魔性'),(12,'超巨大'),(13,'人類の脅威'),(14,'愛する者'),(15,'ギリシャ神話系男性'),(16,'サーヴァント'),(18,'エヌマ特攻無効');
/*!40000 ALTER TABLE `s_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `s_tenchijin`
--

DROP TABLE IF EXISTS `s_tenchijin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `s_tenchijin` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `s_tenchijin`
--

LOCK TABLES `s_tenchijin` WRITE;
/*!40000 ALTER TABLE `s_tenchijin` DISABLE KEYS */;
INSERT INTO `s_tenchijin` VALUES (1,'天'),(2,'地'),(3,'人'),(4,'星'),(5,'星（特別）'),(6,'獣');
/*!40000 ALTER TABLE `s_tenchijin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servant`
--

DROP TABLE IF EXISTS `servant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servant` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `hp` int(11) NOT NULL,
  `atk` int(11) NOT NULL,
  `card` varchar(255) NOT NULL,
  `b_hit` int(11) DEFAULT NULL,
  `a_hit` int(11) DEFAULT NULL,
  `q_hit` int(11) DEFAULT NULL,
  `ex_hit` int(11) DEFAULT NULL,
  `n_hit` int(11) DEFAULT NULL,
  `np_eff` float DEFAULT NULL,
  `np_damage` float DEFAULT NULL,
  `star` float DEFAULT NULL,
  `star_atsumare` float DEFAULT NULL,
  `illustrator` varchar(255) DEFAULT NULL,
  `cv` varchar(255) DEFAULT NULL,
  `rare_id` int(11) DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  `tenchijin_id` int(11) DEFAULT NULL,
  `sex_id` int(11) DEFAULT NULL,
  `seikaku_id` int(11) DEFAULT NULL,
  `houshin_id` int(11) DEFAULT NULL,
  `rare2_id` int(11) DEFAULT NULL,
  `color_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=265 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servant`
--

LOCK TABLES `servant` WRITE;
/*!40000 ALTER TABLE `servant` DISABLE KEYS */;
INSERT INTO `servant` VALUES (1,'マシュ・キリエライト',11516,7815,'QAABB',1,2,2,3,0,0.84,3,9.9,99,'武内崇','高橋李依',18,13,2,2,1,1,19,2),(2,'アルトリア・ペンドラゴン',15150,11221,'QAABB',1,2,2,3,1,0.86,3,10,102,'武内崇','川澄綾子',6,1,2,2,1,1,NULL,1),(3,'アルトリア・ペンドラゴン〔オルタ〕',11589,10248,'QAABB',1,2,2,3,3,0.86,3,9.9,99,'武内崇','川澄綾子',5,1,3,2,2,1,NULL,1),(4,'アルトリア・ペンドラゴン〔リリィ〕',10623,7726,'QAABB',1,2,2,3,8,0.86,3,10,102,'武内崇','川澄綾子',10,1,2,2,1,1,NULL,1),(5,'ネロ・クラウディウス',11753,9449,'QAABB',1,2,2,5,1,0.84,3,10.1,102,'ワダアルコ','丹下桜',13,1,3,2,1,2,NULL,2),(6,'ジークフリート',14165,8181,'QAABB',1,2,2,3,1,0.83,3,10,97,'近衛乙嗣','諏訪部順一',5,1,2,1,1,2,NULL,1),(7,'ガイウス・ユリウス・カエサル',9595,7497,'QQABB',1,2,2,3,10,1.1,3,10,99,'しまどりる','置鮎龍太郎',4,1,3,1,3,3,NULL,3),(8,'アルテラ',13907,12343,'QAABB',1,2,2,3,1,0.84,3,10.1,102,'huke','能登麻美子',6,1,3,2,1,2,NULL,1),(9,'ジル・ド・レェ',10498,6615,'QAABB',1,2,2,3,0,0.82,3,9.9,98,'真じろう','鶴岡聡',4,1,3,1,1,1,NULL,2),(10,'シュヴァリエ・デオン',13256,8765,'QAABB',1,2,2,3,0,0.83,3,10,102,'森山大輔','斎藤千和',5,1,3,3,3,3,NULL,2),(11,'エミヤ',11521,9398,'QAAAB',1,3,2,5,10,0.51,3,7.9,145,'武内崇','諏訪部順一',5,2,3,1,3,3,NULL,1),(12,'ギルガメッシュ',13097,12280,'QAABB',5,5,5,8,1,0.34,3,7.9,153,'武内崇','関智一',17,2,1,1,1,2,NULL,1),(13,'ロビンフッド',10187,6715,'QQAAB',1,2,3,3,1,0.87,3,8,150,'ワダアルコ','鳥海浩輔',4,2,3,1,1,3,NULL,2),(14,'アタランテ',12476,8633,'QQAAB',1,2,3,3,10,0.5,3,8,148,'輪くすさが','早見沙織',5,2,2,2,2,3,NULL,3),(15,'エウリュアレ',9506,7032,'QQAAB',1,2,3,3,1,0.9,3,7.9,156,'AKIRA','浅川悠',4,2,1,2,1,2,NULL,2),(16,'アーラシュ',7122,5816,'QAABB',1,2,3,3,1,0.84,3,8,147,'BLACK','鶴岡聡',2,2,2,1,3,2,NULL,1),(17,'クー・フーリン',9593,7239,'QQABB',1,2,2,3,1,1.07,4,12.1,87,'武内崇','神奈延年',4,3,1,1,3,1,NULL,3),(18,'エリザベート・バートリー',11870,9122,'QQABB',1,2,2,3,5,1.1,4,11.8,90,'ワダアルコ','大久保瑠美',5,3,3,2,2,2,NULL,1),(19,'武蔵坊弁慶',9149,5801,'QQAAB',1,2,2,3,0,0.79,4,11.9,89,'真じろう','稲田徹',3,3,3,1,1,2,NULL,2),(20,'クー・フーリン〔プロトタイプ〕',10098,7082,'QQABB',1,2,2,3,1,1.08,4,12.1,88,'中原','中井和哉',4,3,1,1,3,1,NULL,3),(21,'レオニダス一世',7959,6583,'QQABB',1,2,2,3,0,1.07,4,11.8,89,'縞うどん','三木眞一郎',3,3,3,1,3,1,NULL,1),(22,'ロムルス',9883,7239,'QQABB',1,2,2,3,7,1.07,4,12.1,90,'こやまひろかず','置鮎龍太郎',4,3,5,1,3,2,NULL,1),(23,'メドゥーサ',8937,7200,'QQAAB',1,3,2,3,1,0.58,3,9,194,'武内崇','浅川悠',4,5,2,2,1,2,NULL,3),(24,'ゲオルギウス',9200,5236,'QQAAB',1,2,2,3,4,0.85,3,8.9,205,'中央東口','西前忠久',3,5,3,1,1,1,NULL,2),(25,'エドワード・ティーチ',7907,6188,'QAABB',1,3,2,3,5,0.56,3,8.8,198,'Bすけ','西前忠久',3,5,3,1,2,2,NULL,1),(26,'ブーディカ',10130,6289,'QQAAB',1,2,2,3,0,0.85,3,8.9,196,'蒼月タカオ','斎藤千和',4,5,3,2,1,3,NULL,2),(27,'牛若丸',9028,7076,'QQAAB',1,2,2,3,1,0.87,3,9.1,204,'坂本みねぢ','早見沙織',4,5,3,2,3,2,NULL,3),(28,'アレキサンダー',8640,7356,'QQAAB',1,2,2,3,1,0.86,3,9,205,'BUNBUN','坂本真綾',4,5,3,1,1,3,NULL,3),(29,'マリー・アントワネット',12348,8293,'QQAAB',1,1,2,3,5,1,3,9,201,'ギンカ','種田梨沙',5,5,3,2,1,1,NULL,3),(30,'マルタ',13068,8014,'QAAAB',1,1,2,3,1,1.58,3,9,205,'坂本みねぢ','早見沙織',5,5,3,2,1,1,NULL,1),(31,'メディア',8643,7418,'QAAAB',1,1,2,3,1,1.64,3,10.9,50,'ネコタワワ','田中敦子',4,6,2,2,2,3,NULL,2),(32,'ジル・ド・レェ',9506,6514,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,12,6,3,1,2,2,NULL,1),(33,'ハンス・クリスチャン・アンデルセン',8484,5758,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3,6,3,1,3,1,NULL,2),(34,'ウィリアム・シェイクスピア',8080,5798,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3,6,3,1,3,3,NULL,1),(35,'メフィストフェレス',9216,6839,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,6,2,1,2,2,NULL,1),(36,'ヴォルフガング・アマデウス・モーツァルト',7129,5195,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,6,5,1,1,3,NULL,2),(37,'諸葛孔明〔エルメロイⅡ世〕',14259,10598,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,6,6,3,1,1,3,NULL,2),(38,'クー・フーリン',9604,6580,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,6,1,1,3,1,NULL,2),(39,'佐々木小次郎',6220,5735,'QQQAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,4,3,1,2,3,NULL,3),(40,'呪腕のハサン',7594,6280,'QQQAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3,4,3,1,2,1,NULL,3),(41,'ステンノ',11518,8985,'QQQAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,4,1,2,1,2,NULL,1),(42,'荊軻',8293,7207,'QQQAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,4,3,2,1,2,NULL,3),(43,'シャルル＝アンリ・サンソン',8309,5456,'QQQAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3,4,3,1,2,1,NULL,1),(44,'ファントム・オブ・ジ・オペラ',8393,5654,'QQQAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3,4,2,1,2,2,NULL,2),(45,'マタ・ハリ',6565,5377,'QQQAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,4,3,2,3,2,NULL,2),(46,'カーミラ',10473,9408,'QQQAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,4,2,2,2,2,NULL,1),(47,'ヘラクレス',10327,10655,'QABBB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,7,1,1,4,2,NULL,1),(48,'ランスロット',10327,10477,'QABBB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,7,2,1,4,1,NULL,3),(49,'呂布奉先',8302,8119,'QABBB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,7,3,1,2,2,NULL,1),(50,'スパルタクス',7722,5073,'QABBB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,7,3,1,3,3,NULL,1),(51,'坂田金時',12150,12712,'QABBB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,6,7,3,1,1,1,NULL,1),(52,'ヴラド三世',13770,11499,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,6,7,2,1,2,2,NULL,2),(53,'アステリオス',6604,6037,'QABBB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,7,2,1,2,2,NULL,2),(54,'カリギュラ',7303,6831,'QABBB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3,7,3,1,2,2,NULL,2),(55,'ダレイオス三世',8763,7608,'QABBB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,7,3,1,3,1,NULL,1),(56,'清姫',9166,6644,'QABBB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,7,2,2,2,2,NULL,1),(57,'エイリーク・ブラッドアクス',7688,6290,'QABBB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3,7,3,1,3,2,NULL,1),(58,'タマモキャット',11458,9026,'QABBB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,7,2,2,1,2,NULL,3),(59,'ジャンヌ・ダルク',16500,9593,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,6,8,4,2,1,1,NULL,2),(60,'オリオン',14553,11107,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,6,2,1,1,3,2,NULL,2),(61,'エリザベート・バートリー〔ハロウィン〕',11404,8616,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,6,3,2,2,2,NULL,1),(62,'玉藻の前',14259,10546,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,6,6,1,2,2,3,NULL,2),(63,'ダビデ',8643,7736,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,2,1,1,3,1,NULL,1),(64,'ヘクトール',10200,6928,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,3,3,1,3,1,NULL,1),(65,'フランシス・ドレイク',12830,11326,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,6,5,5,2,2,2,NULL,1),(66,'アン・ボニー＆メアリー・リード',11286,9029,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,5,3,2,3,2,NULL,3),(67,'メディア〔リリィ〕',13070,7766,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,13,6,2,2,1,1,NULL,2),(68,'沖田総司',13225,12068,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,1,3,2,3,3,NULL,3),(69,'織田信長',11637,9494,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,2,3,2,3,1,NULL,1),(70,'スカサハ',14825,11345,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,3,4,2,1,3,NULL,3),(71,'ディルムッド・オディナ',10098,6877,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,3,2,1,3,1,NULL,3),(72,'フェルグス・マック・ロイ',9786,7460,'QABBB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,1,2,1,3,1,NULL,1),(73,'アルトリア・ペンドラゴン〔サンタオルタ〕',11286,9258,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,5,3,2,1,1,NULL,1),(74,'ナーサリー・ライム',11882,8629,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,6,3,2,3,3,NULL,2),(75,'ジャック・ザ・リッパー',12695,11557,'QQQAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,6,4,2,2,2,2,NULL,3),(76,'モードレッド',14680,11723,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,6,1,2,2,3,2,NULL,1),(77,'ニコラ・テスラ',13825,11781,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,14,2,5,1,1,2,NULL,1),(78,'アルトリア・ペンドラゴン〔オルタ〕',11761,9968,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,13,3,1,2,1,1,NULL,1),(79,'ヴァン・ホーエンハイム・パラケルスス',9506,6711,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,6,3,1,1,2,NULL,2),(80,'チャールズ・バベッジ',10887,5996,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,6,3,1,3,2,NULL,1),(81,'ヘンリー・ジキル＆ハイド',9675,6320,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,4,2,1,1,1,NULL,1),(82,'フランケンシュタイン',10687,9441,'QABBB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,7,2,2,3,2,NULL,3),(83,'ソロモン',0,0,'-',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,6,5,1,2,2,NULL,NULL),(84,'アルジュナ',13230,12342,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,6,2,1,1,3,1,NULL,1),(85,'カルナ',13632,11976,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,6,3,1,1,1,1,NULL,1),(86,'謎のヒロインX',12696,11761,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,4,5,2,1,2,NULL,3),(87,'フィン・マックール',12750,8930,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,3,1,1,3,3,NULL,2),(88,'ブリュンヒルデ',14825,11432,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,3,1,2,1,3,NULL,1),(89,'ベオウルフ',10327,10247,'QABBB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,7,2,1,1,2,NULL,1),(90,'ネロ・クラウディウス〔ブライド〕',14248,11607,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,1,3,2,6,2,NULL,2),(91,'両儀式〔セイバー〕',15453,10721,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,1,3,2,3,3,NULL,2),(92,'両儀式〔アサシン〕',11055,8867,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,4,3,2,1,2,NULL,2),(93,'天草四郎',14107,10972,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,8,3,1,1,1,NULL,1),(94,'アストルフォ',11172,8937,'QQQAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,5,2,3,1,2,NULL,3),(95,'子ギル',8731,7696,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,2,1,1,1,2,NULL,1),(96,'巌窟王 エドモン・ダンテス',12177,12641,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,9,3,1,2,2,NULL,3),(97,'ナイチンゲール',15221,10184,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,6,7,3,2,1,1,NULL,2),(98,'クー・フーリン〔オルタ〕',12210,12805,'QABBB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,14,7,2,1,2,2,NULL,1),(99,'女王メイヴ',13968,10296,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,14,5,2,2,2,2,NULL,1),(100,'エレナ・ブラヴァツキー',11882,8629,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,6,3,2,1,2,NULL,2),(101,'ラーマ',11993,9854,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,1,1,1,1,1,NULL,1),(102,'神槍 李書文',11360,9653,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,3,3,1,2,3,NULL,2),(103,'トーマス・エジソン',11882,7952,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,13,6,3,1,3,1,NULL,2),(104,'ジェロニモ',9123,6857,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,6,3,1,1,3,NULL,2),(105,'ビリー・ザ・キッド',9506,6890,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,2,3,1,3,2,NULL,3),(106,'ジャンヌ・ダルク〔オルタ〕',11761,13244,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,9,3,2,2,2,NULL,1),(107,'アンリマユ',7981,5683,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,9,3,1,2,2,NULL,2),(108,'イスカンダル',13219,11560,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,5,3,1,1,3,NULL,1),(109,'エミヤ〔アサシン〕',11168,8958,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,4,3,1,2,2,NULL,2),(110,'百貌のハサン',9310,6686,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,4,3,2,2,1,NULL,2),(111,'アイリスフィール〔天の衣〕',12476,8237,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,6,1,2,1,2,NULL,2),(112,'酒呑童子',12825,11993,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,4,2,2,2,2,NULL,2),(113,'玄奘三蔵',12965,11658,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,6,6,3,2,1,1,NULL,1),(114,'源頼光',13500,11556,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,7,1,2,1,2,NULL,1),(115,'坂田金時〔ライダー〕',10800,9819,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,5,2,1,1,1,NULL,3),(116,'茨木童子',10954,9636,'QABBB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,7,2,2,2,2,NULL,1),(117,'風魔小太郎',8844,7091,'QQQAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,4,3,1,2,2,NULL,3),(118,'オジマンディアス',12830,11971,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,6,5,1,1,3,2,NULL,1),(119,'アルトリア・ペンドラゴン',15606,10995,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,14,3,1,2,1,1,NULL,1),(120,'ニトクリス',11288,9060,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,6,2,2,1,1,NULL,2),(121,'ランスロット',11589,9949,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,1,2,1,1,1,NULL,2),(122,'トリスタン',11637,9735,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,13,2,2,1,1,1,NULL,3),(123,'ガウェイン',11419,10173,'QABBB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,13,1,2,1,1,1,NULL,1),(124,'静謐のハサン',9310,6636,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,4,3,2,2,1,NULL,2),(125,'俵藤太',9800,7032,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,2,3,1,1,3,NULL,1),(126,'ベディヴィエール',9595,7627,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,12,1,4,1,1,1,NULL,1),(127,'レオナルド・ダ・ヴィンチ',14259,10598,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,6,4,2,1,2,NULL,2),(128,'玉藻の前〔ランサー〕',15147,10726,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,3,1,2,5,3,NULL,1),(129,'アルトリア・ペンドラゴン〔アーチャー〕',14553,11276,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,2,2,2,1,1,NULL,2),(130,'マリー・アントワネット〔キャスター〕',11404,9060,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,16,6,3,2,1,1,NULL,2),(131,'アン・ボニー＆メアリー・リード〔アーチャー〕',11521,9446,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,16,2,3,2,3,2,NULL,1),(132,'モードレッド〔ライダー〕',11400,9212,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,16,5,2,2,1,2,NULL,2),(133,'スカサハ〔アサシン〕',11168,9049,'QQQAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,4,4,2,1,3,NULL,3),(134,'清姫〔ランサー〕',11870,8936,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,16,3,2,2,2,2,NULL,1),(135,'マルタ〔ルーラー〕',11250,9546,'QABBB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,16,8,3,2,1,1,NULL,1),(136,'イリヤスフィール・フォン・アインツベルン',13825,10857,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,6,3,2,1,3,NULL,1),(137,'クロエ・フォン・アインツベルン',10914,9845,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,2,1,2,1,2,NULL,2),(138,'エリザベート・バートリー〔ブレイブ〕',11248,9899,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,1,2,2,1,2,NULL,1),(139,'クレオパトラ',13402,11088,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,4,3,2,3,1,NULL,1),(140,'ヴラド三世〔EXTRA〕',13005,8775,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,3,3,1,1,1,NULL,1),(141,'ジャンヌ・ダルク・オルタ・サンタ・リリィ',11870,9261,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,3,3,2,1,2,NULL,1),(142,'イシュタル',13965,12252,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,2,1,2,1,1,NULL,1),(143,'エルキドゥ',15300,10780,'QQQAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,6,3,1,3,3,3,NULL,1),(144,'ケツァル・コアトル',12960,12001,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,6,5,1,2,1,1,NULL,1),(145,'ギルガメッシュ〔キャスター〕',12005,8460,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,6,3,1,1,1,NULL,2),(146,'メドゥーサ〔ランサー〕',13119,8253,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,3,2,2,1,3,NULL,3),(147,'ゴルゴーン',10197,10706,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,9,2,2,2,2,NULL,1),(148,'ジャガーマン',9593,7022,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,12,3,2,2,3,2,NULL,1),(149,'ティアマト',0,0,'-',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,14,6,4,2,2,NULL,NULL),(150,'マーリン',14259,10546,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,6,2,1,1,1,NULL,2),(151,'ゲーティア',0,0,'-',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,14,6,4,2,2,NULL,NULL),(152,'ソロモン',0,0,'-',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,6,5,1,1,3,NULL,NULL),(153,'宮本武蔵',13635,12037,'QABBB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,1,3,2,1,2,NULL,1),(154,'\"山の翁\"',13338,11848,'QABBB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,4,3,1,2,1,NULL,1),(155,'謎のヒロインＸ〔オルタ〕',14175,11113,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,7,5,2,2,3,NULL,3),(156,'ジェームズ・モリアーティ',13685,11781,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,2,3,1,2,2,NULL,1),(157,'エミヤ〔オルタ〕',12250,8996,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,2,3,1,2,2,NULL,2),(158,'ヘシアン・ロボ',9949,10628,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,9,2,1,2,2,NULL,3),(159,'燕青',11637,8661,'QQQAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,4,3,1,2,2,NULL,3),(160,'アーサー・ペンドラゴン〔プロトタイプ〕',13975,12465,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,1,2,1,1,1,NULL,1),(161,'土方歳三',12028,12089,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,7,3,1,2,1,NULL,1),(162,'茶々',11025,8945,'QABBB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,7,3,2,3,2,NULL,1),(163,'メルトリリス',13402,11692,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,10,2,2,1,1,NULL,3),(164,'パッションリップ',10901,10299,'QABBB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,16,10,2,2,3,1,NULL,1),(165,'鈴鹿御前',11753,9544,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,1,1,2,2,3,NULL,1),(166,'BB',13643,8197,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,11,3,2,1,2,NULL,2),(167,'殺生院キアラ',14606,11668,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,10,6,2,2,2,NULL,2),(168,'ビーストⅢ／Ｒ',0,0,'-',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,14,6,2,2,2,NULL,NULL),(169,'シェヘラザード',15846,9212,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,6,6,3,2,3,1,NULL,2),(170,'武則天',10942,8981,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,4,3,2,2,1,NULL,3),(171,'ペンテシレイア',10175,10502,'QABBB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,7,2,2,1,1,NULL,1),(172,'クリストファー・コロンブス',9600,6552,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,12,5,3,1,2,3,NULL,1),(173,'シャーロック・ホームズ',13365,11495,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,8,5,1,1,3,NULL,2),(174,'ポール・バニヤン',6196,6044,'QABBB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,7,7,2,2,3,3,NULL,1),(175,'ネロ・クラウディウス〔キャスター〕',13685,10857,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,6,3,2,5,2,NULL,1),(176,'フランケンシュタイン〔セイバー〕',11993,9353,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,16,1,2,2,5,3,NULL,3),(177,'ニトクリス〔アサシン〕',11518,8812,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,16,4,1,2,1,1,NULL,2),(178,'織田信長〔バーサーカー〕',10023,10146,'QABBB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,16,7,3,2,5,2,NULL,1),(179,'アルトリア・ペンドラゴン〔オルタ〕',14256,10776,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,5,3,2,1,1,NULL,3),(180,'エレナ・ブラヴァツキー',11404,9446,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,16,2,3,2,1,2,NULL,2),(181,'源頼光',12112,9168,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,16,3,1,2,1,1,NULL,1),(182,'イシュタル〔ライダー〕',10692,9603,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,5,1,2,1,1,NULL,3),(183,'パールヴァティー',13253,8127,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,3,1,2,1,1,NULL,3),(184,'巴御前',10804,9946,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,2,2,2,3,1,NULL,1),(185,'望月千代女',11637,8510,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,4,2,2,2,2,NULL,2),(186,'宝蔵院胤舜',9996,6791,'QQQAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,3,3,1,3,3,NULL,2),(187,'柳生但馬守宗矩',11135,9999,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,13,1,3,1,3,1,NULL,2),(188,'加藤段蔵',11055,8935,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,4,2,2,3,3,NULL,1),(189,'刑部姫',13822,10824,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,6,4,2,2,3,2,NULL,3),(190,'メカエリチャン',10901,9997,'QABBB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,10,3,2,1,1,NULL,1),(191,'メカエリチャンⅡ号機',10901,9997,'QABBB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,10,3,2,1,1,NULL,1),(192,'キルケー',12250,8671,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,6,1,2,3,2,NULL,1),(193,'哪吒',12112,9284,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,3,1,2,1,3,NULL,1),(194,'シバの女王',12127,8629,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,6,3,2,1,3,NULL,2),(195,'アビゲイル・ウィリアムズ',13770,12100,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,12,2,2,2,2,NULL,1),(196,'エレシュキガル',16065,10343,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,3,2,2,2,2,NULL,1),(197,'アルテラ・ザ・サン〔タ〕',11637,9759,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,2,5,2,1,2,NULL,3),(198,'葛飾北斎',13230,12100,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,12,3,2,3,2,NULL,2),(199,'セミラミス',13266,11309,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,4,2,2,2,1,NULL,1),(200,'浅上藤乃',11025,10299,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,16,2,3,2,2,1,NULL,1),(201,'アナスタシア',14259,10546,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,6,6,3,2,3,3,NULL,2),(202,'アタランテ〔オルタ〕',10634,9806,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,7,2,2,2,2,NULL,3),(203,'アヴィケブロン',9981,6376,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,6,3,1,3,1,NULL,1),(204,'アントニオ・サリエリ',7840,8125,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,12,9,2,1,2,2,NULL,2),(205,'イヴァン雷帝',13284,11619,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,5,3,1,2,1,NULL,1),(206,'アキレウス',13219,11883,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,6,5,2,1,3,1,NULL,3),(207,'ケイローン',12250,9294,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,2,1,1,1,1,NULL,2),(208,'ジーク',11288,8394,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,6,3,1,1,3,NULL,2),(209,'沖田総司〔オルタ〕',12696,12465,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,10,3,2,3,3,NULL,1),(210,'岡田以蔵',8844,6879,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,15,4,3,1,2,3,NULL,2),(211,'坂本龍馬',11880,8555,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,5,3,1,3,3,NULL,2),(212,'ナポレオン',13097,12033,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,6,2,5,1,1,3,NULL,1),(213,'シグルド',13975,12465,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,1,2,1,1,3,NULL,1),(214,'ワルキューレ',14025,8037,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,3,1,2,1,1,NULL,3),(215,'スカサハ＝スカディ',14406,10753,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,6,1,2,1,2,NULL,2),(216,'ジャンヌ・ダルク',15743,10525,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,2,3,2,5,1,NULL,2),(217,'茨木童子',12354,9133,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,16,3,2,2,2,2,NULL,1),(218,'牛若丸',10580,9456,'QQQAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,16,4,3,2,5,3,NULL,3),(219,'ジャンヌ・ダルク〔オルタ〕',9922,10298,'QABBB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,7,3,2,5,2,NULL,1),(220,'BB',14812,11182,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,11,2,2,2,2,NULL,1),(221,'女王メイヴ',13609,8017,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,16,1,2,2,2,2,NULL,2),(222,'謎のヒロインXX',11250,9751,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,16,12,5,2,1,1,NULL,2),(223,'ディルムッド・オディナ',11362,10048,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,16,1,2,1,3,1,NULL,3),(224,'シトナイ',13965,11668,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,10,1,2,1,2,NULL,2),(225,'酒呑童子',11025,9538,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,6,2,2,2,2,NULL,1),(226,'項羽',13770,11613,'QABBB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,6,7,3,1,3,1,NULL,3),(227,'蘭陵王',12325,9112,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,1,3,1,1,1,NULL,2),(228,'秦良玉',13387,8295,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,3,3,2,1,1,NULL,2),(229,'始皇帝',15828,9977,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,8,3,3,1,1,NULL,2),(230,'虞美人',13389,7970,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,4,2,2,2,1,NULL,1),(231,'赤兎馬',10483,6434,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,15,5,2,1,3,3,NULL,3),(232,'ブラダマンテ',15682,10833,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,14,3,2,2,1,1,NULL,3),(233,'ケツァル・コアトル〔サンバ／サンタ〕',11306,9687,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,8,1,2,1,1,NULL,1),(234,'紅閻魔',13960,11607,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,1,2,2,1,1,NULL,2),(235,'李書文',12568,11470,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,14,4,3,1,2,3,NULL,2),(236,'美遊・エーデルフェルト',12005,8629,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,16,6,3,2,1,1,NULL,2),(237,'紫式部',12833,11374,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,6,3,2,1,3,NULL,2),(238,'キングプロテア',13338,12835,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,10,2,2,1,1,NULL,1),(239,'カーマ',12889,11528,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,4,1,2,2,2,NULL,3),(240,'ビーストⅢ／Ｌ',0,0,'-',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,14,6,2,2,2,NULL,NULL),(241,'司馬懿〔ライネス〕',13543,11427,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,5,3,2,3,3,NULL,2),(242,'アストライア',11531,9734,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,8,1,2,1,1,NULL,2),(243,'グレイ',10580,9456,'QQQAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,4,3,2,1,1,NULL,1),(244,'大いなる石像神',17844,9166,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,14,11,1,2,1,1,NULL,2),(245,'ラクシュミー・バーイー',11362,9949,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,13,1,3,2,1,1,NULL,3),(246,'ウィリアム・テル',9310,7384,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,2,3,1,1,1,NULL,2),(247,'アルジュナ〔オルタ〕',13837,11669,'QABBB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,7,1,1,7,1,NULL,1),(248,'アシュヴァッターマン',11245,10249,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,13,2,1,1,3,2,NULL,1),(249,'アスクレピオス',10084,6376,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,12,6,2,1,3,3,NULL,2),(250,'魔王信長',11761,12641,'QABBB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,9,2,2,3,2,NULL,1),(251,'森長可',8019,7732,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,15,7,3,1,4,2,NULL,1),(252,'長尾景虎',11360,9617,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,3,3,2,1,1,NULL,1),(253,'レオナルド・ダ・ヴィンチ',14112,10883,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,5,3,2,1,1,NULL,2),(254,'イアソン',7575,5457,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,1,2,1,1,1,NULL,2),(255,'パリス',7834,6523,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3,2,2,1,3,3,NULL,3),(256,'ガレス',9537,5413,'QQABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3,3,2,2,1,1,NULL,1),(257,'バーソロミュー・ロバーツ',6720,5461,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,5,3,1,2,2,NULL,3),(258,'陳宮',7755,6119,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3,6,3,1,1,2,NULL,2),(259,'シャルロット・コルデー',6220,5488,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,4,3,2,1,3,NULL,2),(260,'サロメ',6885,6884,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3,7,2,2,2,2,NULL,2),(261,'宮本武蔵',12150,12712,'QAABB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,7,3,2,1,2,NULL,2),(262,'刑部姫',12476,8895,'QAAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,16,2,2,2,5,3,NULL,1),(263,'カーミラ',10476,9651,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,16,5,2,2,2,3,NULL,3),(264,'葛飾北斎',11873,9389,'QQAAB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,1,3,2,1,2,NULL,2);
/*!40000 ALTER TABLE `servant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servant_skill`
--

DROP TABLE IF EXISTS `servant_skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servant_skill` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `ct` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=157 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servant_skill`
--

LOCK TABLES `servant_skill` WRITE;
/*!40000 ALTER TABLE `servant_skill` DISABLE KEYS */;
INSERT INTO `servant_skill` VALUES (1,'カリスマE',5),(2,'カリスマC',5),(3,'カリスマC+',5),(4,'カリスマB-',5),(5,'カリスマB',5),(6,'カリスマA',5),(7,'カリスマA+',5),(8,'直感C',5),(9,'直感C+',5),(10,'直感B',5),(11,'直感A',5),(12,'黄金律E',6),(13,'黄金律C-',6),(14,'黄金律B',6),(15,'黄金律A',6),(16,'千里眼C',6),(17,'千里眼C+',6),(18,'千里眼B',6),(19,'千里眼A',6),(20,'心眼（偽）C',6),(21,'心眼（偽）B-',6),(22,'心眼（偽）B',6),(23,'心眼（偽）A',6),(24,'心眼（真）C',6),(25,'心眼（真）B',6),(26,'心眼（真）A',6),(27,'軍略C',5),(28,'軍略C+',5),(29,'軍略B',5),(30,'魔力放出A-',5),(31,'魔力放出A',5),(32,'魔力放出A+',5),(33,'皇帝特権B',5),(34,'皇帝特権A',5),(35,'皇帝特権EX',5),(36,'無辜の怪物D',5),(37,'無辜の怪物B',5),(38,'無辜の怪物A',5),(39,'変化C',5),(40,'変化B',5),(41,'変化A',5),(42,'変化A+',5),(43,'今は脆き雪花の壁',5),(44,'誉れ堅き雪花の壁',5),(45,'時に煙る白亜の壁',7),(46,'奮い断つ決意の盾',6),(47,'バンカーボルトA',4),(48,'アマルガムゴートD',5),(49,'悲壮なる奮起の盾',7),(50,'輝ける路EX',5),(51,'花の旅路EX',5),(52,'頭痛持ちB',5),(53,'富の杯B',5),(54,'三度、落陽を迎えてもA',10),(55,'仕切り直しC',5),(56,'仕切り直しB',5),(57,'仕切り直しA',5),(58,'戦闘続行B',7),(59,'戦闘続行A',7),(60,'戦闘続行A+',7),(61,'戦闘続行EX',7),(62,'竜殺しA',5),(63,'竜殺しA++',5),(64,'扇動EX',5),(65,'扇動EX（強化後）',5),(66,'プレラーティの激励B',5),(67,'自己暗示A',5),(68,'麗しの風貌C',5),(69,'鷹の瞳B+',6),(70,'魔術C-',5),(71,'魔術C',5),(72,'魔術B',5),(73,'投影魔術C',5),(74,'投影魔術B',5),(75,'投影魔術A',5),(76,'コレクターEX',5),(77,'バビロンの蔵EX',5),(78,'破壊工作A',5),(79,'皐月の王B',6),(80,'アルカディア越えB',5),(81,'アルカディア越えA',5),(82,'追い込みの美学C',4),(83,'カリュドーン狩りA',6),(84,'吸血C',6),(85,'吸血A',6),(86,'魅惑の美声C',7),(87,'魅惑の美声B',7),(88,'魅惑の美声A',7),(89,'女神のきまぐれA（エウリュアレ）',5),(90,'女神のきまぐれA（ステンノ）',5),(91,'頑健EX',5),(92,'弓矢作成A',6),(93,'矢避けの加護C',5),(94,'矢避けの加護B',5),(95,'矢避けの加護A',5),(96,'嗜虐のカリスマA',5),(97,'拷問技術A',5),(98,'怨霊調伏A',5),(99,'仁王立ちB',5),(100,'白紙の勧進帳',8),(101,'ルーン魔術B',5),(102,'ルーン魔術A',5),(103,'獣殺しB+',5),(104,'獣殺しB++',5),(105,'殿の矜持A',6),(106,'戦士の雄叫びB',5),(107,'天性の肉体D',5),(108,'天性の肉体C',5),(109,'天性の肉体A',5),(110,'天性の肉体EX',5),(111,'七つの丘A',7),(112,'魔眼B+',6),(113,'魔眼A+',6),(114,'魔眼A++',6),(115,'怪力B（ジキル）',5),(116,'怪力C-',5),(117,'怪力C',5),(118,'怪力B',5),(119,'怪力A',5),(120,'怪力EX',5),(121,'鮮血神殿B',6),(122,'守護騎士A+',5),(123,'殉教者の魂B+',5),(124,'嵐の航海者B',5),(125,'嵐の航海者A',5),(126,'嵐の航海者A+',5),(127,'海賊の誉れC+',5),(128,'海賊の誉れB',5),(129,'紳士的な愛C',6),(130,'女神への誓いB',5),(131,'アンドラスタの加護A',5),(132,'天狗の兵法A',5),(133,'天狗の兵法EX',5),(134,'燕の早業B',5),(135,'紅顔の美少年C',7),(136,'紅顔の美少年B',7),(137,'覇王の兆しA',5),(138,'麗しの姫君A',6),(139,'神の恩寵B',5),(140,'信仰の加護A',5),(141,'信仰の加護A+++',5),(142,'奇蹟D',6),(143,'奇蹟D+',6),(144,'聖女の誓いC',5),(145,'高速神言B',7),(146,'高速神言A',7),(147,'高速詠唱E',6),(148,'高速詠唱B+',8),(149,'高速詠唱A',8),(150,'金羊の皮',4),(151,'キルケーの教えA',6),(152,'星の紋章EX',5),(153,'神の鞭A',5),(154,'浪漫の風B',5),(155,'勝利の女神A',5),(156,'紅顔の美少年（雷）A',7);
/*!40000 ALTER TABLE `servant_skill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servant_skill_effect`
--

DROP TABLE IF EXISTS `servant_skill_effect`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servant_skill_effect` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `skill_id` int(11) NOT NULL,
  `effect_text` varchar(255) NOT NULL,
  `effect_genre` int(11) NOT NULL,
  `turn` int(11) DEFAULT NULL,
  `bord` int(11) NOT NULL,
  `effect_para_1` float DEFAULT NULL,
  `effect_para_2` float DEFAULT NULL,
  `effect_para_3` float DEFAULT NULL,
  `effect_para_4` float DEFAULT NULL,
  `effect_para_5` float DEFAULT NULL,
  `effect_para_6` float DEFAULT NULL,
  `effect_para_7` float DEFAULT NULL,
  `effect_para_8` float DEFAULT NULL,
  `effect_para_9` float DEFAULT NULL,
  `effect_para_10` float DEFAULT NULL,
  `target_id` int(11) DEFAULT NULL,
  `fore` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=259 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servant_skill_effect`
--

LOCK TABLES `servant_skill_effect` WRITE;
/*!40000 ALTER TABLE `servant_skill_effect` DISABLE KEYS */;
INSERT INTO `servant_skill_effect` VALUES (1,1,'味方全体の攻撃力をアップ',1,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,12,3,1),(2,2,'味方全体の攻撃力をアップ',1,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,16,3,1),(3,3,'味方全体の攻撃力をアップ',1,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,3,1),(4,4,'味方全体の攻撃力をアップ',1,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17.8,3,1),(5,5,'味方全体の攻撃力をアップ',1,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,18,3,1),(6,6,'味方全体の攻撃力をアップ',1,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,3,1),(7,7,'味方全体の攻撃力をアップ',1,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,21,3,1),(8,8,'スターを大量獲得',20,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,14,3,1),(9,9,'スターを大量獲得',20,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,14,3,1),(10,10,'スターを大量獲得',20,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,14,3,1),(11,11,'スターを大量獲得',20,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,15,3,1),(12,12,'自身のNP獲得量をアップ',11,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,30,1,1),(13,13,'自身のNP獲得量をアップ',11,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,40,1,1),(14,14,'自身のNP獲得量をアップ',11,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,45,1,1),(15,15,'自身のNP獲得量をアップ',11,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,50,1,1),(16,16,'自身のスター発生率をアップ',10,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,32,1,1),(17,17,'自身のスター発生率をアップ',10,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,1,1),(18,18,'自身のスター発生率をアップ',10,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,38,1,1),(19,19,'自身のスター発生率をアップ',10,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,40,1,1),(20,20,'自身に回避状態を付与',22,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1),(21,21,'自身に回避状態を付与',22,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1),(22,22,'自身に回避状態を付与',22,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1),(23,23,'自身に回避状態を付与',22,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1),(24,20,'＆クリティカル威力をアップ',9,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,32,1,1),(25,21,'＆クリティカル威力をアップ',9,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,34,1,1),(26,22,'＆クリティカル威力をアップ',9,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,1,1),(27,23,'＆クリティカル威力をアップ',9,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,40,1,1),(28,24,'自身に回避状態を付与',22,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1),(29,25,'自身に回避状態を付与',22,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1),(30,26,'自身に回避状態を付与',22,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1),(31,24,'＆防御力をアップ',28,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,16,1,1),(32,25,'＆防御力をアップ',28,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,18,1,1),(33,26,'＆防御力をアップ',28,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,1,1),(34,27,'味方全体の宝具威力をアップ',5,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,16,3,1),(35,28,'味方全体の宝具威力をアップ',5,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,3,1),(36,29,'味方全体の宝具威力をアップ',5,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,18,3,1),(37,30,'自身のBusterカード性能をアップ',2,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,45,1,1),(38,31,'自身のBusterカード性能をアップ',2,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,50,1,1),(39,32,'自身のBusterカード性能をアップ',2,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,55,1,1),(40,33,'自身の攻撃力をアップ 確率60%',1,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,38,1,1),(41,34,'自身の攻撃力をアップ 確率60%',1,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,40,1,1),(42,35,'自身の攻撃力をアップ 確率60%',1,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,44,1,1),(43,33,'＆防御力をアップ 確率60%',28,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,38,1,1),(44,34,'＆防御力をアップ 確率60%',28,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,40,1,1),(45,35,'＆防御力をアップ 確率60%',28,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,44,1,1),(46,33,'＆HPを回復',25,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2800,1,1),(47,34,'＆HPを回復',25,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3000,1,1),(48,35,'＆HPを回復',25,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3400,1,1),(49,36,'自身に毎ターンスター獲得状態を付与',20,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,9,1,1),(50,37,'自身に毎ターンスター獲得状態を付与',20,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,9,1,1),(51,38,'自身に毎ターンスター獲得状態を付与',20,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,1,1),(52,36,'＆防御力をダウン【デメリット】',28,3,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,14,1,1),(53,37,'＆防御力をダウン【デメリット】',28,3,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,18,1,1),(54,38,'＆ターゲット集中状態を付与',23,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1),(55,39,'自身の防御力をアップ',28,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,24,1,1),(56,40,'自身の防御力をアップ',28,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,27,1,1),(57,41,'自身の防御力を大アップ',28,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,30,1,1),(58,42,'自身の防御力を大アップ',28,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,30,1,1),(59,41,'＆自身の防御力をアップ',28,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,30,1,1),(60,42,'＆自身の防御力をアップ',28,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,30,1,1),(61,42,'＆弱体耐性をアップ',13,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,40,1,1),(62,43,'味方全体の防御力をアップ',28,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,15,3,1),(63,44,'味方全体の防御力をアップ',28,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,3,1),(64,44,'＆被ダメージカット状態を付与(1回)',30,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2000,3,1),(65,45,'味方単体に無敵状態を付与',22,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,1),(66,45,'＆NPを増やす',12,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,2,1),(67,46,'自身にターゲット集中状態を付与',23,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1),(68,46,'＆NP獲得量を大アップ',11,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,400,1,1),(69,47,'自身のBusterカード性能をアップ(1回）',2,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,50,1,1),(70,47,'＆Buster攻撃のクリティカル威力をアップ(1回）',9,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,50,1,1),(71,48,'自身にターゲット集中状態を付与',23,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,300,1,1),(72,48,'＆NPを増やす',12,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,1,1),(73,49,'自身に無敵状態を付与',22,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1),(74,49,'＆ターゲット集中状態を付与',23,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,300,1,1),(75,49,'＆HPを減らす【デメリット】',25,NULL,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,500,1,1),(76,50,'自身のNPを増やす',12,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,30,1,1),(77,50,'＆スターを大量獲得',20,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,15,1,1),(78,51,'味方全体のNP獲得量をアップ',11,2,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,3,1),(79,52,'自身の精神異常耐性をアップ',34,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,100,1,1),(80,52,'＆HPを回復',25,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2000,1,1),(81,53,'自身のNPを増やす',12,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,30,1,1),(82,53,'＆〔水辺〕または〔都市〕のフィールドの時、Artsカード性能をアップ',3,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,1,1),(83,53,'＆精神異常耐性をアップ',34,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,100,1,1),(84,54,'自身にガッツ状態(3回)を付与',27,5,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,600,1,1),(85,55,'自身の弱体状態を解除',33,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1),(86,56,'自身の弱体状態を解除',33,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1),(87,57,'自身の弱体状態を解除',33,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1),(88,55,'＆HPを回復',25,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1500,1,1),(89,56,'＆HPを回復',25,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2000,1,1),(90,57,'＆HPを回復',25,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2500,1,1),(91,58,'自身にガッツ状態を付与（1回）',27,4,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2000,1,1),(92,59,'自身にガッツ状態を付与（1回）',27,5,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2500,1,1),(93,60,'自身にガッツ状態を付与（1回）',27,5,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2700,1,1),(94,61,'自身にガッツ状態を付与（1回）',27,5,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5000,1,1),(95,62,'自身に〔竜〕特攻状態を付与',29,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,80,1,1),(96,63,'自身に〔竜〕特攻状態を付与',29,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,80,1,1),(97,62,'＆〔竜〕特防状態を付与',28,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,30,1,1),(98,63,'＆〔竜〕特防状態を付与',28,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,30,1,1),(99,63,'＆Busterカード性能をアップ',2,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,50,1,1),(100,64,'味方単体のクリティカル威力をアップ',9,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,40,2,1),(101,65,'味方単体のクリティカル威力をアップ',9,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,40,2,1),(102,64,'＆防御力をダウン【デメリット】',28,3,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,2,1),(103,65,'＆スター発生率をアップ',10,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,100,2,1),(104,65,'＆防御力をダウン【デメリット】',28,3,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,2,1),(105,66,'自身のBusterカード性能をアップ',2,5,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,40,1,1),(106,67,'自身の弱体状態を解除',33,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1),(107,67,'＆弱体耐性を大アップ',13,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,100,1,1),(108,68,'自身にターゲット集中状態を付与',23,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1),(109,68,'＆HPを大回復',25,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2500,1,1),(110,69,'自身のスター発生率をアップ',10,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,100,1,1),(111,69,'＆クリティカル威力をアップ',9,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,100,1,1),(112,70,'自身のArtsカード性能をアップ',3,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,35,1,1),(113,71,'自身のArtsカード性能をアップ',3,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,1,1),(114,72,'自身のArtsカード性能をアップ',3,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,40,1,1),(115,73,'自身のArtsカード性能をアップ',3,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,30,1,1),(116,74,'自身のArtsカード性能をアップ',3,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,35,1,1),(117,75,'自身のArtsカード性能をアップ',3,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,40,1,1),(118,73,'＆Quickカード性能をアップ',4,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,30,1,1),(119,74,'＆Quickカード性能をアップ',4,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,35,1,1),(120,75,'＆Quickカード性能をアップ',4,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,40,1,1),(121,73,'＆Busterカード性能をアップ',2,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,30,1,1),(122,74,'＆Busterカード性能をアップ',2,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,35,1,1),(123,75,'＆Busterカード性能をアップ',2,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,40,1,1),(124,76,'自身のスター集中度を大アップ',19,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,600,1,1),(125,77,'自身のスター集中度を大アップ',19,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,600,1,1),(126,77,'＆NPを増やす',12,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,30,1,1),(127,78,'敵全体の攻撃力をダウン',1,3,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,15,3,2),(128,78,'＆毒状態を付与',15,5,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,500,3,2),(129,79,'自身に必中状態を付与',18,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1),(130,79,'＆回避状態を付与',22,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1),(131,79,'＆スターを獲得',20,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,1,1),(132,80,'味方全体のQuickカード性能をアップ',4,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,36,3,1),(133,81,'味方全体のQuickカード性能をアップ',4,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,50,3,1),(134,82,'自身のスター集中度を大アップ',19,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1000,1,1),(135,83,'自身に回避状態を付与',22,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1),(136,83,'＆NP獲得量をアップ',11,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,50,1,1),(137,84,'敵単体のチャージを中確率で減らす',11,NULL,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,80,2,2),(138,85,'敵単体のチャージを中確率で減らす',11,NULL,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,100,2,2),(139,84,'＋自身のNPを増やす',12,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,27,1,1),(140,85,'＋自身のNPを増やす',12,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,30,1,1),(141,86,'敵単体〔異性〕に確率で魅了付与',17,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,80,7,2),(142,87,'敵単体〔異性〕に確率で魅了付与',17,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,90,7,2),(143,88,'敵単体〔異性〕に確率で魅了付与',17,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,100,7,2),(144,89,'自身のArtsカード性能をアップ',3,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,30,1,1),(145,90,'味方全体の攻撃力をアップ',1,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,3,1),(146,90,'＆〔神性〕特性の味方全体の攻撃力をアップ',1,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,36,1),(147,91,'自身の防御力をアップ',28,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,1,1),(148,91,'＆毒耐性をアップ',13,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,160,1,1),(149,92,'自身のNPを増やす',12,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,30,1,1),(150,92,'＆HPを回復',25,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3000,1,1),(151,93,'自身に回避状態を付与(2回)',22,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1),(152,94,'自身に回避状態を付与(3回)',22,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1),(153,95,'自身に回避状態を付与(3回)',22,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1),(154,93,'＆防御力をアップ',28,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,14,1,1),(155,94,'＆防御力をアップ',28,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,16,1,1),(156,95,'＆防御力をアップ',28,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,18,1,1),(157,96,'味方全体の攻撃力をアップ',1,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,3,1),(158,96,'＋自身を除く味方全体の〔女性〕の攻撃力をアップ',1,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,9,1),(159,97,'敵単体の防御力をダウン',28,3,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,2,2),(160,98,'敵単体に確率でスキル封印状態を付与',32,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,100,2,2),(161,99,'自身にターゲット集中状態を付与',23,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1),(162,99,'＆防御力をアップ大',28,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,60,1,1),(163,100,'敵全体に宝具封印状態を付与',12,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,80,3,2),(164,101,'自身のクリティカル威力をアップ',9,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,45,1,1),(165,102,'自身のクリティカル威力をアップ',9,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,50,1,1),(166,101,'＆弱体耐性をアップ',13,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,45,1,1),(167,102,'＆弱体耐性をアップ',13,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,50,1,1),(168,103,'自身に〔猛獣〕特攻状態を付与',29,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,60,1,1),(169,104,'自身に〔猛獣〕特攻状態を付与',29,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,100,1,1),(170,104,'＆スター集中度をアップ',19,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,500,1,1),(171,105,'自身にターゲット集中状態を付与',23,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1),(172,105,'＆NP獲得量をアップ',11,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,100,1,1),(173,106,'味方全体のBusterカード性能をアップ',2,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,25,3,1),(174,107,'自身の攻撃弱体耐性をアップ',13,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,80,1,1),(175,108,'自身の攻撃弱体耐性をアップ',13,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,100,1,1),(176,109,'自身の攻撃弱体耐性をアップ',13,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,120,1,1),(177,110,'自身の攻撃弱体耐性をアップ',13,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,120,1,1),(178,107,'＆HPを大回復',25,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2500,1,1),(179,108,'＆HPを大回復',25,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2500,1,1),(180,109,'＆HPを大回復',25,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3000,1,1),(181,110,'＆HPを大回復',25,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3000,1,1),(182,110,'＆スター集中度をアップ',19,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,300,1,1),(183,111,'味方単体にガッツ状態を付与（1回）',27,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1000,2,1),(184,111,'＆Busterカード性能をアップ',2,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,30,2,1),(185,112,'敵単体の〔男性〕を確率で魅了状態にする',17,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,100,5,2),(186,113,'敵単体を確率で行動不能状態にする',17,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,100,2,2),(187,114,'敵単体を確率で行動不能状態にする',17,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,105,2,2),(188,112,'＆攻撃力をダウン',1,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,2,2),(189,115,'自身の攻撃力をアップ',1,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,15,1,1),(190,115,'＆ハイド時さらに攻撃力をアップ',1,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,35,1,1),(191,116,'自身の攻撃力をアップ',1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,28,1,1),(192,117,'自身の攻撃力をアップ',1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,30,1,1),(193,118,'自身の攻撃力をアップ',1,2,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,30,1,1),(194,119,'自身の攻撃力をアップ',1,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,30,1,1),(195,120,'自身の攻撃力をアップ',1,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,40,1,1),(196,121,'自身のNPを増やす',12,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,1,1),(197,121,'＆NP獲得量をアップ',11,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,30,1,1),(198,122,'自身にターゲット集中状態を付与',23,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1),(199,122,'＆防御力をアップ',28,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,40,1,1),(200,123,'自身の精神異常耐性を大アップ',34,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,100,1,1),(201,123,'＆HPを回復',25,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2000,1,1),(202,124,'味方全体の宝具威力をアップ',5,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,14,3,1),(203,125,'味方全体の宝具威力をアップ',5,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,16,3,1),(204,126,'味方全体の宝具威力をアップ',5,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,3,1),(205,124,'＆攻撃力をアップ',1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,14,3,1),(206,125,'＆攻撃力をアップ',1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,16,3,1),(207,126,'＆攻撃力をアップ',1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,17,3,1),(208,127,'自身の攻撃力をアップ',1,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,25.5,1,1),(209,128,'自身の攻撃力をアップ',1,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,27,1,1),(210,127,'＆ガッツ状態を付与（1回）',27,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,1),(211,128,'＆ガッツ状態を付与（1回）',27,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,1),(212,127,'＆弱体耐性をダウン【デメリット】',13,3,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,50,1,1),(213,128,'＆弱体耐性をダウン【デメリット】',13,3,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,50,1,1),(214,129,'味方全体のHPを回復',25,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2000,3,1),(215,129,'＋自身を除く味方全体の〔女性〕のHPを回復',25,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2000,9,1),(216,130,'自身に〔ローマ〕特攻状態を付与',29,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,60,1,1),(217,131,'味方全体のArtsカード性能をアップ',3,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,3,1),(218,132,'味方全体のNP獲得量をアップ',11,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,3,1),(219,133,'味方全体のNP獲得量をアップ',11,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,3,1),(220,133,'＋自身のQuickカード性能をアップ',4,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,1,1),(221,134,'自身に回避状態を付与',22,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1),(222,134,'＆スター発生率をアップ',10,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,100,1,1),(223,135,'敵単体〔人型〕に確率で魅了付与',17,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,75,12,2),(224,136,'敵単体〔人型〕に確率で魅了付与',17,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,80,12,2),(225,137,'味方全体のQuickカード性能をアップ',4,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,3,1),(226,138,'自身に無敵状態を付与（3回）',22,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1),(227,138,'＆毎ターンHP回復状態を付与',25,5,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,600,1,1),(228,139,'自身の精神異常付与成功率をアップ',21,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,30,1,1),(229,139,'＆HPを大回復',25,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2500,1,1),(230,140,'自身の弱体耐性をアップ',13,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,100,1,1),(231,141,'自身の弱体耐性をアップ',13,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,100,1,1),(232,140,'＆HPを大回復',25,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2500,1,1),(233,141,'＆防御力をアップ',28,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,40,1,1),(234,141,'＆攻撃力をアップ',1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,1,1),(235,142,'味方全体のHPを回復',25,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1500,3,1),(236,143,'味方全体のHPを回復',25,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2000,3,1),(237,142,'＆弱体状態を解除',33,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3,1),(238,144,'敵単体の強化状態を解除',16,NULL,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,2),(239,144,'＆防御力をダウン',28,3,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,30,2,2),(240,145,'自身のNPをものすごく増やす',12,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,120,1,1),(241,146,'自身のNPをものすごく増やす',12,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,150,1,1),(242,147,'自身のNPをすごく増やす',12,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,75,1,1),(243,148,'自身のNPをすごく増やす',12,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,79.5,1,1),(244,149,'自身のNPをすごく増やす',12,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,80,1,1),(245,150,'皮を愛でて自身のHPを回復',25,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2500,1,1),(246,151,'味方単体の弱体状態を解除',33,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,1),(247,151,'＆NP獲得量をアップ',11,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,50,2,1),(248,152,'自身の攻撃力をアップ',1,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,30,1,1),(249,152,'＋ スターを獲得',20,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,15,3,1),(250,153,'味方全体の宝具威力をアップ',5,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,3,1),(251,153,'＋敵全体の防御強化状態を解除',16,NULL,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3,2),(252,154,'自身の攻撃弱体耐性をアップ',13,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,100,1,1),(253,154,'＆HPを大回復',25,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4000,1,1),(254,154,'＆NPを増やす',12,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,1,1),(255,155,'味方全体に〔ローマ〕特攻状態を付与',29,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,60,3,1),(256,155,'＆クリティカル威力をアップ',9,3,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,50,3,1),(257,156,'敵単体〔人型〕に確率で魅了付与',17,1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,100,12,2),(258,156,'＋敵単体の強化状態を解除',16,NULL,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,2);
/*!40000 ALTER TABLE `servant_skill_effect` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skill_relation`
--

DROP TABLE IF EXISTS `skill_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skill_relation` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `servant_id` int(11) NOT NULL,
  `skill_id` int(11) NOT NULL,
  `junban` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skill_relation`
--

LOCK TABLES `skill_relation` WRITE;
/*!40000 ALTER TABLE `skill_relation` DISABLE KEYS */;
INSERT INTO `skill_relation` VALUES (1,1,43,1),(2,1,45,3),(3,1,46,5),(4,1,44,2),(5,1,47,7),(6,1,48,9),(7,1,49,11),(8,2,5,1),(9,2,31,3),(10,2,11,5),(11,2,50,6),(12,3,31,1),(13,3,10,3),(14,3,1,5),(15,4,10,1),(16,4,31,3),(17,4,51,5),(18,5,52,1),(19,5,35,3),(20,5,54,5),(21,5,53,4),(22,6,13,1),(23,6,57,3),(24,6,62,5),(25,6,63,6),(26,7,29,1),(27,7,2,3),(28,7,64,5),(29,7,65,6),(30,8,29,1),(31,8,107,3),(32,8,152,5),(33,8,153,2),(34,8,110,4),(35,9,27,1),(36,9,14,3),(37,9,66,5),(38,10,24,1),(39,10,67,3),(40,10,68,5),(41,11,25,1),(42,11,16,3),(43,11,70,5),(44,11,69,4),(45,11,75,6),(46,12,7,1),(47,12,15,3),(48,12,76,5),(49,12,77,6),(50,13,78,1),(51,13,12,3),(52,13,79,5),(53,14,80,1),(54,14,82,3),(55,14,83,5),(56,14,81,2),(57,15,84,1),(58,15,88,3),(59,15,89,5),(60,16,91,1),(61,16,19,3),(62,16,92,5),(63,17,59,1),(64,17,94,3),(65,17,55,5),(66,18,2,1),(67,18,97,3),(68,18,58,5),(69,18,96,2),(70,19,98,1),(71,19,99,3),(72,19,100,5),(73,20,101,1),(74,20,94,3),(75,20,103,5),(76,20,104,6),(77,21,105,1),(78,21,59,3),(79,21,106,5),(80,22,108,1),(81,22,35,3),(82,22,111,5),(83,22,154,2),(84,23,113,1),(85,23,118,3),(86,23,121,5),(87,24,122,1),(88,24,123,3),(89,24,59,5),(90,25,125,1),(91,25,128,3),(92,25,129,5),(93,26,130,1),(94,26,57,3),(95,26,131,5),(96,26,155,2),(97,27,132,1),(98,27,3,3),(99,27,134,5),(100,27,133,2),(101,28,2,1),(102,28,136,3),(103,28,137,5),(104,28,156,4),(105,29,86,1),(106,29,138,3),(107,29,139,5),(108,30,140,1),(109,30,142,3),(110,30,144,5),(111,30,143,4),(112,31,146,1),(113,31,150,3),(114,31,151,5);
/*!40000 ALTER TABLE `skill_relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `state_relation`
--

DROP TABLE IF EXISTS `state_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `state_relation` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `servant_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `state_relation`
--

LOCK TABLES `state_relation` WRITE;
/*!40000 ALTER TABLE `state_relation` DISABLE KEYS */;
INSERT INTO `state_relation` VALUES (1,1,16),(2,1,6),(3,1,2),(4,2,16),(5,2,6),(6,2,2),(8,2,4),(9,2,8),(10,2,7),(11,2,5),(12,3,16),(13,3,2),(14,3,8),(15,3,7),(16,3,5),(17,3,4),(18,4,16),(26,4,2),(27,4,6),(28,4,4),(29,4,7),(30,4,8),(31,4,5),(32,5,16),(33,5,2),(34,5,6),(35,5,9),(36,5,8),(37,5,5),(38,6,16),(39,6,2),(40,6,4),(41,6,14),(42,6,19),(43,6,5),(44,7,16),(45,7,2),(46,7,1),(47,7,9),(48,7,14),(49,7,5),(50,8,16),(51,8,2),(52,8,1),(53,8,5),(54,9,16),(55,9,2),(56,9,6),(57,10,16),(58,10,2),(59,10,6),(60,11,16),(61,11,2),(62,11,14),(63,12,16),(64,12,2),(65,12,1),(66,12,5),(67,13,16),(68,13,2),(69,13,14),(70,14,16),(71,14,2),(73,15,16),(74,15,2),(75,15,1),(76,16,16),(77,16,2),(78,16,14),(79,17,16),(80,17,2),(81,17,1),(82,17,14),(83,18,16),(84,18,2),(85,18,4),(86,19,16),(87,19,2),(88,19,14),(89,20,16),(90,20,2),(91,20,1),(92,21,16),(93,21,2),(94,21,5),(95,22,16),(96,22,2),(97,22,9),(98,22,5),(99,22,18),(100,23,16),(101,23,2),(102,23,6),(103,23,1),(104,24,16),(105,24,2),(106,24,6),(107,24,14),(108,25,16),(109,25,2),(110,26,16),(111,26,2),(112,26,6),(113,26,5),(114,27,16),(115,27,2),(116,27,6),(117,27,14),(118,28,16),(119,28,2),(120,28,6),(121,28,1),(122,28,5),(123,28,15),(124,29,16),(125,29,2),(126,29,6),(128,30,16),(129,30,2),(130,30,6),(131,30,1),(132,31,16),(133,31,2);
/*!40000 ALTER TABLE `state_relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `target`
--

DROP TABLE IF EXISTS `target`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `target` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `target_name` varchar(255) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  `rare_id` int(11) DEFAULT NULL,
  `tenchijin_id` int(11) DEFAULT NULL,
  `seikaku_id` int(11) DEFAULT NULL,
  `houshin_id` int(11) DEFAULT NULL,
  `sex_id` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `target`
--

LOCK TABLES `target` WRITE;
/*!40000 ALTER TABLE `target` DISABLE KEYS */;
INSERT INTO `target` VALUES (1,'自身',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'単体',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'全体',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,'全体（自身を除く）',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,'単体〔男性〕',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL),(6,'単体〔女性〕 ',NULL,NULL,NULL,NULL,NULL,NULL,2,NULL),(7,'単体〔異性〕',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,'全体〔男性〕',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL),(9,'全体〔女性〕',NULL,NULL,NULL,NULL,NULL,NULL,2,NULL),(10,'全体〔異性〕',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,'単体',1,NULL,NULL,NULL,NULL,NULL,NULL,'神性'),(12,'単体',2,NULL,NULL,NULL,NULL,NULL,NULL,'人型'),(13,'単体',3,NULL,NULL,NULL,NULL,NULL,NULL,'人間'),(14,'単体',4,NULL,NULL,NULL,NULL,NULL,NULL,'竜'),(15,'単体',5,NULL,NULL,NULL,NULL,NULL,NULL,'王'),(16,'単体',6,NULL,NULL,NULL,NULL,NULL,NULL,'騎乗'),(17,'単体',7,NULL,NULL,NULL,NULL,NULL,NULL,'アーサー'),(18,'単体',8,NULL,NULL,NULL,NULL,NULL,NULL,'アルトリア顔'),(19,'単体',9,NULL,NULL,NULL,NULL,NULL,NULL,'ローマ'),(20,'単体',10,NULL,NULL,NULL,NULL,NULL,NULL,'猛獣'),(21,'単体',11,NULL,NULL,NULL,NULL,NULL,NULL,'魔性'),(22,'単体',12,NULL,NULL,NULL,NULL,NULL,NULL,'超巨大'),(23,'単体',13,NULL,NULL,NULL,NULL,NULL,NULL,'人類の脅威'),(24,'単体',14,NULL,NULL,NULL,NULL,NULL,NULL,'愛する者'),(25,'単体',15,NULL,NULL,NULL,NULL,NULL,NULL,'ギリシャ神話系男性'),(26,'単体',16,NULL,NULL,NULL,NULL,NULL,NULL,'サーヴァント'),(27,'単体',17,NULL,NULL,NULL,NULL,NULL,NULL,'疑似サーヴァント'),(28,'単体',18,NULL,NULL,NULL,NULL,NULL,NULL,'エヌマ特攻無効'),(36,'全体',1,NULL,NULL,NULL,NULL,NULL,NULL,'神性'),(37,'全体',2,NULL,NULL,NULL,NULL,NULL,NULL,'人型'),(38,'全体',3,NULL,NULL,NULL,NULL,NULL,NULL,'人間'),(39,'全体',4,NULL,NULL,NULL,NULL,NULL,NULL,'竜'),(40,'全体',5,NULL,NULL,NULL,NULL,NULL,NULL,'王'),(41,'全体',6,NULL,NULL,NULL,NULL,NULL,NULL,'騎乗'),(42,'全体',7,NULL,NULL,NULL,NULL,NULL,NULL,'アーサー'),(43,'全体',8,NULL,NULL,NULL,NULL,NULL,NULL,'アルトリア顔'),(44,'全体',9,NULL,NULL,NULL,NULL,NULL,NULL,'ローマ'),(45,'全体',10,NULL,NULL,NULL,NULL,NULL,NULL,'猛獣'),(46,'全体',11,NULL,NULL,NULL,NULL,NULL,NULL,'魔性'),(47,'全体',12,NULL,NULL,NULL,NULL,NULL,NULL,'超巨大'),(48,'全体',13,NULL,NULL,NULL,NULL,NULL,NULL,'人類の脅威'),(49,'全体',14,NULL,NULL,NULL,NULL,NULL,NULL,'愛する者'),(50,'全体',15,NULL,NULL,NULL,NULL,NULL,NULL,'ギリシャ神話系男性'),(51,'全体',16,NULL,NULL,NULL,NULL,NULL,NULL,'サーヴァント'),(52,'全体',17,NULL,NULL,NULL,NULL,NULL,NULL,'疑似サーヴァント'),(53,'全体',18,NULL,NULL,NULL,NULL,NULL,NULL,'エヌマ特攻無効'),(61,'全体（自身を除く）',1,NULL,NULL,NULL,NULL,NULL,NULL,'神性'),(62,'全体（自身を除く）',2,NULL,NULL,NULL,NULL,NULL,NULL,'人型'),(63,'全体（自身を除く）',3,NULL,NULL,NULL,NULL,NULL,NULL,'人間'),(64,'全体（自身を除く）',4,NULL,NULL,NULL,NULL,NULL,NULL,'竜'),(65,'全体（自身を除く）',5,NULL,NULL,NULL,NULL,NULL,NULL,'王'),(66,'全体（自身を除く）',6,NULL,NULL,NULL,NULL,NULL,NULL,'騎乗'),(67,'全体（自身を除く）',7,NULL,NULL,NULL,NULL,NULL,NULL,'アーサー'),(68,'全体（自身を除く）',8,NULL,NULL,NULL,NULL,NULL,NULL,'アルトリア顔'),(69,'全体（自身を除く）',9,NULL,NULL,NULL,NULL,NULL,NULL,'ローマ'),(70,'全体（自身を除く）',10,NULL,NULL,NULL,NULL,NULL,NULL,'猛獣'),(71,'全体（自身を除く）',11,NULL,NULL,NULL,NULL,NULL,NULL,'魔性'),(72,'全体（自身を除く）',12,NULL,NULL,NULL,NULL,NULL,NULL,'超巨大'),(73,'全体（自身を除く）',13,NULL,NULL,NULL,NULL,NULL,NULL,'人類の脅威'),(74,'全体（自身を除く）',14,NULL,NULL,NULL,NULL,NULL,NULL,'愛する者'),(75,'全体（自身を除く）',15,NULL,NULL,NULL,NULL,NULL,NULL,'ギリシャ神話系男性'),(76,'全体（自身を除く）',16,NULL,NULL,NULL,NULL,NULL,NULL,'サーヴァント'),(77,'全体（自身を除く）',17,NULL,NULL,NULL,NULL,NULL,NULL,'疑似サーヴァント'),(78,'全体（自身を除く）',18,NULL,NULL,NULL,NULL,NULL,NULL,'エヌマ特攻無効'),(79,'全体（自身を除く）',NULL,NULL,NULL,NULL,NULL,NULL,1,'男'),(80,'全体（自身を除く）',NULL,NULL,NULL,NULL,NULL,NULL,2,'女');
/*!40000 ALTER TABLE `target` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-02 18:38:09

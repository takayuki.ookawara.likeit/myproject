package model;

import java.io.Serializable;

import dao.TargetDAO;

public class Skill_Effect implements Serializable {
	private int id;
	private String ef_text;
	private int ef_genre;
	private int turn;
	private int bord;
	private float ef_para_1;
	private float ef_para_2;
	private float ef_para_3;
	private float ef_para_4;
	private float ef_para_5;
	private float ef_para_6;
	private float ef_para_7;
	private float ef_para_8;
	private float ef_para_9;
	private float ef_para_10;
	private int tageid;
	private int fore;


	public Target getTarget() {
		return TargetDAO.findByID(this.tageid);
	}



	public Skill_Effect(int id,String ef_text,int ef_genre,int turn,int bord,float ef_para_1,
			float ef_para_2,float ef_para_3,float ef_para_4,float ef_para_5,float ef_para_6,
			float ef_para_7,float ef_para_8,float ef_para_9,float ef_para_10) {
		this.id = id;
		this.ef_text = ef_text;
		this.ef_genre = ef_genre;
		this.turn = turn;
		this.bord = bord;
		this.ef_para_1 = ef_para_1;
		this.ef_para_2 = ef_para_2;
		this.ef_para_3 = ef_para_3;
		this.ef_para_4 = ef_para_4;
		this.ef_para_5 = ef_para_5;
		this.ef_para_6 = ef_para_6;
		this.ef_para_7 = ef_para_7;
		this.ef_para_8 = ef_para_8;
		this.ef_para_9 = ef_para_9;
		this.ef_para_10 = ef_para_10;
	}

	public Skill_Effect(String ef_text,int ef_genre,int turn,int bord,float ef_para_1,
			float ef_para_2,float ef_para_3,float ef_para_4,float ef_para_5,float ef_para_6,
			float ef_para_7,float ef_para_8,float ef_para_9,float ef_para_10,int tageid,int fore) {
		this.ef_text = ef_text;
		this.ef_genre = ef_genre;
		this.turn = turn;
		this.bord = bord;
		this.ef_para_1 = ef_para_1;
		this.ef_para_2 = ef_para_2;
		this.ef_para_3 = ef_para_3;
		this.ef_para_4 = ef_para_4;
		this.ef_para_5 = ef_para_5;
		this.ef_para_6 = ef_para_6;
		this.ef_para_7 = ef_para_7;
		this.ef_para_8 = ef_para_8;
		this.ef_para_9 = ef_para_9;
		this.ef_para_10 = ef_para_10;
		this.tageid = tageid;
		this.fore = fore;
	}

	public float getEf_para(String type) {
		float para = 0;
		switch (Integer.parseInt(type)) {
		case 1:
			para = this.getEf_para_1();
			break;
		case 2:
			para = this.getEf_para_2();
			break;
		case 3:
			para = this.getEf_para_3();
			break;
		case 4:
			para = this.getEf_para_4();
			break;
		case 5:
			para = this.getEf_para_5();
			break;
		case 6:
			para = this.getEf_para_6();
			break;
		case 7:
			para = this.getEf_para_7();
			break;
		case 8:
			para = this.getEf_para_8();
			break;
		case 9:
			para = this.getEf_para_9();
			break;
		case 10:
			para = this.getEf_para_10();
			break;
		default:
			break;
		}
		return para;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEf_text() {
		return ef_text;
	}
	public void setEf_text(String ef_text) {
		this.ef_text = ef_text;
	}
	public int getEf_genre() {
		return ef_genre;
	}
	public void setEf_genre(int ef_genre) {
		this.ef_genre = ef_genre;
	}
	public int getTurn() {
		return turn;
	}
	public void setTurn(int turn) {
		this.turn = turn;
	}
	public int getBord() {
		return bord;
	}
	public void setBord(int bord) {
		this.bord = bord;
	}
	public float getEf_para_1() {
		return ef_para_1;
	}
	public void setEf_para_1(float ef_para_1) {
		this.ef_para_1 = ef_para_1;
	}
	public float getEf_para_2() {
		return ef_para_2;
	}
	public void setEf_para_2(float ef_para_2) {
		this.ef_para_2 = ef_para_2;
	}
	public float getEf_para_3() {
		return ef_para_3;
	}
	public void setEf_para_3(float ef_para_3) {
		this.ef_para_3 = ef_para_3;
	}
	public float getEf_para_4() {
		return ef_para_4;
	}
	public void setEf_para_4(float ef_para_4) {
		this.ef_para_4 = ef_para_4;
	}
	public float getEf_para_5() {
		return ef_para_5;
	}
	public void setEf_para_5(float ef_para_5) {
		this.ef_para_5 = ef_para_5;
	}
	public float getEf_para_6() {
		return ef_para_6;
	}
	public void setEf_para_6(float ef_para_6) {
		this.ef_para_6 = ef_para_6;
	}
	public float getEf_para_7() {
		return ef_para_7;
	}
	public void setEf_para_7(float ef_para_7) {
		this.ef_para_7 = ef_para_7;
	}
	public float getEf_para_8() {
		return ef_para_8;
	}
	public void setEf_para_8(float ef_para_8) {
		this.ef_para_8 = ef_para_8;
	}
	public float getEf_para_9() {
		return ef_para_9;
	}
	public void setEf_para_9(float ef_para_9) {
		this.ef_para_9 = ef_para_9;
	}
	public float getEf_para_10() {
		return ef_para_10;
	}
	public void setEf_para_10(float ef_para_10) {
		this.ef_para_10 = ef_para_10;
	}
	public int getTageid() {
		return tageid;
	}
	public void setTageid(int tageid) {
		this.tageid = tageid;
	}
	public int getFore() {
		return fore;
	}
	public void setFore(int fore) {
		this.fore = fore;
	}
}
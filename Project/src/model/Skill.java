package model;

import java.io.Serializable;
import java.util.List;

import dao.SEDAO;

public class Skill implements Serializable {
	private int id;
	private String name;
	private int ct;

	List<Skill_Effect> skill_Effects;


	//結合
	public List<Skill_Effect> getSkill_Effect(){
		this.skill_Effects = SEDAO.findByID(this.id);
		return skill_Effects;
	}




	public Skill(int id, String name,int ct) {
		this.id = id;
		this.name = name;
		this.ct = ct;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCt() {
		return ct;
	}
	public void setCt(int ct) {
		this.ct = ct;
	}

}

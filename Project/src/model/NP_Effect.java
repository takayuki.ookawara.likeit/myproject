package model;

import java.io.Serializable;

public class NP_Effect implements Serializable {
	private int id;
	private String ef_text;
	private int ef_genre;
	private int turn;
	private int bord;
	private float ef_para_1;
	private float ef_para_2;
	private float ef_para_3;
	private float ef_para_4;
	private float ef_para_5;
	private int beaf;
	private int tageid;
	private int fast;
	private int fore;
	private int oc;





	//全乗せ
	public NP_Effect(String ef_text,int ef_genre,int turn,int bord,float ef_para_1,
			float ef_para_2,float ef_para_3,float ef_para_4,float ef_para_5,int beaf,int tageid,
			int fast,int fore,int oc) {
		this.ef_text = ef_text;
		this.ef_genre = ef_genre;
		this.turn = turn;
		this.bord = bord;
		this.ef_para_1 = ef_para_1;
		this.ef_para_2 = ef_para_2;
		this.ef_para_3 = ef_para_3;
		this.ef_para_4 = ef_para_4;
		this.ef_para_5 = ef_para_5;
		this.beaf = beaf;
		this.tageid = tageid;
		this.fast = fast;
		this.fore = fore;
		this.oc = oc;
	}



	/**
	 * typeごとのgetEf_paraを返す
	 * @param type
	 * @return
	 */
	public float getEf_para(String type) {
		float para = 0;
		switch (Integer.parseInt(type)) {
		case 1:
			para = this.getEf_para_1();
			break;
		case 2:
			para = this.getEf_para_2();
			break;
		case 3:
			para = this.getEf_para_3();
			break;
		case 4:
			para = this.getEf_para_4();
			break;
		case 5:
			para = this.getEf_para_5();
			break;
		default:
			break;
		}
		return para;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEf_text() {
		return ef_text;
	}
	public void setEf_text(String ef_text) {
		this.ef_text = ef_text;
	}
	public int getEf_genre() {
		return ef_genre;
	}
	public void setEf_genre(int ef_genre) {
		this.ef_genre = ef_genre;
	}
	public int getTurn() {
		return turn;
	}
	public void setTurn(int turn) {
		this.turn = turn;
	}
	public int getBord() {
		return bord;
	}
	public void setBord(int bord) {
		this.bord = bord;
	}
	public float getEf_para_1() {
		return ef_para_1;
	}
	public void setEf_para_1(float ef_para_1) {
		this.ef_para_1 = ef_para_1;
	}
	public float getEf_para_2() {
		return ef_para_2;
	}
	public void setEf_para_2(float ef_para_2) {
		this.ef_para_2 = ef_para_2;
	}
	public float getEf_para_3() {
		return ef_para_3;
	}
	public void setEf_para_3(float ef_para_3) {
		this.ef_para_3 = ef_para_3;
	}
	public float getEf_para_4() {
		return ef_para_4;
	}
	public void setEf_para_4(float ef_para_4) {
		this.ef_para_4 = ef_para_4;
	}
	public float getEf_para_5() {
		return ef_para_5;
	}
	public void setEf_para_5(float ef_para_5) {
		this.ef_para_5 = ef_para_5;
	}
	public int getBeaf() {
		return beaf;
	}
	public void setBeaf(int beaf) {
		this.beaf = beaf;
	}
	public int getTageid() {
		return tageid;
	}
	public void setTageid(int tageid) {
		this.tageid = tageid;
	}
	public int getFast() {
		return fast;
	}
	public void setFast(int fast) {
		this.fast = fast;
	}
	public int getFore() {
		return fore;
	}
	public void setFore(int fore) {
		this.fore = fore;
	}
	public int getOc() {
		return oc;
	}
	public void setOc(int oc) {
		this.oc = oc;
	}
}

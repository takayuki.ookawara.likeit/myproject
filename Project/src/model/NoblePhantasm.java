package model;

import java.io.Serializable;
import java.util.List;

import dao.NPBDAO;
import dao.NPefDAO;

public class NoblePhantasm implements Serializable {
	private int id;
	private String name;
	private String ruby;
	private String rank;
	private String type;
	private int bairitsu_id;
	private String damage_text;
	private String description;

	List<NP_Effect> np_Effects;



	//全乗せ
	public NoblePhantasm(int id,String name,String ruby,String rank,String type,int bairitsu_id,
			String damage_text,String description) {
		this.id = id;
		this.name = name;
		this.ruby = ruby;
		this.rank = rank;
		this.type = type;
		this.bairitsu_id = bairitsu_id;
		this.damage_text = damage_text;
		this.description = description;
	}


	// 結合
	public NP_bairitsu getNP_bairitsu() {
		return NPBDAO.findByID(this.bairitsu_id);
	}
	public List<NP_Effect> getNP_Effect() {
		this.np_Effects = NPefDAO.findByID(this.id);
		return np_Effects;
	}
	public List<NP_Effect> getNP_EffectB() {
		this.np_Effects = NPefDAO.findByIDforBeaf(this.id);
		return np_Effects;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRuby() {
		return ruby;
	}
	public void setRuby(String ruby) {
		this.ruby = ruby;
	}
	public String getRank() {
		return rank;
	}
	public void setRank(String rank) {
		this.rank = rank;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getBairitsu_id() {
		return bairitsu_id;
	}
	public void setBairitsu_id(int bairitsu_id) {
		this.bairitsu_id = bairitsu_id;
	}
	public String getDamage_text() {
		return damage_text;
	}
	public void setDamage_text(String damage_text) {
		this.damage_text = damage_text;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}

package model;

import java.io.Serializable;

import dao.Relation_stateDAO;
import dao.S_ClassDAO;
import dao.S_HoushinDAO;
import dao.S_RareDAO;
import dao.S_SeikakuDAO;
import dao.S_SexDAO;
import dao.S_TenchijinDAO;

public class Target implements Serializable {
	private int id;
	private int state_id;
	private int rare_id;
	private int class_id;
	private int tenchijin_id;
	private int sex_id;
	private int seikaku_id;
	private int houshin_id;
	private String tage_name;
	private String desc;






	//レア度 コスト 入手方法
	public S_rare getRare(){
		return S_RareDAO.findByID(this.rare_id);
	}
	//クラス名
	public S_class getClass_(){
		return S_ClassDAO.findByID(this.class_id);
	}
	//天地人
	public S_tenchijin getTenchijin(){
		return S_TenchijinDAO.findByID(this.tenchijin_id);
	}
	//性別
	public S_sex getSex(){
		return S_SexDAO.findByID(this.sex_id);
	}
	//性格
	public S_seikaku getSeikaku(){
		return S_SeikakuDAO.findByID(this.seikaku_id);
	}
	//方針
	public S_houshin getHoushin(){
		return S_HoushinDAO.findByID(this.houshin_id);
	}
	//特性
	public Relation_state getRelation_state() {
		return Relation_stateDAO.findforTage(this.state_id);
	}

	public Target(int id,int state_id,int rare_id,int class_id,int tenchijin_id,
			int sex_id,int seikaku_id,int houshin_id,String tage_name,String desc) {
		this.id = id;
		this.state_id = state_id;
		this.rare_id = rare_id;
		this.class_id = class_id;
		this.tenchijin_id = tenchijin_id;
		this.sex_id = sex_id;
		this.seikaku_id = seikaku_id;
		this.houshin_id = houshin_id;
		this.tage_name = tage_name;
		this.desc = desc;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getState_id() {
		return state_id;
	}
	public void setState_id(int state_id) {
		this.state_id = state_id;
	}
	public int getRare_id() {
		return rare_id;
	}
	public void setRare_id(int rare_id) {
		this.rare_id = rare_id;
	}
	public int getClass_id() {
		return class_id;
	}
	public void setClass_id(int class_id) {
		this.class_id = class_id;
	}
	public int getTenchijin_id() {
		return tenchijin_id;
	}
	public void setTenchijin_id(int tenchijin_id) {
		this.tenchijin_id = tenchijin_id;
	}
	public int getSex_id() {
		return sex_id;
	}
	public void setSex_id(int sex_id) {
		this.sex_id = sex_id;
	}
	public int getSeikaku_id() {
		return seikaku_id;
	}
	public void setSeikaku_id(int seikaku_id) {
		this.seikaku_id = seikaku_id;
	}
	public int getHoushin_id() {
		return houshin_id;
	}
	public void setHoushin_id(int houshin_id) {
		this.houshin_id = houshin_id;
	}
	public String getTage_name() {
		return tage_name;
	}
	public void setTage_name(String tage_name) {
		this.tage_name = tage_name;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
}

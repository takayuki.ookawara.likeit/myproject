package model;

import java.io.Serializable;

import dao.CSDAO;

public class Relation_c_skill implements Serializable {
	private int id;
	private int servant_id;
	private int c_skill_id;
	private int junban;





	public Class_Skill getClass_Skill() {
		return CSDAO.findByID(c_skill_id);
	}

	public Relation_c_skill(int id,int servant_id,int c_skill_id,int junban) {
		this.id = id;
		this.servant_id = servant_id;
		this.c_skill_id = c_skill_id;
		this.junban = junban;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getServant_id() {
		return servant_id;
	}
	public void setServant_id(int servant_id) {
		this.servant_id = servant_id;
	}
	public int getC_skill_id() {
		return c_skill_id;
	}
	public void setC_skill_id(int c_skill_id) {
		this.c_skill_id = c_skill_id;
	}
	public int getJunban() {
		return junban;
	}
	public void setJunban(int junban) {
		this.junban = junban;
	}
}

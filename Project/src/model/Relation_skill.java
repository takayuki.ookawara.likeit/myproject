package model;

import java.io.Serializable;

import dao.SkillDAO;

public class Relation_skill implements Serializable {
	private int id;
	private int servant_id;
	private int skill_id;
	private int junban;




	public Skill getSkill() {
		return SkillDAO.findByID(this.skill_id);
	}

	public Relation_skill(int id,int servant_id,int skill_id,int junban) {
		this.id = id;
		this.servant_id = servant_id;
		this.skill_id = skill_id;
		this.junban = junban;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getServant_id() {
		return servant_id;
	}
	public void setServant_id(int servant_id) {
		this.servant_id = servant_id;
	}
	public int getSkill_id() {
		return skill_id;
	}
	public void setSkill_id(int skill_id) {
		this.skill_id = skill_id;
	}
	public int getJunban() {
		return junban;
	}
	public void setJunban(int junban) {
		this.junban = junban;
	}

}

package model;

import java.io.Serializable;

public class S_state implements Serializable {
	private int id;
	private String state;





	public S_state(int id,String state) {
		this.id = id;
		this.state = state;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}

}

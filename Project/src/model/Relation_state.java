package model;

import java.io.Serializable;

import dao.S_StateDAO;

public class Relation_state implements Serializable {
	private int id;
	private int servant_id;
	private int state_id;




	public S_state getS_state() {
		return S_StateDAO.findByID(state_id);
	}

	public Relation_state(int id,int servant_id,int state_id) {
		this.id = id;
		this.servant_id = servant_id;
		this.state_id = state_id;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getServant_id() {
		return servant_id;
	}
	public void setServant_id(int servant_id) {
		this.servant_id = servant_id;
	}
	public int getState_id() {
		return state_id;
	}
	public void setState_id(int state_id) {
		this.state_id = state_id;
	}

}

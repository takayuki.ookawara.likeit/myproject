package model;

import java.io.Serializable;

public class NP_bairitsu implements Serializable {
	private int id;
	private float np_bairitsu_1;
	private float np_bairitsu_2;
	private float np_bairitsu_3;
	private float np_bairitsu_4;
	private float np_bairitsu_5;



	public NP_bairitsu(float np_bairitsu_1,float np_bairitsu_2,float np_bairitsu_3,
			float np_bairitsu_4,float np_bairitsu_5) {
		this.np_bairitsu_1 = np_bairitsu_1;
		this.np_bairitsu_2 = np_bairitsu_2;
		this.np_bairitsu_3 = np_bairitsu_3;
		this.np_bairitsu_4 = np_bairitsu_4;
		this.np_bairitsu_5 = np_bairitsu_5;
	}

	public float getNp_bairitsu(String npl) {
		float re = 0;
		switch (Integer.parseInt(npl)) {
		case 1:
			re = this.getNp_bairitsu_1();
			break;
		case 2:
			re = this.getNp_bairitsu_2();
			break;
		case 3:
			re = this.getNp_bairitsu_3();
			break;
		case 4:
			re = this.getNp_bairitsu_4();
			break;
		case 5:
			re = this.getNp_bairitsu_5();
			break;
		default:
			break;
		}
		return re;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public float getNp_bairitsu_1() {
		return np_bairitsu_1;
	}
	public void setNp_bairitsu_1(float np_bairitsu_1) {
		this.np_bairitsu_1 = np_bairitsu_1;
	}
	public float getNp_bairitsu_2() {
		return np_bairitsu_2;
	}
	public void setNp_bairitsu_2(float np_bairitsu_2) {
		this.np_bairitsu_2 = np_bairitsu_2;
	}
	public float getNp_bairitsu_3() {
		return np_bairitsu_3;
	}
	public void setNp_bairitsu_3(float np_bairitsu_3) {
		this.np_bairitsu_3 = np_bairitsu_3;
	}
	public float getNp_bairitsu_4() {
		return np_bairitsu_4;
	}
	public void setNp_bairitsu_4(float np_bairitsu_4) {
		this.np_bairitsu_4 = np_bairitsu_4;
	}
	public float getNp_bairitsu_5() {
		return np_bairitsu_5;
	}
	public void setNp_bairitsu_5(float np_bairitsu_5) {
		this.np_bairitsu_5 = np_bairitsu_5;
	}

}

package model;

import java.io.Serializable;

public class S_houshin implements Serializable {
	private int id;
	private String name;




	public S_houshin(int id,String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}


}

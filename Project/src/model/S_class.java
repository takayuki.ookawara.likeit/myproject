package model;

import java.io.Serializable;

public class S_class implements Serializable {
	private int id;
	private String name;
	private float in_value;




	public S_class(int id,String name,float in_value) {
		this.id = id;
		this.name = name;
		this.in_value = in_value;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getIn_value() {
		return in_value;
	}
	public void setIn_value(float in_value) {
		this.in_value = in_value;
	}

}

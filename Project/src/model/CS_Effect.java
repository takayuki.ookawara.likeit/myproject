package model;

import java.io.Serializable;

public class CS_Effect implements Serializable {
	private int id;
	private String ef_text;
	private int ef_genre;
	private int bord;
	private float ef_para;
	private int tageid;



	public CS_Effect(int id,String ef_text,int ef_genre,int bord,float ef_para,int tageid) {
		this.id = id;
		this.ef_text = ef_text;
		this.ef_genre = ef_genre;
		this.bord = bord;
		this.ef_para = ef_para;
		this.tageid = tageid;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEf_text() {
		return ef_text;
	}
	public void setEf_text(String ef_text) {
		this.ef_text = ef_text;
	}
	public int getEf_genre() {
		return ef_genre;
	}
	public void setEf_genre(int ef_genre) {
		this.ef_genre = ef_genre;
	}
	public int getBord() {
		return bord;
	}
	public void setBord(int bord) {
		this.bord = bord;
	}
	public float getEf_para() {
		return ef_para;
	}
	public void setEf_para(float ef_para) {
		this.ef_para = ef_para;
	}
	public int getTageid() {
		return tageid;
	}
	public void setTageid(int tageid) {
		this.tageid = tageid;
	}

}

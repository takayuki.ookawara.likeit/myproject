package model;

import java.io.Serializable;
import java.util.List;

import dao.ColorDAO;
import dao.Relation_CSDAO;
import dao.Relation_npDAO;
import dao.Relation_skillDAO;
import dao.Relation_stateDAO;
import dao.S_ClassDAO;
import dao.S_HoushinDAO;
import dao.S_RareDAO;
import dao.S_SeikakuDAO;
import dao.S_SexDAO;
import dao.S_TenchijinDAO;

public class Servant implements Serializable {
	private int id;
	private String name;
	private int hp;
	private int atk;
	private String card;
	private int b_hit;
	private int a_hit;
	private int q_hit;
	private int ex_hit;
	private int n_hit;
	private float np_eff;
	private float np_damage;
	private float star;
	private float star_atsumare;
	private String illustrator;
	private String cv;
	private int rare_id;
	private int class_id;
	private int tenchijin_id;
	private int sex_id;
	private int seikaku_id;
	private int houshin_id;
	private int color_id;

	List<Relation_np> R_np;
	List<Relation_skill> R_skill;
	List<Relation_c_skill> R_cs;
	List<Relation_state> R_state;




	//宝具
	public List<Relation_np> getRelation_np(){
		this.R_np = Relation_npDAO.findByID(this.id);
		return R_np;
	}
	public Relation_np getRelation_npONE1() {
		return Relation_npDAO.findOneNP1(this.id);
	}
	public Relation_np getRelation_npONE2() {
		return Relation_npDAO.findOneNP2(this.id);
	}
	//スキル
	public List<Relation_skill> getRelation_skill(){
		this.R_skill = Relation_skillDAO.findByID(this.id);
		return R_skill;
	}
	public List<Relation_skill> getRelation_skill_other(){
		this.R_skill = Relation_skillDAO.findByIDforOther(this.id);
		return R_skill;
	}
	public Relation_skill getSkill_n(String skill_junban) {
		return Relation_skillDAO.findoneByID(skill_junban, this.id);
	}
	//クラススキル
	public List<Relation_c_skill> getRelation_c_skill(){
		this.R_cs = Relation_CSDAO.findByID(this.id);
		return R_cs;
	}
	//特性
	public List<Relation_state> getRelation_state(){
		this.R_state = Relation_stateDAO.findByID(this.id);
		return R_state;
	}



	//カード枚数
	public int getB() {
		int X = (card.length() - card.replace("B","").length());
		return X;
	}
	public int getA() {
		int X = (card.length() - card.replace("A","").length());
		return X;
	}
	public int getQ() {
		int X = (card.length() - card.replace("Q","").length());
		return X;
	}

	//レア度 コスト 入手方法
	public S_rare getS_rare(){
		return S_RareDAO.findByID(this.rare_id);
	}
	//クラス名
	public S_class getS_class(){
		return S_ClassDAO.findByID(this.class_id);
	}
	//天地人
	public S_tenchijin getS_tenchijin(){
		return S_TenchijinDAO.findByID(this.tenchijin_id);
	}
	//性別
	public S_sex getS_sex(){
		return S_SexDAO.findByID(this.sex_id);
	}
	//性格
	public S_seikaku getS_seikaku(){
		return S_SeikakuDAO.findByID(this.seikaku_id);
	}
	//方針
	public S_houshin getS_houshin(){
		return S_HoushinDAO.findByID(this.houshin_id);
	}
	//色
	public Color getS_Color(){
		return ColorDAO.findByID(this.color_id);
	}



	//検索
	public Servant(int id,String name,int hp,int atk,String card,
			int rare_id,int class_id,int tenchijin_id,int sex_id,
			int seikaku_id,int houshin_id,int color_id) {
		this.id = id;
		this.name = name;
		this.hp = hp;
		this.atk = atk;
		this.card = card;
		this.rare_id = rare_id;
		this.class_id = class_id;
		this.tenchijin_id = tenchijin_id;
		this.sex_id = sex_id;
		this.seikaku_id = seikaku_id;
		this.houshin_id = houshin_id;
		this.color_id = color_id;
	}

	//計算機
	public Servant(int id,String name,int atk,int rare_id,int class_id,int tenchijin_id,
			int sex_id,int seikaku_id,int houshin_id,int color_id) {
		this.id = id;
		this.name = name;
		this.atk = atk;
		this.rare_id = rare_id;
		this.class_id = class_id;
		this.tenchijin_id = tenchijin_id;
		this.sex_id = sex_id;
		this.seikaku_id = seikaku_id;
		this.houshin_id = houshin_id;
		this.color_id = color_id;
	}

	//全乗せ 対応するものがない場合は中身がnull
	public Servant(int id,String name,int hp,int atk,String card,int b_hit,int a_hit,int q_hit,
			int ex_hit,int n_hit,float np_eff,float np_damage,float star,float star_atsumare,
			String illustrator,String cv,int rare_id,int class_id,int tenchijin_id,int sex_id,
			int seikaku_id,int houshin_id,int color_id) {
		this.id = id;
		this.name = name;
		this.hp = hp;
		this.atk = atk;
		this.card = card;
		this.b_hit = b_hit;
		this.a_hit = a_hit;
		this.q_hit = q_hit;
		this.ex_hit = ex_hit;
		this.n_hit = n_hit;
		this.np_eff = np_eff;
		this.np_damage = np_damage;
		this.star = star;
		this.star_atsumare = star_atsumare;
		this.illustrator = illustrator;
		this.cv = cv;
		this.rare_id = rare_id;
		this.class_id = class_id;
		this.tenchijin_id = tenchijin_id;
		this.sex_id = sex_id;
		this.seikaku_id = seikaku_id;
		this.houshin_id = houshin_id;
		this.color_id = color_id;
	}



	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getHp() {
		return hp;
	}
	public void setHp(int hp) {
		this.hp = hp;
	}
	public int getAtk() {
		return atk;
	}
	public void setAtk(int atk) {
		this.atk = atk;
	}
	public String getCard() {
		return card;
	}
	public void setCard(String card) {
		this.card = card;
	}
	public int getB_hit() {
		return b_hit;
	}
	public void setB_hit(int b_hit) {
		this.b_hit = b_hit;
	}
	public int getA_hit() {
		return a_hit;
	}
	public void setA_hit(int a_hit) {
		this.a_hit = a_hit;
	}
	public int getQ_hit() {
		return q_hit;
	}
	public void setQ_hit(int q_hit) {
		this.q_hit = q_hit;
	}
	public int getEx_hit() {
		return ex_hit;
	}
	public void setEx_hit(int ex_hit) {
		this.ex_hit = ex_hit;
	}
	public int getN_hit() {
		return n_hit;
	}
	public void setN_hit(int n_hit) {
		this.n_hit = n_hit;
	}
	public float getNp_eff() {
		return np_eff;
	}
	public void setNp_eff(float np_eff) {
		this.np_eff = np_eff;
	}
	public float getNp_damage() {
		return np_damage;
	}
	public void setNp_damage(float np_damage) {
		this.np_damage = np_damage;
	}
	public float getStar() {
		return star;
	}
	public void setStar(float star) {
		this.star = star;
	}
	public float getStar_atsumare() {
		return star_atsumare;
	}
	public void setStar_atsumare(float star_atsumare) {
		this.star_atsumare = star_atsumare;
	}
	public String getIllustrator() {
		return illustrator;
	}
	public void setIllustrator(String illustrator) {
		this.illustrator = illustrator;
	}
	public String getCv() {
		return cv;
	}
	public void setCv(String cv) {
		this.cv = cv;
	}
	public int getRare_id() {
		return rare_id;
	}
	public void setRare_id(int rare_id) {
		this.rare_id = rare_id;
	}
	public int getClass_id() {
		return class_id;
	}
	public void setClass_id(int class_id) {
		this.class_id = class_id;
	}
	public int getTenchijin_id() {
		return tenchijin_id;
	}
	public void setTenchijin_id(int tenchijin_id) {
		this.tenchijin_id = tenchijin_id;
	}
	public int getSex_id() {
		return sex_id;
	}
	public void setSex_id(int sex_id) {
		this.sex_id = sex_id;
	}
	public int getSeikaku_id() {
		return seikaku_id;
	}
	public void setSeikaku_id(int seikaku_id) {
		this.seikaku_id = seikaku_id;
	}
	public int getHoushin_id() {
		return houshin_id;
	}
	public void setHoushin_id(int houshin_id) {
		this.houshin_id = houshin_id;
	}
	public int getColor_id() {
		return color_id;
	}
	public void setColor_id(int color_id) {
		this.color_id = color_id;
	}
}

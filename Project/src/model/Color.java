package model;

import java.io.Serializable;

public class Color implements Serializable {
	private int id;
	private String color;
	private float in_value;




	public Color(int id,String color,float in_value) {
		this.id = id;
		this.color = color;
		this.in_value = in_value;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public float getIn_value() {
		return in_value;
	}
	public void setIn_value(float in_value) {
		this.in_value = in_value;
	}

}

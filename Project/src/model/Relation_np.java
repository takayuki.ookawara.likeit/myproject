package model;

import java.io.Serializable;

import dao.NoblePhantasmDAO;

public class Relation_np implements Serializable {
	private int id;
	private int servant_id;
	private int np_id;
	private int junban;




	public NoblePhantasm getNoblePhantasm() {
		return NoblePhantasmDAO.findByID(np_id);
	}

	public Relation_np(int id,int servant_id,int np_id,int junban) {
		this.id = id;
		this.servant_id = servant_id;
		this.np_id = np_id;
		this.junban = junban;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getServant_id() {
		return servant_id;
	}
	public void setServant_id(int servant_id) {
		this.servant_id = servant_id;
	}
	public int getNp_id() {
		return np_id;
	}
	public void setNp_id(int np_id) {
		this.np_id = np_id;
	}
	public int getJunban() {
		return junban;
	}
	public void setJunban(int junban) {
		this.junban = junban;
	}

}

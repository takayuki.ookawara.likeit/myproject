package model;

import java.io.Serializable;

public class S_rare implements Serializable {
	private int id;
	private int rare;
	private int cost;
	private String gacha;




	public S_rare(int id,int rare,int cost,String gacha) {
		this.id = id;
		this.rare = rare;
		this.cost = cost;
		this.gacha = gacha;
	}

	public S_rare(int rare) {
		this.rare = rare;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getRare() {
		return rare;
	}
	public void setRare(int rare) {
		this.rare = rare;
	}
	public int getCost() {
		return cost;
	}
	public void setCost(int cost) {
		this.cost = cost;
	}
	public String getGacha() {
		return gacha;
	}
	public void setGacha(String gacha) {
		this.gacha = gacha;
	}
}

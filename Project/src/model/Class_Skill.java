package model;

import java.io.Serializable;
import java.util.List;

import dao.CSEDAO;

public class Class_Skill implements Serializable {
	private int id;
	private String name;
	private String img_id;

	List<CS_Effect> cs_Effects;


	public List<CS_Effect> getCS_Effect(){
		this.cs_Effects = CSEDAO.findByID(this.id);
		return cs_Effects;
	}

	public Class_Skill(int id,String name,String img_id) {
		this.id = id;
		this.name = name;
		this.img_id = img_id;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImg_id() {
		return img_id;
	}
	public void setImg_id(String img_id) {
		this.img_id = img_id;
	}
}

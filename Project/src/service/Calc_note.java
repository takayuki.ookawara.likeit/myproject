package service;

import java.io.Serializable;

public class Calc_note implements Serializable {

	private static float np_bairitsu;
	private static float ATKbuff;
	private static float Bbuff;
	private static float Abuff;
	private static float Qbuff;
	private static float NPbuff;
	private static float Bdamage;
	private static float NPCbuff;
	private static float NPSbuff;
	private static float SAbuff;
	private static float Bdebuff;
	private static float Adebuff;
	private static float Qdebuff;
	private static float Ddamage;
	private static float DEFdebuff;
	private static int error;



	public static void removeALL(){
		Calc_note.Abuff = 0;
		Calc_note.Adebuff = 0;
		Calc_note.ATKbuff = 0;
		Calc_note.Bbuff = 0;
		Calc_note.Bdamage = 0;
		Calc_note.Bdebuff= 0;
		Calc_note.Ddamage = 0;
		Calc_note.DEFdebuff = 0;
		Calc_note.error = 0;
		Calc_note.np_bairitsu = 0;
		Calc_note.NPbuff = 0;
		Calc_note.Qbuff = 0;
		Calc_note.Qdebuff = 0;
		Calc_note.SAbuff = 0;
		System.out.println("値を初期化します");
	}


	public static float getNp_bairitsu() {
		return np_bairitsu;
	}
	public static void setNp_bairitsu(float np_bairitsu) {
		Calc_note.np_bairitsu = np_bairitsu;
	}
	public static float getATKbuff() {
		return ATKbuff;
	}
	public static void setATKbuff(float aTKbuff) {
		ATKbuff = aTKbuff;
	}
	public static void plusATKbuff(float aTKbuff) {
		Calc_note.ATKbuff += aTKbuff;
	}
	public static float getBbuff() {
		return Bbuff;
	}
	public static void setBbuff(float bbuff) {
		Bbuff = bbuff;
	}
	public static void plusBbuff(float bbuff) {
		Calc_note.Bbuff += bbuff;
	}
	public static float getAbuff() {
		return Abuff;
	}
	public static void setAbuff(float abuff) {
		Abuff = abuff;
	}
	public static void plusAbuff(float abuff) {
		Calc_note.Abuff += abuff;
	}
	public static float getQbuff() {
		return Qbuff;
	}
	public static void setQbuff(float qbuff) {
		Qbuff = qbuff;
	}
	public static void plusQbuff(float qbuff) {
		Calc_note.Qbuff += qbuff;
	}
	public static float getNPbuff() {
		return NPbuff;
	}
	public static void setNPbuff(float nPbuff) {
		NPbuff = nPbuff;
	}
	public static void plusNPbuff(float nPbuff) {
		Calc_note.NPbuff += nPbuff;
	}
	public static float getBdamage() {
		return Bdamage;
	}
	public static void setBdamage(float bdamage) {
		Bdamage = bdamage;
	}
	public static void plusBdamage(float bdamage) {
		Calc_note.Bdamage += bdamage;
	}
	public static float getNPCbuff() {
		return NPCbuff;
	}
	public static void setNPCbuff(float nPCbuff) {
		NPCbuff = nPCbuff;
	}
	public static void plusNPCbuff(float nPCbuff) {
		Calc_note.NPCbuff += nPCbuff;
	}
	public static float getNPSbuff() {
		return NPSbuff;
	}
	public static void setNPSbuff(float nPSbuff) {
		NPSbuff = nPSbuff;
	}
	public static void plusNPSbuff(float nPSbuff) {
		Calc_note.NPSbuff += nPSbuff;
	}
	public static float getSAbuff() {
		return SAbuff;
	}
	public static void setSAbuff(float sAbuff) {
		SAbuff = sAbuff;
	}
	public static void plusSAbuff(float sAbuff) {
		Calc_note.SAbuff += sAbuff;
	}
	public static float getBdebuff() {
		return Bdebuff;
	}
	public static void setBdebuff(float bdebuff) {
		Bdebuff = bdebuff;
	}
	public static void plusBdebuff(float bdebuff) {
		Calc_note.Bdebuff += bdebuff;
	}
	public static float getAdebuff() {
		return Adebuff;
	}
	public static void setAdebuff(float adebuff) {
		Adebuff = adebuff;
	}
	public static void plusAdebuff(float adebuff) {
		Calc_note.Adebuff += adebuff;
	}
	public static float getQdebuff() {
		return Qdebuff;
	}
	public static void setQdebuff(float qdebuff) {
		Qdebuff = qdebuff;
	}
	public static void plusQdebuff(float qdebuff) {
		Calc_note.Qdebuff += qdebuff;
	}
	public static float getDdamage() {
		return Ddamage;
	}
	public static void setDdamage(float ddamage) {
		Ddamage = ddamage;
	}
	public static void plusDdamage(float ddamage) {
		Calc_note.Ddamage += ddamage;
	}
	public static float getDEFdebuff() {
		return DEFdebuff;
	}
	public static void setDEFdebuff(float dEFdebuff) {
		DEFdebuff = dEFdebuff;
	}
	public static void plusDEFdebuff(float dEFdebuff) {
		Calc_note.DEFdebuff += dEFdebuff;
	}
	public static float getError() {
		return error;
	}
	public static void setError(int Error) {
		error = Error;
	}
	public static void plusError(int Error) {
		Calc_note.error += Error;
	}
}

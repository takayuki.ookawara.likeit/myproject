package service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import dao.ServantDAO;
import model.CS_Effect;
import model.NP_Effect;
import model.Servant;
import model.Skill_Effect;

public class Judge_bot implements Serializable {

	/**
	 * ジャッジを行い、キャラ情報などをセットする
	 * 1.[AddJudge]の取得をし、ifでnull判別
	 * 2.[AddJudge]の中身を、switchで判別
	 * 3.適した部分で、セットやリムーブを行い、ログを出す
	 * @param request
	 */
	public static void judge(HttpServletRequest request) {

		String AddJudge = request.getParameter("AddJudge");

		HttpSession req = request.getSession();
		//キャラ追加
		if(!(AddJudge == null)) {
			//値の取得
			String id = request.getParameter("addServant");
			//検索
			ServantDAO servantDAO = new ServantDAO();
			Servant addServant = servantDAO.findByIDforCalc(id);
			switch(AddJudge) {
				case "main":
					if(addServant.getRelation_npONE1() == null) {
						//セット
						request.setAttribute("alert",1);
						//ログ
						java.lang.System.out.println("宝具データがありません");
						break;
					}else if(addServant.getRelation_npONE1().getNoblePhantasm().getDescription().equals("補助")) {
						//セット
						request.setAttribute("alert",2);
						//ログ
						java.lang.System.out.println("攻撃型宝具ではないので追加できません");
						break;
					}else {
						//セット
						req.setAttribute("addServant", addServant);
						//ログ
						java.lang.System.out.println("メインに追加します");
						break;
					}

				case "sup1":
					//セット
					req.setAttribute("addSup1", addServant);
					//ログ
					java.lang.System.out.println("サポ1に追加します");
					break;

				case "sup2":
					//セット
					req.setAttribute("addSup2", addServant);
					//ログ
					java.lang.System.out.println("サポ2に追加します");
					break;

				case "sup3":
					//セット
					req.setAttribute("addSup3", addServant);
					//ログ
					java.lang.System.out.println("サポ3に追加します");
					break;

				case "sup4":
					//セット
					req.setAttribute("addSup4", addServant);
					//ログ
					java.lang.System.out.println("サポ4に追加します");
					break;

				case "sup5":
					//セット
					req.setAttribute("addSup5", addServant);
					//ログ
					java.lang.System.out.println("サポ5に追加します");
					break;

				case "resetALL":
					//リムーブ
					req.removeAttribute("addServant");
					req.removeAttribute("addSup1");
					req.removeAttribute("addSup2");
					req.removeAttribute("addSup3");
					req.removeAttribute("addSup4");
					req.removeAttribute("addSup5");
					//ログ
					java.lang.System.out.println("全てリセットしました");
					break;

				case "resetSup1":
					//リムーブ
					req.removeAttribute("addSup1");
					//ログ
					java.lang.System.out.println("サポ1をリセットしました");
					break;

				case "resetSup2":
					//リムーブ
					req.removeAttribute("addSup2");
					//ログ
					java.lang.System.out.println("サポ2をリセットしました");
					break;

				case "resetSup3":
					//リムーブ
					req.removeAttribute("addSup3");
					//ログ
					java.lang.System.out.println("サポ3をリセットしました");
					break;

				case "resetSup4":
					//リムーブ
					req.removeAttribute("addSup4");
					//ログ
					java.lang.System.out.println("サポ4をリセットしました");
					break;

				case "resetSup5":
					//リムーブ
					req.removeAttribute("addSup5");
					//ログ
					java.lang.System.out.println("サポ5をリセットしました");
					break;

				default:
					//ログ
					java.lang.System.out.println("何もしていません");
					break;
			}
		}else if(AddJudge == null){
			java.lang.System.out.println("AddJudge is null");
		}
	}


	/**
	 * mainがnullでなければ、カードバフをセットする
	 * @param request
	 * @param Bbuff
	 * @param Abuff
	 * @param Qbuff
	 * @param Bdebuff
	 * @param Adebuff
	 */
	public static void setCordbuff(HttpServletRequest request, float Bbuff, float Abuff, float Qbuff, float Bdebuff, float Adebuff) {

		HttpSession req = request.getSession();
		Servant main = (Servant)req.getAttribute("addServant");

		if(!(main == null)) {
			if(main.getColor_id() == 1) {
				req.setAttribute("Cradbuff", Bbuff);
				req.setAttribute("Craddebuff", Bdebuff);
			}else if(main.getColor_id() == 2) {
				req.setAttribute("Cradbuff", Abuff);
				req.setAttribute("Craddebuff", Adebuff);
			}else if(main.getColor_id() == 3) {
				req.setAttribute("Cradbuff", Qbuff);
				req.setAttribute("Craddebuff", Adebuff);
			}else {
				java.lang.System.out.println("カードへ値を反映できません:Color_id error　"
						+ "colorID="+main.getColor_id());
			}
		}else {
			java.lang.System.out.println("カードへ値を反映できません:main is null");
		}
	}



	/**
	 * 宝具レベルの判定を行い、倍率をセットする
	 * @param main
	 * @param NP
	 */
	public static void setNP(HttpServletRequest request) {

		HttpSession req = request.getSession();
		Servant main = (Servant)req.getAttribute("addServant");
		String NP = request.getParameter("NP");

		if(!(NP == null)) {
			if(main.getRelation_npONE2() == null) {
				Calc_note.setNp_bairitsu(main.getRelation_npONE1().getNoblePhantasm().getNP_bairitsu().getNp_bairitsu(NP));
			}else if(!(main.getRelation_npONE2() == null)){
				Calc_note.setNp_bairitsu(main.getRelation_npONE2().getNoblePhantasm().getNP_bairitsu().getNp_bairitsu(NP));
			}else {
				System.out.println("NP判定エラー:予期せぬ値　NP="+NP);
			}
		}
	}

	/**
	 * 宝具OCの判定を行い、効果をセットする
	 * @param main
	 * @param NP
	 */
	public static void setNPE(HttpServletRequest request) {

		HttpSession req = request.getSession();
		Servant main = (Servant)req.getAttribute("addServant");
		String OC = request.getParameter("OC");

		if(!(OC ==null)) {
			if(main.getRelation_npONE2()==null) {
				List<NP_Effect> NE = main.getRelation_npONE1().getNoblePhantasm().getNP_EffectB();
				for (int i=0;i < NE.size();i++) {
					if(NE.get(i).getOc() == 1) {
						//味方
						if(NE.get(i).getFore() == 1) {
							if(NE.get(i).getBord() == 1) {
								if(NE.get(i).getEf_genre() == 1) {
									Calc_note.plusATKbuff(NE.get(i).getEf_para(OC));
								}else if(NE.get(i).getEf_genre() == 2) {
									Calc_note.plusBbuff(NE.get(i).getEf_para(OC));
								}else if(NE.get(i).getEf_genre() == 3) {
									Calc_note.plusAbuff(NE.get(i).getEf_para(OC));
								}else if(NE.get(i).getEf_genre() == 4) {
									Calc_note.plusQbuff(NE.get(i).getEf_para(OC));
								}else if(NE.get(i).getEf_genre() == 5) {
									Calc_note.plusNPbuff(NE.get(i).getEf_para(OC));
								}else if(NE.get(i).getEf_genre() == 6) {
									Calc_note.plusBdamage(NE.get(i).getEf_para(OC));
								}else if(NE.get(i).getEf_genre() == 7) {
									Calc_note.plusNPCbuff(NE.get(i).getEf_para(OC));
								}else if(NE.get(i).getEf_genre() == 8) {
									Calc_note.plusNPSbuff(NE.get(i).getEf_para(OC));
								}else if(NE.get(i).getEf_genre() == 31) {
									Calc_note.plusSAbuff(NE.get(i).getEf_para(OC));
								}
							}
						//敵
						}else if(NE.get(i).getFore() == 2) {
							if(NE.get(i).getBord() == 2) {
								if(NE.get(i).getEf_genre() == 2) {
									Calc_note.plusBdebuff(NE.get(i).getEf_para(OC));
								}else if(NE.get(i).getEf_genre() == 3) {
									Calc_note.plusAdebuff(NE.get(i).getEf_para(OC));
								}else if(NE.get(i).getEf_genre() == 4) {
									Calc_note.plusQdebuff(NE.get(i).getEf_para(OC));
								}else if(NE.get(i).getEf_genre() == 28) {
									Calc_note.plusDEFdebuff(NE.get(i).getEf_para(OC));
								}else if(NE.get(i).getEf_genre() == 6) {
									Calc_note.plusDdamage(NE.get(i).getEf_para(OC));
								}
							}
						}else {
							java.lang.System.out.println("NPE判定エラー:予期せぬ値　OC="+OC);
						}
					}
				}
			}else {
				List<NP_Effect> NE = main.getRelation_npONE2().getNoblePhantasm().getNP_EffectB();
				for (int i=0;i < NE.size();i++) {
					if(NE.get(i).getOc() == 1) {
						//味方
						if(NE.get(i).getFore() == 1) {
							if(NE.get(i).getBord() == 1) {
								if(NE.get(i).getEf_genre() == 1) {
									Calc_note.plusATKbuff(NE.get(i).getEf_para(OC));
								}else if(NE.get(i).getEf_genre() == 2) {
									Calc_note.plusBbuff(NE.get(i).getEf_para(OC));
								}else if(NE.get(i).getEf_genre() == 3) {
									Calc_note.plusAbuff(NE.get(i).getEf_para(OC));
								}else if(NE.get(i).getEf_genre() == 4) {
									Calc_note.plusQbuff(NE.get(i).getEf_para(OC));
								}else if(NE.get(i).getEf_genre() == 5) {
									Calc_note.plusNPbuff(NE.get(i).getEf_para(OC));
								}else if(NE.get(i).getEf_genre() == 6) {
									Calc_note.plusBdamage(NE.get(i).getEf_para(OC));
								}else if(NE.get(i).getEf_genre() == 7) {
									Calc_note.plusNPCbuff(NE.get(i).getEf_para(OC));
								}else if(NE.get(i).getEf_genre() == 8) {
									Calc_note.plusNPSbuff(NE.get(i).getEf_para(OC));
								}else if(NE.get(i).getEf_genre() == 31) {
									Calc_note.plusSAbuff(NE.get(i).getEf_para(OC));
								}
							}
						//敵
						}else if(NE.get(i).getFore() == 2) {
							if(NE.get(i).getBord() == 2) {
								if(NE.get(i).getEf_genre() == 2) {
									Calc_note.plusBdebuff(NE.get(i).getEf_para(OC));
								}else if(NE.get(i).getEf_genre() == 3) {
									Calc_note.plusAdebuff(NE.get(i).getEf_para(OC));
								}else if(NE.get(i).getEf_genre() == 4) {
									Calc_note.plusQdebuff(NE.get(i).getEf_para(OC));
								}else if(NE.get(i).getEf_genre() == 28) {
									Calc_note.plusDEFdebuff(NE.get(i).getEf_para(OC));
								}else if(NE.get(i).getEf_genre() == 6) {
									Calc_note.plusDdamage(NE.get(i).getEf_para(OC));
								}
							}
						}else {
							java.lang.System.out.println("NPE判定エラー:予期せぬ値　OC="+OC);
						}
					}
				}
			}
		}
	}

	/**
	 * クラススキルの値をセットする
	 * @param request
	 */
	public static void setClass_Skill(HttpServletRequest request) {

		HttpSession req = request.getSession();
		Servant main = (Servant)req.getAttribute("addServant");

		for(int n = 0; n < main.getRelation_c_skill().size() ; n++) {
			for(int N = 0; N < main.getRelation_c_skill().get(n).getClass_Skill().getCS_Effect().size() ; N++) {
				List<CS_Effect> SE = main.getRelation_c_skill().get(n).getClass_Skill().getCS_Effect();
				//味方
				if(SE.get(N).getBord() == 1) {
					if(SE.get(N).getEf_genre() == 1) {
						Calc_note.plusATKbuff(SE.get(N).getEf_para());
					}else if(SE.get(N).getEf_genre() == 2) {
						Calc_note.plusBbuff(SE.get(N).getEf_para());
					}else if(SE.get(N).getEf_genre() == 3) {
						Calc_note.plusAbuff(SE.get(N).getEf_para());
					}else if(SE.get(N).getEf_genre() == 4) {
						Calc_note.plusQbuff(SE.get(N).getEf_para());
					}else if(SE.get(N).getEf_genre() == 5) {
						Calc_note.plusNPbuff(SE.get(N).getEf_para());
					}else if(SE.get(N).getEf_genre() == 6) {
						Calc_note.plusBdamage(SE.get(N).getEf_para());
					}else if(SE.get(N).getEf_genre() == 7) {
						Calc_note.plusNPCbuff(SE.get(N).getEf_para());
					}else if(SE.get(N).getEf_genre() == 8) {
						Calc_note.plusNPSbuff(SE.get(N).getEf_para());
					}else if(SE.get(N).getEf_genre() == 31) {
						Calc_note.plusSAbuff(SE.get(N).getEf_para());
					}
				}else {
					java.lang.System.out.println("クラススキル適用エラー main.class_skill判定");
				}
			}
		}
	}



	/**
	 * [main]のスキル判定を行う
	 * @param request
	 */
	public static void setSkillM(HttpServletRequest request) {

		HttpSession req = request.getSession();
		Servant main = (Servant)req.getAttribute("addServant");
		String[] skill = request.getParameterValues("skill");
		if(!(skill == null)) {
			if(Count_bot.coutSkill(skill).equals("OK")) {
				for (int f=0;f < skill.length ;f++) {
					List<Skill_Effect> SE = main.getSkill_n(skill[f]).getSkill().getSkill_Effect();
					for (int i=0;i < SE.size();i++) {
						System.out.println("敵味方　"+SE.get(i).getFore());
						System.out.println("ジャンル　"+SE.get(i).getEf_genre());
						System.out.println("バフデバフ　"+SE.get(i).getBord());
						System.out.println("数値　"+SE.get(i).getEf_para_10());
						System.out.println("-");
						//味方
						if(SE.get(i).getFore() == 1) {
							if(SE.get(i).getBord() == 1) {
								if(SE.get(i).getEf_genre() == 1) {
									Calc_note.plusATKbuff(SE.get(i).getEf_para_10());
								}else if(SE.get(i).getEf_genre() == 2) {
									Calc_note.plusBbuff(SE.get(i).getEf_para_10());
								}else if(SE.get(i).getEf_genre() == 3) {
									Calc_note.plusAbuff(SE.get(i).getEf_para_10());
								}else if(SE.get(i).getEf_genre() == 4) {
									Calc_note.plusQbuff(SE.get(i).getEf_para_10());
								}else if(SE.get(i).getEf_genre() == 5) {
									Calc_note.plusNPbuff(SE.get(i).getEf_para_10());
								}else if(SE.get(i).getEf_genre() == 6) {
									Calc_note.plusBdamage(SE.get(i).getEf_para_10());
								}else if(SE.get(i).getEf_genre() == 7) {
									Calc_note.plusNPCbuff(SE.get(i).getEf_para_10());
								}else if(SE.get(i).getEf_genre() == 8) {
									Calc_note.plusNPSbuff(SE.get(i).getEf_para_10());
								}else if(SE.get(i).getEf_genre() == 31) {
									Calc_note.plusSAbuff(SE.get(i).getEf_para_10());
								}
							}
						//敵
						}else if(SE.get(i).getFore() == 2) {
							if(SE.get(i).getBord() == 2) {
								if(SE.get(i).getEf_genre() == 2) {
									Calc_note.plusBdebuff(SE.get(i).getEf_para_10());
								}else if(SE.get(i).getEf_genre() == 3) {
									Calc_note.plusAdebuff(SE.get(i).getEf_para_10());
								}else if(SE.get(i).getEf_genre() == 4) {
									Calc_note.plusQdebuff(SE.get(i).getEf_para_10());
								}else if(SE.get(i).getEf_genre() == 28) {
									Calc_note.plusDEFdebuff(SE.get(i).getEf_para_10());
								}else if(SE.get(i).getEf_genre() == 6) {
									Calc_note.plusDdamage(SE.get(i).getEf_para_10());
								}
							}
						}else {
							java.lang.System.out.println("スキル適用エラー skill判定");
						}
					}
				}
			}else if(Count_bot.coutSkill(skill).equals("OUT")){
				System.out.println("スキル適用エラー:スキル重複");
				request.setAttribute("alert",3);
				Calc_note.plusError(1);
			}else {
				System.out.println("スキル部で謎抜け発生");
				request.setAttribute("alert",3);
				Calc_note.plusError(1);
			}
		}else {
			System.out.println("計算処理を飛ばしました");
		}
	}



	/**
	 * [Sup]のスキル判定を行う
	 * @param request
	 */
	public static void setSkillS(HttpServletRequest request) {

		HttpSession req = request.getSession();
		List<Servant> SList = new ArrayList<Servant>();
		SList.add((Servant)req.getAttribute("addSup1"));
		SList.add((Servant)req.getAttribute("addSup2"));
		SList.add((Servant)req.getAttribute("addSup3"));
		SList.add((Servant)req.getAttribute("addSup4"));
		SList.add((Servant)req.getAttribute("addSup5"));
		for (int sup=0;sup < SList.size() ;sup++) {
			System.out.println("/addSup"+(sup+1)+"の処理を開始します");
			if(!(SList.get(sup) == null)) {

				//読み込み
				String[] sup_skill = request.getParameterValues("s"+(sup+1)+"skill");
				//判定
				if(!(sup_skill == null)) {
					if(Count_bot.coutSkill(sup_skill).equals("OK")) {
						for (int f=0;f < sup_skill.length ;f++) {
							List<Skill_Effect> SE = SList.get(sup).getSkill_n(sup_skill[f]).getSkill().getSkill_Effect();
							for (int i=0;i < SE.size();i++) {
								System.out.println("敵味方　"+SE.get(i).getFore());
								System.out.println("ジャンル　"+SE.get(i).getEf_genre());
								System.out.println("バフデバフ　"+SE.get(i).getBord());
								System.out.println("数値　"+SE.get(i).getEf_para_10());
								System.out.println("-");
								if(!(SE.get(i).getTarget().getId() == 1)) {
									//味方
									if(SE.get(i).getFore() == 1) {
										if(SE.get(i).getBord() == 1) {
											if(SE.get(i).getEf_genre() == 1) {
												Calc_note.plusATKbuff(SE.get(i).getEf_para_10());
											}else if(SE.get(i).getEf_genre() == 2) {
												Calc_note.plusBbuff(SE.get(i).getEf_para_10());
											}else if(SE.get(i).getEf_genre() == 3) {
												Calc_note.plusAbuff(SE.get(i).getEf_para_10());
											}else if(SE.get(i).getEf_genre() == 4) {
												Calc_note.plusQbuff(SE.get(i).getEf_para_10());
											}else if(SE.get(i).getEf_genre() == 5) {
												Calc_note.plusNPbuff(SE.get(i).getEf_para_10());
											}else if(SE.get(i).getEf_genre() == 6) {
												Calc_note.plusBdamage(SE.get(i).getEf_para_10());
											}else if(SE.get(i).getEf_genre() == 7) {
												Calc_note.plusNPCbuff(SE.get(i).getEf_para_10());
											}else if(SE.get(i).getEf_genre() == 8) {
												Calc_note.plusNPSbuff(SE.get(i).getEf_para_10());
											}else if(SE.get(i).getEf_genre() == 31) {
												Calc_note.plusSAbuff(SE.get(i).getEf_para_10());
											}
										}
									//敵
									}else if(SE.get(i).getFore() == 2) {
										if(SE.get(i).getBord() == 2) {
											if(SE.get(i).getEf_genre() == 2) {
												Calc_note.plusBdebuff(SE.get(i).getEf_para_10());
											}else if(SE.get(i).getEf_genre() == 3) {
												Calc_note.plusAdebuff(SE.get(i).getEf_para_10());
											}else if(SE.get(i).getEf_genre() == 4) {
												Calc_note.plusQdebuff(SE.get(i).getEf_para_10());
											}else if(SE.get(i).getEf_genre() == 28) {
												Calc_note.plusDEFdebuff(SE.get(i).getEf_para_10());
											}else if(SE.get(i).getEf_genre() == 6) {
												Calc_note.plusDdamage(SE.get(i).getEf_para_10());
											}
										}
									}else {
										java.lang.System.out.println("スキル適用エラー addSup"+(sup+1)
												+":skill"+f+":effect"+i);
									}
								}
							}
						}
					}else if(Count_bot.coutSkill(sup_skill).equals("OUT")){
						System.out.println("スキル適用エラー:スキル重複:addSup"+(sup+1));
						request.setAttribute("alert",3);
						Calc_note.plusError(1);
					}else {
						System.out.println("スキル部で謎抜け発生:addSup"+(sup+1));
						request.setAttribute("alert",3);
						Calc_note.plusError(1);
					}
				}else {
					java.lang.System.out.println("計算処理を飛ばしました addSup"+(sup+1)+":skill");
				}
			}else {
				java.lang.System.out.println("addSup"+(sup+1)+" is null");
			}
		System.out.println("addSup"+(sup+1)+"の処理を終了します/");
		System.out.println();
		}
	}
}

package service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import dao.ColorDAO;
import dao.S_ClassDAO;
import dao.S_HoushinDAO;
import dao.S_SeikakuDAO;
import dao.S_SexDAO;
import dao.S_StateDAO;
import dao.S_TenchijinDAO;
import model.Color;
import model.S_class;
import model.S_houshin;
import model.S_seikaku;
import model.S_sex;
import model.S_state;
import model.S_tenchijin;

public class Choice_bot {

	/**
	 * 選択肢リストの表示に必要なモノの取得
	 * Color / Class / Sex / 方針 / 性格 / 天地人 / 特性
	 * @param req
	 */
	public static void choiseSet(HttpServletRequest request) {

		HttpSession req = request.getSession();
		/*Color*/
		ColorDAO colorDAO = new ColorDAO();
		List<Color> colorList = colorDAO.findall();
		req.setAttribute("colorList",colorList);
		/*Class*/
		S_ClassDAO classDAO = new S_ClassDAO();
		List<S_class> classList = classDAO.findall();
		req.setAttribute("classList",classList);
		/*Sex*/
		S_SexDAO sexDAO = new S_SexDAO();
		List<S_sex> sexList = sexDAO.findall();
		req.setAttribute("sexList",sexList);
		/*Houshin*/
		S_HoushinDAO houshinDAO = new S_HoushinDAO();
		List<S_houshin> houshinList = houshinDAO.findall();
		req.setAttribute("houshinList",houshinList);
		/*Seikaku*/
		S_SeikakuDAO seikakuDAO = new S_SeikakuDAO();
		List<S_seikaku> seikakuList = seikakuDAO.findall();
		req.setAttribute("seikakuList",seikakuList);
		/*Tenchijin*/
		S_TenchijinDAO tenchijinDAO = new S_TenchijinDAO();
		List<S_tenchijin> tenchijinList = tenchijinDAO.findall();
		req.setAttribute("tenchijinList",tenchijinList);
		/*State*/
		S_StateDAO stateDAO = new S_StateDAO();
		List<S_state> stateList = stateDAO.findall();
		req.setAttribute("stateList",stateList);
	}
}

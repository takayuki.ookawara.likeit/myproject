package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Servant;

public class ServantDAO {

	//リスト 全探し
	public List<Servant> findall(){
		Connection con = null;
		List<Servant> servantList = new ArrayList<Servant>();

		try {
			con=DBManager.getConnection();
			String sql = "select * from servant order by id asc";

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			System.out.println(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				int hp = rs.getInt("hp");
				int atk = rs.getInt("atk");
				String card = rs.getString("card");
				int rare_id = rs.getInt("rare_id");
				int class_id = rs.getInt("class_id");
				int tenchijin_id = rs.getInt("tenchijin_id");
				int sex_id = rs.getInt("sex_id");
				int seikaku_id = rs.getInt("seikaku_id");
				int houshin_id = rs.getInt("houshin_id");
				int color_id = rs.getInt("color_id");

				Servant servant = new Servant(id,name,hp,atk,card,rare_id,class_id,
						tenchijin_id,sex_id,seikaku_id,houshin_id,color_id);

				servantList.add(servant);
			}

		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			if(con != null){
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return servantList;
	}

	//リスト 検索
		public List<Servant> findByCalc(String servant_id, String servant_name, String s_class,
				String rare,String sex){

			Connection con = null;
			List<Servant> servantList = new ArrayList<Servant>();

			try {
				con=DBManager.getConnection();
				Statement stmt = con.createStatement();

				//SQLベース
				String sql = "select * from servant where 1 = 1 ";

				//SQLセット
				DaoUtil daoUtil = new DaoUtil(sql);

				/*servant*/
				if(!(servant_id==null)) {
					daoUtil.setEqual("id", servant_id);
				}
				if(!(servant_name==null)) {
					daoUtil.setLike("name", servant_name);
				}
				//forEachで回して、valueでIDを飛ばす組
				/*s_class*/
				if(!(s_class==null)) {
					daoUtil.setEqual("class_id", s_class);
				}
				/*s_sex*/
				if(!(sex==null)) {
					daoUtil.setEqual("sex_id", sex);
				}
				/*s_rare*/
				if(!(rare==null)) {
					if(!rare.isEmpty()) {
						ArrayList<Integer> idList = S_RareDAO.getIDListByRare(rare);
						daoUtil.setArray("rare_id", idList);
					}
				}

				/*順番*/
				daoUtil.setEnd("order by id asc");

				System.out.println(daoUtil.getBaseSQL());


				ResultSet rs = stmt.executeQuery(daoUtil.getBaseSQL());

				while (rs.next()) {
					int id = rs.getInt("id");
					String name = rs.getString("name");
					int hp = rs.getInt("hp");
					int atk = rs.getInt("atk");
					String card = rs.getString("card");
					int rare_id = rs.getInt("rare_id");
					int class_id = rs.getInt("class_id");
					int tenchijin_id = rs.getInt("tenchijin_id");
					int sex_id = rs.getInt("sex_id");
					int seikaku_id = rs.getInt("seikaku_id");
					int houshin_id = rs.getInt("houshin_id");
					int color_id = rs.getInt("color_id");

					Servant servant = new Servant(id,name,hp,atk,card,rare_id,class_id,
							tenchijin_id,sex_id,seikaku_id,houshin_id,color_id);

					servantList.add(servant);
				}

			}catch(SQLException e){
				e.printStackTrace();
			}finally{
				if(con != null){
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
			return servantList;
		}


		//リスト 検索
			public List<Servant> findByConditions(String servant_id,String servant_name,
					String cv,String illustrator,String Bcard,String Acard,String Qcard,
					String rare,String gacha,String s_class,String[] tenchijin,String sex,
					String[] seikaku,String[] houshin,String color,
					String[] chara,String description){

				Connection con = null;
				List<Servant> servantList = new ArrayList<Servant>();

				try {
					con=DBManager.getConnection();
					Statement stmt = con.createStatement();

					//SQLベース
					String sql = "select * from servant where 1 = 1 ";

					//SQLセット
					DaoUtil daoUtil = new DaoUtil(sql);

					/*servant*/
					daoUtil.setEqual("id", servant_id);
					daoUtil.setLike("name", servant_name);
					daoUtil.setLike("illustrator", illustrator);
					daoUtil.setLike("cv", cv);
					daoUtil.setLikeL("card", Bcard);
					daoUtil.setLike("card", Acard);
					daoUtil.setLikeR("card", Qcard);
					daoUtil.setEqual("color_id", color);//valueID

					//forEachで回して、valueでIDを飛ばす組
					/*s_class*/
					daoUtil.setEqual("class_id", s_class);
					/*s_sex*/
					daoUtil.setEqual("sex_id", sex);
					/*s_state*/
					if(!(chara == null)) {
						ArrayList<Integer> idList = Relation_stateDAO.getIDListByChara(chara);
						daoUtil.setArray("id", idList);
					}

					/*s_rare*/
					if(!rare.isEmpty()) {
						ArrayList<Integer> idList = S_RareDAO.getIDListByRare(rare);
						daoUtil.setArray("rare_id", idList);
					}
					if(!gacha.isEmpty()) {
						ArrayList<Integer> idList = S_RareDAO.getIDListByGacha(gacha);
						daoUtil.setArray("rare_id", idList);
					}
					/*s_tenchijin*/
					if(!(tenchijin == null)) {
						ArrayList<Integer> idList = S_TenchijinDAO.getIDListByTenchi(tenchijin);
						daoUtil.setArray("tenchijin_id", idList);
					}
					/*s_seikaku*/
					if(!(seikaku == null)) {
						ArrayList<Integer> idList = S_SeikakuDAO.getIDListBySeikaku(seikaku);
						daoUtil.setArray("seikaku_id", idList);
					}
					/*s_houshin*/
					if(!(houshin == null)) {
						ArrayList<Integer> idList = S_HoushinDAO.getIDListByHoushin(houshin);
						daoUtil.setArray("houshin_id", idList);
					}
					/*noblephantasm*/
					if(!description.isEmpty()) {
						ArrayList<Integer> idList = Relation_npDAO.getIDListByNP(description);
						daoUtil.setArray("id", idList);
					}

					/*順番*/
					daoUtil.setEnd("group by id order by id asc");

					System.out.println(daoUtil.getBaseSQL());

					ResultSet rs = stmt.executeQuery(daoUtil.getBaseSQL());

					while (rs.next()) {
						int id = rs.getInt("id");
						String name = rs.getString("name");
						int hp = rs.getInt("hp");
						int atk = rs.getInt("atk");
						String card = rs.getString("card");
						int rare_id = rs.getInt("rare_id");
						int class_id = rs.getInt("class_id");
						int tenchijin_id = rs.getInt("tenchijin_id");
						int sex_id = rs.getInt("sex_id");
						int seikaku_id = rs.getInt("seikaku_id");
						int houshin_id = rs.getInt("houshin_id");
						int color_id = rs.getInt("color_id");

						Servant servant = new Servant(id,name,hp,atk,card,rare_id,class_id,
								tenchijin_id,sex_id,seikaku_id,houshin_id,color_id);

						servantList.add(servant);
					}

				}catch(SQLException e){
					e.printStackTrace();
				}finally{
					if(con != null){
						try {
							con.close();
						} catch (SQLException e) {
							e.printStackTrace();
							return null;
						}
					}
				}
				return servantList;
			}


	public Servant findByIDforCalc(String s_id) {
		Connection con = null;
		try {
			//接続
			con = DBManager.getConnection();
			//SQL
			String sql = "select * from servant where id =?";

			//select実行
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, s_id);
			ResultSet rs = stmt.executeQuery();

			if(!rs.next()) {
				return null;
			}
			//成功時
			int id = rs.getInt("id");
			String name = rs.getString("name");
			int atk = rs.getInt("atk");
			int rare_id = rs.getInt("rare_id");
			int class_id = rs.getInt("class_id");
			int tenchijin_id = rs.getInt("tenchijin_id");
			int sex_id = rs.getInt("sex_id");
			int seikaku_id = rs.getInt("seikaku_id");
			int houshin_id = rs.getInt("houshin_id");
			int color_id = rs.getInt("color_id");

			return new Servant(id,name,atk,rare_id,class_id,
					tenchijin_id,sex_id,seikaku_id,houshin_id,color_id);

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public Servant findByIDforPage(String s_id) {
		Connection con = null;
		try {
			//接続
			con = DBManager.getConnection();
			//SQL
			String sql = "select * from servant where id =?";

			//select実行
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, s_id);
			ResultSet rs = stmt.executeQuery();

			if(!rs.next()) {
				return null;
			}
			//成功時
			int id = rs.getInt("id");
			String name = rs.getString("name");
			int hp = rs.getInt("hp");
			int atk = rs.getInt("atk");
			String card = rs.getString("card");
			int b_hit = rs.getInt("b_hit");
			int a_hit = rs.getInt("a_hit");
			int q_hit = rs.getInt("q_hit");
			int ex_hit = rs.getInt("ex_hit");
			int n_hit = rs.getInt("n_hit");
			float np_eff = rs.getFloat("np_eff");
			float np_damage = rs.getFloat("np_damage");
			float star = rs.getFloat("star");
			float star_atsumare = rs.getFloat("star_atsumare");
			String illustrator = rs.getString("illustrator");
			String cv = rs.getString("cv");
			int rare_id = rs.getInt("rare_id");
			int class_id = rs.getInt("class_id");
			int tenchijin_id = rs.getInt("tenchijin_id");
			int sex_id = rs.getInt("sex_id");
			int seikaku_id = rs.getInt("seikaku_id");
			int houshin_id = rs.getInt("houshin_id");
			int color_id = rs.getInt("color_id");

			return new Servant(id,name,hp,atk,card,b_hit,a_hit,q_hit,ex_hit,n_hit,np_eff,
					np_damage,star,star_atsumare,illustrator,cv,rare_id,class_id,
					tenchijin_id,sex_id,seikaku_id,houshin_id,color_id);

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
}

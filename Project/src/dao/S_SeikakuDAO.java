package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.S_seikaku;

public class S_SeikakuDAO {

	public static S_seikaku findByID(int id) {
		Connection con = null;
		try {
			con=DBManager.getConnection();
			String sql = "select * from s_seikaku where id =?";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

			int s_id = rs.getInt("id");
			String name = rs.getString("name");

			return new S_seikaku(s_id,name);

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public static ArrayList<Integer> getIDListBySeikaku(String[] seikaku) {

		ArrayList<Integer> idList = new ArrayList<>();

		Connection con = null;
		try {
			con=DBManager.getConnection();

			String x = "";
			for (int i = 0;i < seikaku.length;i++) {
				x += "'"+seikaku[i]+"',";
			}
			x += "''";

			String sql = "select * from s_seikaku where id in ("+x+")";
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while(rs.next()) {
				idList.add(rs.getInt("id"));
			}


			return idList;

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public List<S_seikaku> findall(){
		Connection con = null;
		List<S_seikaku> List = new ArrayList<S_seikaku>();

		try {
			con=DBManager.getConnection();
			String sql = "select * from s_seikaku";

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");

				S_seikaku in = new S_seikaku(id,name);

				List.add(in);
			}

		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			if(con != null){
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return List;
	}
}

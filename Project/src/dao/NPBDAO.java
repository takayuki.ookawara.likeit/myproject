package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.NP_bairitsu;

public class NPBDAO {

	public static NP_bairitsu findByID(int id) {
		Connection con = null;
		try {
			con=DBManager.getConnection();
			String sql = "select * from np_bairitsu where id =?";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

			float np_bairitsu_1 = rs.getFloat("np_bairitsu_1");
			float np_bairitsu_2 = rs.getFloat("np_bairitsu_2");
			float np_bairitsu_3 = rs.getFloat("np_bairitsu_3");
			float np_bairitsu_4 = rs.getFloat("np_bairitsu_4");
			float np_bairitsu_5 = rs.getFloat("np_bairitsu_5");

			return  new NP_bairitsu(np_bairitsu_1,np_bairitsu_2,np_bairitsu_3,np_bairitsu_4,
					np_bairitsu_5);

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

}

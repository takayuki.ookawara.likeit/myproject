package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.S_class;

public class S_ClassDAO {

	public static S_class findByID(int id) {
		Connection con = null;
		try {
			con=DBManager.getConnection();
			String sql = "select * from s_class where id =?";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

			int c_id = rs.getInt("id");
			String name = rs.getString("name");
			float in_value = rs.getFloat("in_value");

			return new S_class(c_id,name,in_value);

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public List<S_class> findall(){
		Connection con = null;
		List<S_class> List = new ArrayList<S_class>();

		try {
			con=DBManager.getConnection();
			String sql = "select * from s_class";

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				float in_value = rs.getFloat("in_value");

				S_class s_class = new S_class(id,name,in_value);

				List.add(s_class);
			}

		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			if(con != null){
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return List;
	}

}

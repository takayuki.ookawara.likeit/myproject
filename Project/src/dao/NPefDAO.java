package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.NP_Effect;

public class NPefDAO {

	public static List<NP_Effect> findByID(int np_id){

		Connection con = null;
		List<NP_Effect> neList = new ArrayList<NP_Effect>();

		try {
			con=DBManager.getConnection();
			String sql = "select * from np_effect where np_id =?";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, np_id);
			ResultSet rs = stmt.executeQuery();


			while (rs.next()) {
			String effect_text = rs.getString("effect_text");
			int effect_genre = rs.getInt("effect_genre");
			int turn = rs.getInt("turn");
			int bord = rs.getInt("bord");
			float effect_para_1 =rs.getFloat("effect_para1");
			float effect_para_2 =rs.getFloat("effect_para2");
			float effect_para_3 =rs.getFloat("effect_para3");
			float effect_para_4 =rs.getFloat("effect_para4");
			float effect_para_5 =rs.getFloat("effect_para5");
			int beaf = rs.getInt("beaf");
			int target_id = rs.getInt("target_id");
			int fast = rs.getInt("fast");
			int fore = rs.getInt("fore");
			int oc = rs.getInt("oc");


			NP_Effect npf = new NP_Effect(effect_text,effect_genre,turn,bord,
					effect_para_1,effect_para_2,effect_para_3,effect_para_4,effect_para_5,
					beaf,target_id,fast,fore,oc);

			neList.add(npf);
			}

		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			if(con != null){
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return neList;
	}

	public static List<NP_Effect> findByIDforBeaf(int np_id){

		Connection con = null;
		List<NP_Effect> neList = new ArrayList<NP_Effect>();

		try {
			con=DBManager.getConnection();
			String sql = "select * from np_effect where np_id =? and beaf = 1 "
					+ "and target_id < 4 or target_id > 4 and target_id > 60";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, np_id);
			ResultSet rs = stmt.executeQuery();


			while (rs.next()) {
			String effect_text = rs.getString("effect_text");
			int effect_genre = rs.getInt("effect_genre");
			int turn = rs.getInt("turn");
			int bord = rs.getInt("bord");
			float effect_para_1 =rs.getFloat("effect_para1");
			float effect_para_2 =rs.getFloat("effect_para2");
			float effect_para_3 =rs.getFloat("effect_para3");
			float effect_para_4 =rs.getFloat("effect_para4");
			float effect_para_5 =rs.getFloat("effect_para5");
			int beaf = rs.getInt("beaf");
			int target_id = rs.getInt("target_id");
			int fast = rs.getInt("fast");
			int fore = rs.getInt("fore");
			int oc = rs.getInt("oc");


			NP_Effect npf = new NP_Effect(effect_text,effect_genre,turn,bord,
					effect_para_1,effect_para_2,effect_para_3,effect_para_4,effect_para_5,
					beaf,target_id,fast,fore,oc);

			neList.add(npf);
			}

		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			if(con != null){
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return neList;
	}

}

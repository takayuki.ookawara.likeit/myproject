package dao;

import java.util.ArrayList;

public class DaoUtil {

	private String baseSQL;


	//SQL文の書き換え
	public DaoUtil(String baseSQL) {
		this.baseSQL = baseSQL;
	}
	//SQL "="の場合
	public void setEqual(String columName,String param) {
		if (!param.isEmpty()) {
			this.baseSQL += " and "+columName+" = '"+param+"'";
		}
	}
	//SQL "Like"の場合
	public void setLike(String columName,String param) {
		if (!param.isEmpty()) {
			this.baseSQL += " and "+columName+" Like '%"+param+"%'";
		}
	}
	public void setLikeR(String columName,String param) {
		if (!param.isEmpty()) {
			this.baseSQL += " and "+columName+" Like '"+param+"%'";
		}
	}
	public void setLikeL(String columName,String param) {
		if (!param.isEmpty()) {
			this.baseSQL += " and "+columName+" Like '%"+param+"'";
		}
	}
	//SQL "List"の場合
	public void setArray(String columName, ArrayList<Integer> params) {
		if (!(params == null)) {
			String x = "";
			for (int i = 0;i < params.size();i++) {
				x += "'"+ params.get(i) +"',";
			}
			x += "''";
			this.baseSQL += " and "+columName+" in ("+x+")";
		}
	}
	//SQL 文末
	public void setEnd(String Conditions) {
		this.baseSQL += " "+Conditions;
	}



	public String getBaseSQL() {
		return baseSQL;
	}
	public void setBaseSQL(String baseSQL) {
		this.baseSQL = baseSQL;
	}

}

package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Relation_c_skill;

public class Relation_CSDAO {

	public static List<Relation_c_skill> findByID(int id) {
		Connection con = null;
		List<Relation_c_skill> List = new ArrayList<Relation_c_skill>();

		try {
			con=DBManager.getConnection();
			String sql = "select * from c_skill_relation where servant_id =?";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				int l_id = rs.getInt("id");
				int servant_id = rs.getInt("servant_id");
				int c_skill_id = rs.getInt("c_skill_id");
				int junban = rs.getInt("junban");

				Relation_c_skill in = new Relation_c_skill(l_id,servant_id,c_skill_id,junban);
				List.add(in);
			}

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return List;
	}
}

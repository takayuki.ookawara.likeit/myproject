package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Relation_np;


public class Relation_npDAO {

	public static Relation_np findOneNP1(int servantID) {
		Connection con = null;
		try {
			con=DBManager.getConnection();
			String sql = "select * from np_relation where junban = 1 and servant_id =?";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, servantID);
			ResultSet rs = stmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

			int id = rs.getInt("id");
			int servant_id = rs.getInt("servant_id");
			int np_id = rs.getInt("np_id");
			int junban = rs.getInt("junban");

			return new Relation_np(id,servant_id,np_id,junban);

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public static Relation_np findOneNP2(int servantID) {
		Connection con = null;
		try {
			con=DBManager.getConnection();
			String sql = "select * from np_relation where junban = 2 and servant_id =?";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, servantID);
			ResultSet rs = stmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

			int id = rs.getInt("id");
			int servant_id = rs.getInt("servant_id");
			int np_id = rs.getInt("np_id");
			int junban = rs.getInt("junban");

			return new Relation_np(id,servant_id,np_id,junban);

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//重複省き
	public List<Relation_np> findall() {
		Connection con = null;
		List<Relation_np> List = new ArrayList<Relation_np>();

		try {
			con=DBManager.getConnection();
			String sql = "select * from np_relation group by servant_id";

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int l_id = rs.getInt("id");
				int servant_id = rs.getInt("servant_id");
				int np_id = rs.getInt("np_id");
				int junban = rs.getInt("junban");

				Relation_np in = new Relation_np(l_id,servant_id,np_id,junban);
				List.add(in);
			}

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return List;
	}

	public static List<Relation_np> findByID(int id) {
		Connection con = null;
		List<Relation_np> List = new ArrayList<Relation_np>();

		try {
			con=DBManager.getConnection();
			String sql = "select * from np_relation where servant_id =?";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				int l_id = rs.getInt("id");
				int servant_id = rs.getInt("servant_id");
				int np_id = rs.getInt("np_id");
				int junban = rs.getInt("junban");

				Relation_np in = new Relation_np(l_id,servant_id,np_id,junban);
				List.add(in);
			}

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return List;
	}

	public static ArrayList<Integer> getIDListByNP(String chara) {

		ArrayList<Integer> idList = new ArrayList<>();

		Connection con = null;
		try {
			con=DBManager.getConnection();
			String sql = "select * from np_relation npr join noblephantasm np on npr.np_id = np.id"
					+ " where np.description = ?";

			PreparedStatement stmt = con.prepareStatement(sql);

			stmt.setString(1,chara);
			ResultSet rs = stmt.executeQuery();

			while(rs.next()) {
				idList.add(rs.getInt("servant_id"));
			}


			return idList;

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
}

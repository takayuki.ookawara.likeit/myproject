package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.Target;

public class TargetDAO {

	public static Target findByID(int id) {
		Connection con = null;
		try {
			con=DBManager.getConnection();
			String sql = "select * from target where id =?";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

			int t_id = rs.getInt("id");
			int state_id = rs.getInt("state_id");
			int class_id = rs.getInt("class_id");
			int rare_id = rs.getInt("rare_id");
			int tenchijin_id = rs.getInt("tenchijin_id");
			int seikaku_id = rs.getInt("seikaku_id");
			int houshin_id = rs.getInt("houshin_id");
			int sex_id = rs.getInt("sex_id");
			String tage_name = rs.getString("target_name");
			String desc = rs.getString("description");

			return  new Target(t_id,state_id,class_id,rare_id,tenchijin_id,seikaku_id,
					houshin_id,sex_id,tage_name,desc);

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

}

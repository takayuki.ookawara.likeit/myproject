package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.S_rare;

public class S_RareDAO {


	public static S_rare findByID(int id) {
		Connection con = null;
		try {
			con=DBManager.getConnection();
			String sql = "select * from s_rare where id =?";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

			int s_id = rs.getInt("id");
			int rare = rs.getInt("rare");
			int cost = rs.getInt("cost");
			String gacha = rs.getString("gacha");

			return new S_rare(s_id,rare,cost,gacha);

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public static ArrayList<Integer> getIDListByRare(String rare) {

		ArrayList<Integer> idList = new ArrayList<>();

		Connection con = null;
		try {
			con=DBManager.getConnection();
			String sql = "select * from s_rare where rare =?";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, rare);
			ResultSet rs = stmt.executeQuery();

			while(rs.next()) {
				idList.add(rs.getInt("id"));
			}


			return idList;

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public static ArrayList<Integer> getIDListByGacha(String gacha) {

		ArrayList<Integer> idList = new ArrayList<>();

		Connection con = null;
		try {
			con=DBManager.getConnection();
			String sql = "select * from s_rare where gacha Like '%"+gacha+"%'";

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while(rs.next()) {
				idList.add(rs.getInt("id"));
			}


			return idList;

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public List<S_rare> findall(){
		Connection con = null;
		List<S_rare> List = new ArrayList<S_rare>();

		try {
			con=DBManager.getConnection();
			String sql = "select * from s_rare group by rare";

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int rare = rs.getInt("rare");

				S_rare in = new S_rare(rare);

				List.add(in);
			}

		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			if(con != null){
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return List;
	}
}

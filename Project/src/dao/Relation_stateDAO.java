package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Relation_state;

public class Relation_stateDAO {

	public static List<Relation_state> findByID(int id) {
		Connection con = null;
		List<Relation_state> List = new ArrayList<Relation_state>();

		try {
			con=DBManager.getConnection();
			String sql = "select * from state_relation where servant_id =?";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				int l_id = rs.getInt("id");
				int servant_id = rs.getInt("servant_id");
				int state_id = rs.getInt("state_id");

				Relation_state in = new Relation_state(l_id,servant_id,state_id);
				List.add(in);
			}

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return List;
	}

	public static ArrayList<Integer> getIDListByChara(String[] chara) {

		ArrayList<Integer> idList = new ArrayList<>();

		Connection con = null;
		try {
			con=DBManager.getConnection();

			String x = "";
			for (int i = 0;i < chara.length;i++) {
				if(i < chara.length-1) {
					x += "'"+chara[i]+"'or state_id =";
				}else if(i == chara.length-1) {
					x += "'"+chara[i]+"'";
				}
			}

			String sql = "select *,count(servant_id) from state_relation where state_id = "+x+
			" group by servant_id having count(servant_id) >"+(chara.length-1);

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while(rs.next()) {
				idList.add(rs.getInt("servant_id"));
			}


			return idList;

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public static Relation_state findforTage(int ID) {
		Connection con = null;
		try {
			con=DBManager.getConnection();
			String sql = "select * from state_relation where id =?";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, ID);
			ResultSet rs = stmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

			int id = rs.getInt("id");
			int servant_id = rs.getInt("servant_id");
			int state_id = rs.getInt("state_id");

			return new Relation_state(id,servant_id,state_id);

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

}

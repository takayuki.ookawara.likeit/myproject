package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.Class_Skill;

public class CSDAO {

	public static Class_Skill findByID(int s_id) {
		Connection con = null;
		try {
			con=DBManager.getConnection();
			String sql = "select * from class_skill where id =?";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, s_id);
			ResultSet rs = stmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

			int id = rs.getInt("id");
			String name = rs.getString("name");
			String img_id = rs.getString("img_id");

			return  new Class_Skill(id,name,img_id);

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}
}

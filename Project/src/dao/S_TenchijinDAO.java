package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.S_tenchijin;

public class S_TenchijinDAO {

	public static S_tenchijin findByID(int id) {
		Connection con = null;
		try {
			con=DBManager.getConnection();
			String sql = "select * from s_tenchijin where id =?";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

			int s_id = rs.getInt("id");
			String name = rs.getString("name");

			return new S_tenchijin(s_id,name);

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public static ArrayList<Integer> getIDListByTenchi(String[] tenchijin) {

		ArrayList<Integer> idList = new ArrayList<>();

		Connection con = null;
		try {
			con=DBManager.getConnection();

			String x = "";
			for (int i = 0;i < tenchijin.length;i++) {
				x += "'"+tenchijin[i]+"',";
			}
			x += "''";

			String sql = "select * from s_tenchijin where id in ("+x+")";
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while(rs.next()) {
				idList.add(rs.getInt("id"));
			}


			return idList;

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public List<S_tenchijin> findall(){
		Connection con = null;
		List<S_tenchijin> List = new ArrayList<S_tenchijin>();

		try {
			con=DBManager.getConnection();
			String sql = "select * from s_tenchijin";

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");

				S_tenchijin in = new S_tenchijin(id,name);

				List.add(in);
			}

		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			if(con != null){
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return List;
	}
}

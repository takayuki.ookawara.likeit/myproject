package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Relation_skill;

public class Relation_skillDAO {

	public static Relation_skill findoneByID(String skill_junban,int servantID) {
		Connection con = null;
		try {
			con=DBManager.getConnection();
			String sql = "select * from skill_relation where junban = ? and servant_id =?";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, skill_junban);
			stmt.setInt(2, servantID);
			ResultSet rs = stmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

			int id = rs.getInt("id");
			int servant_id = rs.getInt("servant_id");
			int skill_id = rs.getInt("skill_id");
			int junban = rs.getInt("junban");

			return new Relation_skill(id,servant_id,skill_id,junban);

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public static List<Relation_skill> findByID(int id) {
		Connection con = null;
		List<Relation_skill> List = new ArrayList<Relation_skill>();

		try {
			con=DBManager.getConnection();
			String sql = "select * from skill_relation where junban <= 6 and servant_id =? order by junban asc";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				int l_id = rs.getInt("id");
				int servant_id = rs.getInt("servant_id");
				int skill_id = rs.getInt("skill_id");
				int junban = rs.getInt("junban");

				Relation_skill in = new Relation_skill(l_id,servant_id,skill_id,junban);
				List.add(in);
			}

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return List;
	}

	public static List<Relation_skill> findByIDforOther(int id) {
		Connection con = null;
		List<Relation_skill> List = new ArrayList<Relation_skill>();

		try {
			con=DBManager.getConnection();
			String sql = "select * from skill_relation where junban >= 7 and servant_id =?";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				int l_id = rs.getInt("id");
				int servant_id = rs.getInt("servant_id");
				int skill_id = rs.getInt("skill_id");
				int junban = rs.getInt("junban");

				Relation_skill in = new Relation_skill(l_id,servant_id,skill_id,junban);
				List.add(in);
			}

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return List;
	}
}

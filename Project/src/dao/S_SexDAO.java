package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.S_sex;

public class S_SexDAO {

	public static S_sex findByID(int id) {
		Connection con = null;
		try {
			con=DBManager.getConnection();
			String sql = "select * from s_sex where id =?";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

			int s_id = rs.getInt("id");
			String name = rs.getString("name");

			return new S_sex(s_id,name);

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public List<S_sex> findall(){
		Connection con = null;
		List<S_sex> List = new ArrayList<S_sex>();

		try {
			con=DBManager.getConnection();
			String sql = "select * from s_sex";

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");

				S_sex in = new S_sex(id,name);

				List.add(in);
			}

		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			if(con != null){
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return List;
	}

}

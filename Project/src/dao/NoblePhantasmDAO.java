package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.NoblePhantasm;

public class NoblePhantasmDAO {

	public static NoblePhantasm findByID(int id) {
		Connection con = null;
		try {
			con=DBManager.getConnection();
			String sql = "select * from noblephantasm where id =?";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

			int s_id = rs.getInt("id");
			String name = rs.getString("name");
			String ruby = rs.getString("ruby");
			String rank = rs.getString("rank");
			String type = rs.getString("type");
			int bairitsu_id = rs.getInt("bairitsu_id");
			String damage_text = rs.getString("damage_text");
			String description = rs.getString("description");

			return new NoblePhantasm(s_id,name,ruby,rank,type,bairitsu_id,damage_text,description);

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}


}

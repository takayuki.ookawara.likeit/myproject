package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.CS_Effect;

public class CSEDAO {

	public static List<CS_Effect> findByID(int cs_id){
		Connection con = null;
		List<CS_Effect> neList = new ArrayList<CS_Effect>();

		try {
			con=DBManager.getConnection();
			String sql = "select * from class_skill_effect where class_skill_id =?";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, cs_id);
			ResultSet rs = stmt.executeQuery();


			while (rs.next()) {
			int id = rs.getInt("id");
			String ef_text = rs.getString("effect_text");
			int ef_genre = rs.getInt("effect_genre");
			int bord = rs.getInt("bord");
			float ef_para = rs.getFloat("effect_para");
			int tageid = rs.getInt("target_id");


			CS_Effect npf = new CS_Effect(id,ef_text,ef_genre,bord,ef_para,tageid);

			neList.add(npf);
			}

		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			if(con != null){
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return neList;
	}
}

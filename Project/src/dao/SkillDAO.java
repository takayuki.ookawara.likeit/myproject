package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Skill;

public class SkillDAO {

	//リスト 全探し
	public List<Skill> findall(){
		Connection con = null;
		List<Skill> SkillList = new ArrayList<Skill>();

		try {
			con=DBManager.getConnection();
			String sql = "select * from servant_skill";

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				int ct = rs.getInt("ct");

				Skill Skill = new Skill(id,name,ct);

				SkillList.add(Skill);
			}

		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			if(con != null){
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return SkillList;
	}

	//検索
	public List<Skill> findByConditions(String skill_name,String fore,String target_id,
			String effect_genre_buf,String effect_genre_deb){
		Connection con = null;
		List<Skill> SkillList = new ArrayList<Skill>();

		try {
			con=DBManager.getConnection();
			Statement stmt = con.createStatement();

			//SQLベース
			String sql = "select * from servant_skill s join servant_skill_effect se"
					+ " on s.id = se.skill_id join target tg"
					+ " on se.target_id = tg.id where 1=1";

			//skill_name
			if(!(skill_name.isEmpty())) {
				sql = sql + " and s.name Like '%"+skill_name+"%'";
			}
			//fore
			if(!(fore.isEmpty())) {
				sql = sql + " and se.fore = '"+fore+"'";
			}
			//target_id
			if(!(target_id.isEmpty())) {
				sql = sql + " and se.target_id "+target_id;
			}
			//effect_genre_buf
			if(!(effect_genre_buf.isEmpty())) {
				sql = sql + " and bord = 1 and se.effect_genre = '"+effect_genre_buf+"'";
			}
			//effect_genre_deb
			if(!(effect_genre_deb.isEmpty())) {
				sql = sql + " and bord = 2 and se.effect_genre = '"+effect_genre_deb+"'";
			}

			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				int ct = rs.getInt("ct");

				Skill skill = new Skill(id,name,ct);

				SkillList.add(skill);
			}

		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			if(con != null){
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return SkillList;
	}

	//鯖スキル取得用
	public static Skill findByID(int s_id) {
		Connection con = null;
		try {
			con=DBManager.getConnection();
			String sql = "select * from servant_skill where id =?";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, s_id);
			ResultSet rs = stmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

			int id = rs.getInt("id");
			String name = rs.getString("name");
			int ct = rs.getInt("ct");

			return  new Skill(id,name,ct);

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con !=null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}



}

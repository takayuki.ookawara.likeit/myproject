package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Skill_Effect;

public class SEDAO {

	public static List<Skill_Effect> findByID(int id) {
		Connection con = null;
		List<Skill_Effect> seList = new ArrayList<Skill_Effect>();

		try {
			con=DBManager.getConnection();
			String sql = "select * from servant_skill_effect where skill_id =?";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();


			while (rs.next()) {
			String effect_text = rs.getString("effect_text");
			int effect_genre = rs.getInt("effect_genre");
			int turn = rs.getInt("turn");
			int bord = rs.getInt("bord");
			float effect_para_1 =rs.getFloat("effect_para_1");
			float effect_para_2 =rs.getFloat("effect_para_2");
			float effect_para_3 =rs.getFloat("effect_para_3");
			float effect_para_4 =rs.getFloat("effect_para_4");
			float effect_para_5 =rs.getFloat("effect_para_5");
			float effect_para_6 =rs.getFloat("effect_para_6");
			float effect_para_7 =rs.getFloat("effect_para_7");
			float effect_para_8 =rs.getFloat("effect_para_8");
			float effect_para_9 =rs.getFloat("effect_para_9");
			float effect_para_10 =rs.getFloat("effect_para_10");
			int target_id = rs.getInt("target_id");
			int fore = rs.getInt("fore");


			Skill_Effect skilleffect = new Skill_Effect(effect_text,effect_genre,turn,bord,
					effect_para_1,effect_para_2,effect_para_3,effect_para_4,effect_para_5,
					effect_para_6,effect_para_7,effect_para_8,effect_para_9,effect_para_10,
					target_id,fore);

			seList.add(skilleffect);
			}

		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			if(con != null){
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return seList;
	}




}

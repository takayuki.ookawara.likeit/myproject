package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ServantDAO;
import model.Servant;
import service.Choice_bot;

/**
 * Servlet implementation class ServantL_Servlet
 */
@WebServlet("/ServantL_Servlet")
public class ServantL_Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServantL_Servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//エンコード
		request.setCharacterEncoding("UTF-8");
		HttpSession req = request.getSession();

		/*Servant*/
		ServantDAO servantDAO = new ServantDAO();
		List<Servant> servantList = servantDAO.findall();
		System.out.println("初期状態のリストを設置しました");
		req.setAttribute("servantList",servantList);
		req.setAttribute("servant_countList",servantList);

		//色などの選択肢の取得・セット
		Choice_bot.choiseSet(request);

		//ふぉわーど
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Servant_List.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//エンコード
		request.setCharacterEncoding("UTF-8");
		//値の取得
		/*servant*/
		String servant_id = request.getParameter("servant_id");
		String servant_name = request.getParameter("servant_name");
		String Bcard = request.getParameter("Bcard");
		String Acard = request.getParameter("Acard");
		String Qcard = request.getParameter("Qcard");
		String illustrator = request.getParameter("illustrator");
		String cv = request.getParameter("cv");
		String gacha = request.getParameter("gacha");
		String sex = request.getParameter("sex");
		String s_class = request.getParameter("s_class");
		String rare = request.getParameter("rare");
		String[] tenchijin = request.getParameterValues("tenchijin");
		String[] houshin = request.getParameterValues("houshin");
		String[] seikaku = request.getParameterValues("seikaku");
		String NPcolor = request.getParameter("NPcolor");
		/*s_state*/
		String[] chara = request.getParameterValues("chara");
		/*noblephantasm*/
		String description = request.getParameter("description");

		//検索
		ServantDAO servantDAO = new ServantDAO();
		List<Servant> servantList = servantDAO.findByConditions(servant_id,servant_name,cv,
				illustrator,Bcard,Acard,Qcard,rare,gacha,s_class,tenchijin,sex,seikaku,houshin,
				NPcolor,chara,description);
		System.out.println("条件検索を行いました");
		List<Servant> countList = servantDAO.findall();
		System.out.println("カウンター用の検索を行いました");
		//セット
		request.setAttribute("servantList",servantList);
		request.setAttribute("servant_countList",countList);
		//ふぉわーど
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Servant_List.jsp");
		dispatcher.forward(request, response);


	}

}

package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.SkillDAO;
import model.Skill;

/**
 * Servlet implementation class SkillL_Servlet
 */
@WebServlet("/SkillL_Servlet")
public class SkillL_Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SkillL_Servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//取得
		HttpSession req = request.getSession();
		SkillDAO skillDAO = new SkillDAO();
		List<Skill> SkillList = skillDAO.findall();
		//セット
		req.setAttribute("SkillList",SkillList);
		req.setAttribute("skill_coutList",SkillList);
		//ふぉわーど
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Skill_List.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//エンコード
		request.setCharacterEncoding("UTF-8");

		//値の取得
		String skill_name = request.getParameter("skill_name");
		String fore = request.getParameter("fore");
		String target_id = request.getParameter("target_id");
		String effect_genre_buf = request.getParameter("effect_genre_buf");
		String effect_genre_deb = request.getParameter("effect_genre_deb");
		//検索
		SkillDAO skillDAO = new SkillDAO();
		List<Skill> SkillList = skillDAO.findByConditions(skill_name,fore,target_id,effect_genre_buf,
				effect_genre_deb);
		//セット
		HttpSession req = request.getSession();
		req.setAttribute("SkillList",SkillList);
		//ふぉわーど
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Skill_List.jsp");
		dispatcher.forward(request, response);
	}

}

package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ServantDAO;
import model.Servant;

/**
 * Servlet implementation class ServantP_Servlet
 */
@WebServlet("/ServantP_Servlet")
public class ServantP_Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServantP_Servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//エンコード
		request.setCharacterEncoding("UTF-8");
		//取得
		String id = request.getParameter("id");
		//キャラデータ取得
		ServantDAO servantDAO = new ServantDAO();
		Servant servant = servantDAO.findByIDforPage(id);
		//キャラデータセット
		request.setAttribute("servant",servant);
		//ふぉわーど
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Servant_Page.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ふぉわーど
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Servant_Page.jsp");
		dispatcher.forward(request, response);
	}

}

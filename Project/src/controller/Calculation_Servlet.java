package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ServantDAO;
import model.Servant;
import service.Calc_note;
import service.Choice_bot;
import service.Judge_bot;

/**
 * Servlet implementation class Calculation_Servlet
 */
@WebServlet("/Calculation_Servlet")
public class Calculation_Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Calculation_Servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//エンコード
		request.setCharacterEncoding("UTF-8");
		//実行に必要なもの
		ServantDAO servantDAO = new ServantDAO();
		HttpSession req = request.getSession();

		//値の取得 - リスト
		String servant_id = request.getParameter("servant_id");
		String servant_name = request.getParameter("servant_name");
		String sex = request.getParameter("sex");
		String rare = request.getParameter("rare");
		String s_class = request.getParameter("s_class");

		//ジャッジ+α
		Judge_bot.judge(request);

		//検索 - リスト
		List<Servant> servantList = servantDAO.findByCalc(servant_id,servant_name,s_class,rare,sex);
		//セット
		req.setAttribute("servantList", servantList);
		/*入力履歴*/
		request.setAttribute("serHis", servant_name);
		request.setAttribute("claHis", s_class);
		request.setAttribute("rarHis", rare);
		request.setAttribute("sexHis", sex);

		//色などの選択肢の取得・セット
		Choice_bot.choiseSet(request);

		//ふぉわーど
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Calculation.jsp");
		dispatcher.forward(request, response);
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//エンコード
		request.setCharacterEncoding("UTF-8");
		//実行に必要なもの
		HttpSession req = request.getSession();
		String SkillRef = request.getParameter("SkillRef");
		//値の初期化
		Calc_note.removeALL();
		//値の反映
		if(!(SkillRef == null)) {
			//読み込み
			Servant main = (Servant)req.getAttribute("addServant");
			//判定
			if(!(main == null)) {
				System.out.println();
				System.out.println("/mainの処理を開始します");
				//読み込み
				String NP = request.getParameter("NP");
				String OC = request.getParameter("OC");
				request.setAttribute("npHis", NP);
				request.setAttribute("ocHis", OC);
				//クラススキル
				Judge_bot.setClass_Skill(request);
				//宝具関連の判定
				Judge_bot.setNP(request);
				Judge_bot.setNPE(request);
				//スキルの判定
				Judge_bot.setSkillM(request);
				//ログ
				System.out.println("mainの処理を終了します/");
				System.out.println();
			}
			//サポート
			Judge_bot.setSkillS(request);

			//数値のセット
			if(Calc_note.getError() == 0) {
				req.setAttribute("ATKbuff", Calc_note.getATKbuff());
				req.setAttribute("NPbuff", Calc_note.getNPbuff());
				req.setAttribute("NPCbuff", Calc_note.getNPCbuff());
				req.setAttribute("NPSbuff", Calc_note.getNPSbuff());
				req.setAttribute("SAbuff", Calc_note.getSAbuff());
				req.setAttribute("DEFdebuff", Calc_note.getDEFdebuff());
				req.setAttribute("Bdamage", Calc_note.getBdamage());
				req.setAttribute("Ddamage", Calc_note.getDdamage());
				req.setAttribute("np_bairitsu", Calc_note.getNp_bairitsu());
				Judge_bot.setCordbuff(request, Calc_note.getBbuff(), Calc_note.getAbuff(),
						Calc_note.getQbuff(), Calc_note.getBdebuff(), Calc_note.getAdebuff());
			}
		}else if(SkillRef == null) {
			java.lang.System.out.println("SkillRef is null");
		}



	//ふぉわーど
	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Calculation.jsp");
	dispatcher.forward(request, response);
	}



}
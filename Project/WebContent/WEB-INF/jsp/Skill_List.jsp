<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
		<script type="text/javascript" src="js/jquery.tablesorter.js"></script>
		<script type="text/javascript" src="js/FGO_DB.js"></script>
		<link rel="stylesheet" href="css/FGO_DB.css">
		<meta charset="UTF-8">
		<title>スキルリスト</title>
	</head>
	<body>
		<jsp:include page="header.jsp" />
		<!-- でかでかとタイトル -->
		<div class="container content">
			<div class="row">
				<div class="col-2">

				</div>
				<div class="col-8 text-center">
					<h1>スキル検索</h1>
				</div>
				<div class="col-2 text-right">

				</div>
			</div>
			<br>
			<!-- ここにフォームとか -->
			<form action="SkillL_Servlet" method="post">
				<div class="row text-center">
					<div class="col">
						スキル名<br>
					</div>
					<div class="col">
						<input class="form-control form-control-sm" name="skill_name" type="text" placeholder="スキル名"><br>
					</div>
					<div class="col text-right">
						対象<br>
					</div>
					<div class="col">
						<label><input type="radio" name="fore" value="" checked="checked"> 指定なし</label>
						<label><input type="radio" name="fore" value="1"> 味方</label>
						<label><input type="radio" name="fore" value="2"> 敵</label>
					</div>
					<div class="col">
						<select class="form-control form-control-sm" name="target_id">
							<option value="">-</option>
							<option value="= 1">自身</option>
							<option value="= 2">単体</option>
							<option value="= 5">単体〔男性〕</option>
							<option value="= 6">単体〔女性〕</option>
							<option value="= 7">単体〔異性〕</option>
							<option value="between 11 and 35">単体〔特性〕</option>
							<option value="= 3">全体</option>
							<option value="= 4">全体(自身を除く)</option>
							<option value="= 8">全体〔男性〕</option>
							<option value="= 9">全体〔女性〕</option>
							<option value="= 10">全体〔異性〕</option>
							<option value="between 36 and 60">全体〔特性〕</option>
						</select>
						<br>
					</div>
				</div>
				<div class="row text-center">
					<div class="col">
						効果（バフのみ）<br>
					</div>
					<div class="col">
						<select class="form-control form-control-sm" name="effect_genre_buf">
							<option value="">-</option>
							<option value="1">攻撃力</option>
							<option value="28">防御力</option>
							<option value="9">クリティカル威力</option>
							<option value="10">スター発生率</option>
							<option value="19">スター集中度</option>
							<option value="20">スター獲得</option>
							<option value="5">宝具威力</option>
							<option value="2">B強化</option>
							<option value="3">A強化</option>
							<option value="4">Q強化</option>
							<option value="29">特攻付与</option>
							<option value="6">ダメージプラス</option>
							<option value="11">NP獲得量</option>
							<option value="12">NPを増やす</option>
							<option value="21">確率アップ系</option>
							<option value="13">弱体耐性</option>
							<option value="14">弱体無効</option>
							<option value="22">回避・無敵</option>
							<option value="23">ターゲット集中</option>
							<option value="24">最大HP</option>
							<option value="25">回復</option>
							<option value="26">回復量アップ</option>
							<option value="27">ガッツ</option>
						</select>
						<br>
					</div>
					<div class="col">
						効果（デバフのみ）<br>
					</div>
					<div class="col">
						<select name="effect_genre_deb" class="form-control form-control-sm">
							<option value="">-</option>
							<option value="1">攻撃力</option>
							<option value="27">防御力</option>
							<option value="9">クリティカル威力</option>
							<option value="10">クリティカル発生率</option>
							<option value="2">Bカード耐性</option>
							<option value="3">Aカード耐性</option>
							<option value="4">Qカード耐性</option>
							<option value="5">宝具威力</option>
							<option value="11">チャージ減</option>
							<option value="12">宝具封印</option>
							<option value="15">状態異常（毒、火傷、呪いなど）</option>
							<option value="17">行動制御系</option>
							<option value="16">強化状態解除</option>
							<option value="13">弱体耐性</option>
							<option value="14">強化無効状態</option>
						</select>
						<br>
					</div>
				</div>
				<br>
				<button type="submit" class="btn btn-secondary btn-lg btn-block">検索</button>
			</form>
			<div class="row">
				<div class="col">
					<font size="1" color="red">
						<strong>
							※スキルの順は適当で、ソート機能ありません
						</strong>
					</font>
				</div>
				<div class="col text-right">
					${fn:length(skill_coutList)}件中 ${fn:length(SkillList)}件がヒットしました。
				</div>
			</div>
			<!-- ここから一覧 -->
			<font size="2">
				<table class="table table-bordered table-sm table-hover">
					<thead class="thead-dark">
						<tr>
							<th scope="col">スキル名</th>
							<th scope="col">CT</th>
							<th scope="col">効果</th>
							<th scope="col">SLv1</th>
							<th scope="col">SLv2</th>
							<th scope="col">SLv3</th>
							<th scope="col">SLv4</th>
							<th scope="col">SLv5</th>
							<th scope="col">SLv6</th>
							<th scope="col">SLv7</th>
							<th scope="col">SLv8</th>
							<th scope="col">SLv9</th>
							<th scope="col">SLv10</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="skill" items="${SkillList}" >
							<tr>
								<th scope="row" rowspan="${fn:length(skill.skill_Effect)+1}">${skill.name}</th>
								<td rowspan="${fn:length(skill.skill_Effect)+1}">${skill.ct}</td>
							</tr>
							<c:forEach var="skillE" items="${skill.skill_Effect}" >
							<tr>
								<td>${skillE.ef_text}</td>
								<c:choose>
									<c:when test="${skillE.ef_para_1 > 0}">
										<td>${skillE.ef_para_1}</td>
									</c:when>
									<c:when test="${skillE.ef_para_1 == 0}">
										<td></td>
									</c:when>
								</c:choose>
								<c:choose>
									<c:when test="${skillE.ef_para_2 > 0}">
										<td>${skillE.ef_para_2}</td>
									</c:when>
									<c:when test="${skillE.ef_para_2 == 0}">
										<td></td>
									</c:when>
								</c:choose>
								<c:choose>
									<c:when test="${skillE.ef_para_3 > 0}">
										<td>${skillE.ef_para_3}</td>
									</c:when>
									<c:when test="${skillE.ef_para_3 == 0}">
										<td></td>
									</c:when>
								</c:choose>
								<c:choose>
									<c:when test="${skillE.ef_para_4 > 0}">
										<td>${skillE.ef_para_4}</td>
									</c:when>
									<c:when test="${skillE.ef_para_4 == 0}">
										<td></td>
									</c:when>
								</c:choose>
								<c:choose>
									<c:when test="${skillE.ef_para_5 > 0}">
										<td>${skillE.ef_para_5}</td>
									</c:when>
									<c:when test="${skillE.ef_para_5 == 0}">
										<td></td>
									</c:when>
								</c:choose>
								<c:choose>
									<c:when test="${skillE.ef_para_6 > 0}">
										<td>${skillE.ef_para_6}</td>
									</c:when>
									<c:when test="${skillE.ef_para_6 == 0}">
										<td></td>
									</c:when>
								</c:choose>
								<c:choose>
									<c:when test="${skillE.ef_para_7 > 0}">
										<td>${skillE.ef_para_7}</td>
									</c:when>
									<c:when test="${skillE.ef_para_7 == 0}">
										<td></td>
									</c:when>
								</c:choose>
								<c:choose>
									<c:when test="${skillE.ef_para_8 > 0}">
										<td>${skillE.ef_para_8}</td>
									</c:when>
									<c:when test="${skillE.ef_para_8 == 0}">
										<td></td>
									</c:when>
								</c:choose>
								<c:choose>
									<c:when test="${skillE.ef_para_9 > 0}">
										<td>${skillE.ef_para_9}</td>
									</c:when>
									<c:when test="${skillE.ef_para_9 == 0}">
										<td></td>
									</c:when>
								</c:choose>
								<c:choose>
									<c:when test="${skillE.ef_para_10 > 0}">
										<td>${skillE.ef_para_10}</td>
									</c:when>
									<c:when test="${skillE.ef_para_10 == 0}">
										<td></td>
									</c:when>
								</c:choose>
							</tr>
							</c:forEach>
						</c:forEach>
					</tbody>
		    	</table>
		    </font>
		</div>
	</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
		<script type="text/javascript" src="js/FGO_DB.js"></script>
		<link rel="stylesheet" href="css/FGO_DB.css">
		<meta charset="UTF-8">
		<title>あたま</title>
	</head>
	<body>
		<header class="header">
			<div class="container">
				<div class="row bg-dark">
					<div class="col-2 text-center">
						<button class="btn btn-info btn-sm" onclick="location.href='Index_Servlet'">ホーム</button>
					</div>
					<div class="col-9">
						<font color="white">
							〔鯖　<a href="ServantL_Servlet"><font color="#f2f2b0">検索</font></a>
							｜<a href=""><font color="#f2f2b0"><s>画像一覧</s></font></a>〕
							〔スキル　<a href="SkillL_Servlet"><font color="#f2f2b0">検索</font></a>〕
							〔計算　<a href="Calculation_Servlet"><font color="#f2f2b0">宝具</font></a>〕
						</font>
					</div>
				</div>
			</div>
		</header>
		<!-- 空の境界 -->
		<div class="header-padding"></div>
	</body>
</html>
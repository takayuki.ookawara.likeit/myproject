<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
		<script type="text/javascript" src="js/jquery.tablesorter.js"></script>
		<script type="text/javascript" src="js/FGO_DB.js"></script>
		<link rel="stylesheet" href="css/lightbox.css">
		<script type="text/javascript" src="js/lightbox.js"></script>
		<link rel="stylesheet" href="css/FGO_DB.css">
		<meta charset="UTF-8">
		<title>礼装 - ぐだぐだ看板娘</title>
	</head>
	<body>
		<jsp:include page="header.jsp" />
		<!-- でかでかとタイトル -->
		<div class="container content">
			<div class="row">
				<div class="col-2">

				</div>
				<div class="col-8 text-center">
					<h1>ぐだぐだ看板娘</h1>
				</div>
				<div class="col-2 text-right">

				</div>
			</div>
			<!-- 以下内容 -->
			<br>
			<div class="row">
				<div class="col text-center">
					<a rel="lightbox" href="picture/CraftEssences/ぐだぐだ看板娘.png">
						<img width="180" height="325" src="picture/CraftEssences/ぐだぐだ看板娘.png">
					</a>
				</div>
				<div class="col-8">
					<table class="table table-bordered">
						<tbody>
							<tr class="text-white font-weight-bold bg-dark">
								<td width="40px">ID</td>
								<td width="40px">Rare</td>
								<td width="40px">Cost</td>
								<td>イラストレーター</td>
								<td>入手方法</td>
							</tr>
							<tr>
								<td>062</td>
								<td>5</td>
								<td>12</td>
								<td>ヤグチミナト</td>
								<td>聖晶石召喚（期間限定）</td>
							</tr>
						</tbody>
					</table>
					<table class="table  table-bordered">
						<tbody>
							<tr class="text-white font-weight-bold bg-dark">
								<td></td>
								<td>Lv.1</td>
								<td>Lv.100</td>
							</tr>
							<tr>
								<td class="text-white font-weight-bold bg-dark">HP</td>
								<td>750</td>
								<td>3000</td>
							</tr>
							<tr>
								<td class="text-white font-weight-bold bg-dark">ATK</td>
								<td>0</td>
								<td>0</td>
							</tr>
							<tr class="text-white font-weight-bold bg-dark" height="1">
								<td>効果</td>
								<td>倍率（未凸）</td>
								<td>倍率（完凸）</td>
							</tr>
							<tr>
								<td>自身にターゲット集中状態（3ターン）</td>
								<td class="text-center" colspan="2">-</td>
							</tr>
							<tr>
								<td>＆ 攻撃力をアップ（3ターン）</td>
								<td>60%</td>
								<td>80%</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<br>
		</div>
		<br>
		<br>
		<br>
	</body>
</html>
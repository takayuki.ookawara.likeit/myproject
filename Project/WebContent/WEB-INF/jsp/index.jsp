<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
		<script type="text/javascript" src="js/jquery.tablesorter.js"></script>
		<script type="text/javascript" src="js/FGO_DB.js"></script>
		<link rel="stylesheet" href="css/FGO_DB.css">
		<meta charset="UTF-8">
		<title>FGO DataBase</title>
	</head>
	<body>
	<!-- 見た目作るの苦手だから大目に見て -->
		<jsp:include page="header.jsp" />
		<!-- でかでかとタイトル -->
		<div class="container content">
			<div class="row">
				<div class="col">

				</div>
				<div class="col-8 text-center">
					<h1>FGO DataBase</h1>
				</div>
				<div class="col">

				</div>
			</div><div class="row">
				<div class="col">

				</div>
				<div class="col-8 text-center">
					<h8>
						当サイトは「<a href="https://www.fate-go.jp/">Fate/Grand Order</a>」の非公式サイトです。<br>
					</h8>
				</div>
				<div class="col">

				</div>
			</div>
			<br>
			<!-- ここからアレ -->
			<div class="row">
				<div class="col-1">

				</div>
				<!-- ここから左側 -->
				<div class="col">
					<div class="text-center">
						<table class="table table-sm">
							<thead>
								<tr class="table-success">
									<th scoop="col">サーヴァント</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th scoop="col"><a href="ServantL_Servlet">検索・リスト</a></th>
								</tr>
								<tr>
									<th scoop="col"><a href=""><s>一覧表示</s></a></th>
								</tr>
								<tr>
									<th scoop="col"></th>
								</tr>
							</tbody>
						</table>
						<table class="table table-sm">
							<thead>
								<tr class="table-success">
									<th scoop="col">スキル</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th scoop="col"><a href="SkillL_Servlet">検索・リスト</a></th>
								</tr>
								<tr>
									<th scoop="col"></th>
								</tr>
							</tbody>
						</table>
						<table class="table table-sm">
							<thead>
								<tr class="table-info">
									<th scoop="col">礼装</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th scoop="col"><a href=""><s>検索・リスト</s></a></th>
								</tr>
								<tr>
									<th scoop="col"></th>
								</tr>
							</tbody>
						</table>
						<table class="table table-sm">
							<thead>
								<tr class="table-info">
									<th scoop="col">マスター礼装</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th scoop="col"><a href=""><s>検索・リスト</s></a></th>
								</tr>
								<tr>
									<th scoop="col"></th>
								</tr>
							</tbody>
						</table>
						<table class="table table-sm">
							<thead>
								<tr class="table-info">
									<th scoop="col">コマンドコード</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th scoop="col"><a href=""><s>検索・リスト</s></a></th>
								</tr>
								<tr>
									<th scoop="col"></th>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<!-- ここからまんなか -->
				<div class="col">
					<div class="text-center">
						<table class="table table-sm">
							<thead>
								<tr class="table-primary">
									<th scoop="col">計算系はこちら</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th scoop="col"><a href="Calculation_Servlet">宝具ダメージ計算</a></th>
								</tr>
								<tr>
									<th scoop="col"></th>
								</tr>
							</tbody>
						</table>
						<table class="table table-sm">
							<thead>
								<tr class="table-warning">
									<th scoop="col">掲示板</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th scoop="col"><a href=""><s>総合</s></a></th>
								</tr>
								<tr>
									<th scoop="col"><a href=""><s>フレンド募集</s></a></th>
								</tr>
								<tr>
									<th scoop="col"></th>
								</tr>
							</tbody>
						</table>
						<table class="table table-sm">
							<thead>
								<tr class="table-danger">
									<th scoop="col">連絡などはこちら</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th scoop="col"><a href="about_Servlet">サイトについて</a></th>
								</tr>
								<tr>
									<th scoop="col"></th>
								</tr>
							</tbody>
						</table>
						<c:forEach var="s" items="${randam}" varStatus="ct">
							<c:if test="${ct.count <= 1}">
								<a href="ServantP_Servlet?id=${s.id}" target="_blank">
									<font color="black">ランダムアクセス！</font>
								</a>
							</c:if>
						</c:forEach>
						<br>
						<font size="2">更新するとリンク先が変わります。</font>
					</div>
				</div>
				<!-- ここから右側 -->
				<div class="col">
					<div class="text-center">
						<!-- TwitterのTL埋め込み -->
						<a class="twitter-timeline"  height="500px"
						href="https://twitter.com/fgoproject?ref_src=twsrc%5Etfw">
						Tweets by fgoproject</a>
						<script async src="https://platform.twitter.com/widgets.js" charset="utf-8">
						</script>
						<!-- TwitterのTL埋め込み -->
					</div>
				</div>
				<div class="col-1">

				</div>
			</div>
		</div>
	</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
		<script type="text/javascript" src="js/jquery.tablesorter.js"></script>
		<script type="text/javascript" src="js/FGO_DB.js"></script>
		<link rel="stylesheet" href="css/FGO_DB.css">
		<meta charset="UTF-8">
		<title>新規登録</title>
	</head>
	<body>
		<!-- ログイン中は名前とログアウトを表示 ログアウト状態ではログインに表示替え -->
		<header class="header">
			<div class="container">
				<div class="row bg-dark">
					<div class="col-3 text-center">
						<button class="btn btn-info btn-sm" onclick="location.href='Index_Servlet'">ホーム</button>
					</div>
					<div class="col-6 text-light text-right">
						<!-- ユーザー名 -->さん
					</div>
					<div class="col-3 text-center">
						<button type="button" class="btn btn-info btn-sm">ログアウト</button>
					</div>
				</div>
			</div>
		</header>
		<!-- 空の境界 -->
		<div class="header-padding"></div>
		<!-- でかでかとタイトル -->
		<div class="container content">
			<div class="row">
				<div class="col-2">

				</div>
				<div class="col-8 text-center">
					<h1>新規登録</h1>
				</div>
				<div class="col-2 text-right">

				</div>
			</div>
			<br>
			<!-- ここにフォームとか -->
			<form action="">
				<div class="row">
					<div class="col text-center">
						　ログインID　　　　　　　<input class="form-control form-control-sm" type="text" placeholder="未入力です" style="width:250px; display:inline" name="" required><br>
						　名前（ハンドルネーム）　<input class="form-control form-control-sm" type="text" placeholder="未入力です" style="width:250px; display:inline" name="" required><br>
						　パスワード　　　　　　　<input class="form-control form-control-sm" type="text" placeholder="未入力です" style="width:250px; display:inline" name="" required><br>
						　パスワード（確認）　　　<input class="form-control form-control-sm" type="text" placeholder="未入力です" style="width:250px; display:inline" name="" required><br>
						<br>
						<br>
						<button type="button" class="btn btn-info btn-sm">登　録</button>
					</div>
				</div>
			</form>
		</div>
	</body>
</html>
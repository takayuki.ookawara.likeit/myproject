<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
		<script type="text/javascript" src="js/jquery.tablesorter.js"></script>
		<script type="text/javascript" src="js/FGO_DB.js"></script>
		<link rel="stylesheet" href="css/lightbox.css">
		<script type="text/javascript" src="js/lightbox.js"></script>
		<link rel="stylesheet" href="css/FGO_DB.css">
		<meta charset="UTF-8">
		<title>鯖 - ${servant.name}〔${servant.s_class.name}〕:${servant.id}</title>
	</head>
	<body>
		<jsp:include page="header.jsp" />
		<!-- でかでかとタイトル -->
		<font size="2">
			<div class="container content">
				<div class="row">
					<div class="col-2">
					</div>
					<div class="col-8 text-center">
						<h1>${servant.name}〔${servant.s_class.name}〕</h1>
					</div>
					<div class="col-2 text-right">

					</div>
				</div>
				<!-- 以下内容 -->
				<br>
				<div class="row">
					<div class="col text-center">
						<a rel="lightbox" href="img/Servant/${servant.id}_1.png">
							<img width="160" height="226" src="img/Servant/${servant.id}_1.png" alt="no data">
						</a>
						<a rel="lightbox" href="img/Servant/${servant.id}_2.png">
							<img width="160" height="226" src="img/Servant/${servant.id}_2.png" alt="no data">
						</a>
						<a rel="lightbox" href="img/Servant/${servant.id}_3.png">
							<img width="160" height="226" src="img/Servant/${servant.id}_3.png" alt="no data">
						</a>
						<a rel="lightbox" href="img/Servant/${servant.id}_4.png">
							<img width="160" height="226" src="img/Servant/${servant.id}_4.png" alt="no data">
						</a>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col">
						<table class="table table-bordered table-sm">
							<thead class="thead-dark">
								<tr>
									<th scope="col">ID</th>
									<th scope="col">Class</th>
									<th scope="col">Rare</th>
									<th scope="col">Cost</th>
									<th scope="col">HP</th>
									<th scope="col">ATK</th>
									<th scope="col">天地人</th>
									<th scope="col">B</th>
									<th scope="col">A</th>
									<th scope="col">Q</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>${servant.id}</td>
									<td>${servant.s_class.name}</td>
									<td>${servant.s_rare.rare}</td>
									<td>${servant.s_rare.cost}</td>
									<td>${servant.hp}</td>
									<td>${servant.atk}</td>
									<td>${servant.s_tenchijin.name}</td>
									<td>${servant.b}</td>
									<td>${servant.a}</td>
									<td>${servant.q}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<!-- ここ折り畳むマン -->
				<details>
					<summary>
						隠しステータス
					</summary>
					<br>
					<!-- ここから折り畳みの内容 -->
					<div class="row">
						<div class="col">
						<table class="table table-bordered table-sm">
							<tbody>
								<tr class="text-white font-weight-bold bg-dark">
									<td>方針</td>
									<td>性格</td>
									<td>性別</td>
									<td>スター発生率</td>
									<td>スター集中度</td>
									<td>N/A</td>
									<td>N/D</td>
									<td colspan="5">ヒット数</td>
								</tr>
								<tr>
									<td>${servant.s_houshin.name}</td>
									<td>${servant.s_seikaku.name}</td>
									<td>${servant.s_sex.name}</td>
									<td>${servant.star}</td>
									<td>${servant.star_atsumare}</td>
									<td>${servant.np_eff}</td>
									<td>${servant.np_damage}</td>
									<td width="65px" class="text-center text-white bg-danger">B</td>
									<td width="65px" class="text-center text-white bg-primary">A</td>
									<td width="65px" class="text-center text-white bg-success">Q</td>
									<td width="65px" class="text-center text-white bg-info">EX</td>
									<td width="65px" class="text-center text-white bg-warning">宝具</td>
								</tr>
								<tr>
									<td class="text-white font-weight-bold bg-dark" colspan="7">特性</td>
									<td class="text-center">${servant.b_hit}</td>
									<td class="text-center">${servant.a_hit}</td>
									<td class="text-center">${servant.q_hit}</td>
									<td class="text-center">${servant.ex_hit}</td>
									<td class="text-center">${servant.n_hit}</td>
								</tr>
								<tr>
									<td colspan="12">
										<c:forEach var="s" items="${servant.relation_state}">
											${s.s_state.state}
										</c:forEach>
									</td>
								</tr>
							</tbody>
						</table>
						</div>
					</div>
				</details>
				<!-- ここで折り畳み終わり -->
				<hr noshade>
				<div class="row">
					<div class="col">
					<font size="5"><strong>宝具</strong></font><br>
					<br>
					</div>
				</div>
				<!-- 以下宝具 -->
				<c:forEach var="np" items="${servant.relation_np}">
					<c:if test="${fn:length(servant.relation_np) > 1}">
						<c:choose>
							<c:when test="${np.junban == '1'}">
								<!-- 強化前 -->
								<details>
									<summary>
										強化前
									</summary>
									<br>
							</c:when>
							<c:when test="${np.junban >= '3'}">
								<hr width="400" size="10" noshade>
								<!-- 特殊 -->
								<details>
									<summary>
										特殊${np.junban -2}
									</summary>
									<br>
							</c:when>
						</c:choose>
					</c:if>
					<div class="row">
						<div class="col">
							<div class="text-center">
								<font size="4">${np.noblePhantasm.ruby}</font><br>
								<font size="6" class="font-weight-bold">${np.noblePhantasm.name}</font>
							</div>
							<table class="table table-bordered table-sm">
								<tbody>
									<tr class="text-white font-weight-bold bg-dark">
										<td>Card</td>
										<td>ランク</td>
										<td>種別</td>
										<td>効果</td>
										<td>1</td>
										<td>2</td>
										<td>3</td>
										<td>4</td>
										<td>5</td>
									</tr>
									<tr>
										<td rowspan="${fn:length(np.noblePhantasm.NP_Effect)+2}">${servant.s_Color.color}</td>
										<td rowspan="${fn:length(np.noblePhantasm.NP_Effect)+2}">${np.noblePhantasm.rank}</td>
										<td rowspan="${fn:length(np.noblePhantasm.NP_Effect)+2}">${np.noblePhantasm.type}</td>
										<c:forEach var="s" items="${np.noblePhantasm.NP_Effect}">
											<c:if test="${s.fast > 0}">
													<td>${s.ef_text}</td>
													<c:choose>
														<c:when test="${s.ef_para_1 == s.ef_para_5}">
															<td colspan="5" class="text-right">${s.ef_para_5}</td>
														</c:when>
														<c:when test="${!(s.ef_para_1 == s.ef_para_5)}">
															<td>${s.ef_para_1}</td>
															<td>${s.ef_para_2}</td>
															<td>${s.ef_para_3}</td>
															<td>${s.ef_para_4}</td>
															<td>${s.ef_para_5}</td>
														</c:when>
													</c:choose>
											</c:if>
										</c:forEach>
									</tr>
									<tr>
										<c:choose>
											<c:when test="${np.noblePhantasm.NP_bairitsu.np_bairitsu_1 > 0}">
												<td>${np.noblePhantasm.damage_text}</td>
												<td>${np.noblePhantasm.NP_bairitsu.np_bairitsu_1}</td>
												<td>${np.noblePhantasm.NP_bairitsu.np_bairitsu_2}</td>
												<td>${np.noblePhantasm.NP_bairitsu.np_bairitsu_3}</td>
												<td>${np.noblePhantasm.NP_bairitsu.np_bairitsu_4}</td>
												<td>${np.noblePhantasm.NP_bairitsu.np_bairitsu_5}</td>
											</c:when>
											<c:when test="${np.noblePhantasm.NP_bairitsu.np_bairitsu_1 == 0}">
											</c:when>
										</c:choose>
									</tr>
									<c:forEach var="s" items="${np.noblePhantasm.NP_Effect}">
										<c:if test="${s.fast == 0}">
											<tr>
												<td>${s.ef_text}</td>
												<c:choose>
													<c:when test="${s.ef_para_1 == s.ef_para_5}">
														<td colspan="5" class="text-right">${s.ef_para_5}</td>
													</c:when>
													<c:when test="${!(s.ef_para_1 == s.ef_para_5)}">
														<td>${s.ef_para_1}</td>
														<td>${s.ef_para_2}</td>
														<td>${s.ef_para_3}</td>
														<td>${s.ef_para_4}</td>
														<td>${s.ef_para_5}</td>
													</c:when>
												</c:choose>
											</tr>
										</c:if>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					<c:if test="${fn:length(servant.relation_np) > 1}">
						<c:choose>
							<c:when test="${np.junban == '1'}">
								</details>
								<hr width="400" size="10" noshade>
							</c:when>
							<c:when test="${np.junban >= '3'}">
								</details>
							</c:when>
						</c:choose>
					</c:if>
				</c:forEach>
				<hr noshade>
				<font size="5"><strong>スキル</strong></font><br>
				<br>
				<div class="row">
					<div class="col">
						<table class="table table-bordered table-sm">
							<thead class="thead-dark">
								<tr>
									<th>所有スキル名</th>
									<th>CT</th>
									<th>効果</th>
									<th>継続</th>
									<th>対象</th>
									<th>SL1</th>
									<th>SL2</th>
									<th>SL3</th>
									<th>SL4</th>
									<th>SL5</th>
									<th>SL6</th>
									<th>SL7</th>
									<th>SL8</th>
									<th>SL9</th>
									<th>SL10</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="SS" items="${servant.relation_skill}">
									<tr>
										<th rowspan="${fn:length(SS.skill.skill_Effect)+1}" scope="row">
											${SS.skill.name}
											<c:if test="${SS.junban == '2'||SS.junban == '4'||SS.junban == '6'}">
												〔強化後〕
											</c:if>
										</th>
										<td rowspan="${fn:length(SS.skill.skill_Effect)+1}">${SS.skill.ct}</td>
									</tr>
									<c:forEach var="ef" items="${SS.skill.skill_Effect}">
										<tr>
											<td>${ef.ef_text}</td>
											<c:choose>
												<c:when test="${ef.turn == 0}">
													<td></td>
												</c:when>
												<c:when test="${ef.turn > 0}">
													<td>${ef.turn}</td>
												</c:when>
											</c:choose>
											<c:choose>
												<c:when test="${empty ef.target.desc}">
													<td>${ef.target.tage_name}</td>
												</c:when>
												<c:when test="${not empty ef.target.desc}">
													<td>${ef.target.tage_name}〔${ef.target.desc}〕</td>
												</c:when>
											</c:choose>
											<c:choose>
												<c:when test="${ef.ef_para_10 == 0}">
													<td colspan="10"></td>
												</c:when>
												<c:when test="${ef.ef_para_10 == ef.ef_para_1}">
													<td colspan="10" class="text-right">${ef.ef_para_10}</td>
												</c:when>
												<c:when test="${ef.ef_para_10 > 0}">
													<td>${ef.ef_para_1}</td>
													<td>${ef.ef_para_2}</td>
													<td>${ef.ef_para_3}</td>
													<td>${ef.ef_para_4}</td>
													<td>${ef.ef_para_5}</td>
													<td>${ef.ef_para_6}</td>
													<td>${ef.ef_para_7}</td>
													<td>${ef.ef_para_8}</td>
													<td>${ef.ef_para_9}</td>
													<td>${ef.ef_para_10}</td>
												</c:when>
											</c:choose>
										</tr>
									</c:forEach>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				<c:if test="${(fn:length(servant.relation_skill_other) > 0)}">
					<!-- スキルを7つ以上持っている場合 -->
					<details>
						<summary>
							特殊
						</summary>
						<br>
						<table class="table table-bordered table-sm">
							<thead class="thead-dark">
								<tr>
									<th>所有スキル名</th>
									<th>CT</th>
									<th>効果</th>
									<th>継続</th>
									<th>対象</th>
									<th>SL1</th>
									<th>SL2</th>
									<th>SL3</th>
									<th>SL4</th>
									<th>SL5</th>
									<th>SL6</th>
									<th>SL7</th>
									<th>SL8</th>
									<th>SL9</th>
									<th>SL10</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="SS" items="${servant.relation_skill_other}">
									<tr>
										<th rowspan="${fn:length(SS.skill.skill_Effect)+1}" scope="row">
											${SS.skill.name}
											<c:if test="${SS.junban == '8'||SS.junban == '10'||SS.junban == '12'}">
												〔強化後〕
											</c:if>
										</th>
										<td rowspan="${fn:length(SS.skill.skill_Effect)+1}">${SS.skill.ct}</td>
									</tr>
									<c:forEach var="ef" items="${SS.skill.skill_Effect}">
										<tr>
											<td>${ef.ef_text}</td>
											<c:choose>
												<c:when test="${ef.turn == 0}">
													<td></td>
												</c:when>
												<c:when test="${ef.turn > 0}">
													<td>${ef.turn}</td>
												</c:when>
											</c:choose>
											<c:choose>
												<c:when test="${empty ef.target.desc}">
													<td>${ef.target.tage_name}</td>
												</c:when>
												<c:when test="${not empty ef.target.desc}">
													<td>${ef.target.tage_name}〔${ef.target.desc}〕</td>
												</c:when>
											</c:choose>
											<c:choose>
												<c:when test="${ef.ef_para_10 == 0}">
													<td colspan="10"></td>
												</c:when>
												<c:when test="${ef.ef_para_10 == ef.ef_para_1}">
													<td colspan="10" class="text-right">${ef.ef_para_10}</td>
												</c:when>
												<c:when test="${ef.ef_para_10 > 0}">
													<td>${ef.ef_para_1}</td>
													<td>${ef.ef_para_2}</td>
													<td>${ef.ef_para_3}</td>
													<td>${ef.ef_para_4}</td>
													<td>${ef.ef_para_5}</td>
													<td>${ef.ef_para_6}</td>
													<td>${ef.ef_para_7}</td>
													<td>${ef.ef_para_8}</td>
													<td>${ef.ef_para_9}</td>
													<td>${ef.ef_para_10}</td>
												</c:when>
											</c:choose>
										</tr>
									</c:forEach>
								</c:forEach>
							</tbody>
						</table>
					</details>
				</c:if>
				<hr noshade>
				<div class="row">
					<div class="col-5">
						<font size="5"><strong>クラススキル</strong></font><br>
						<br>
						<table class="table table-bordered table-sm">
							<c:forEach var="cs" items="${servant.relation_c_skill}">
								<tbody>
									<tr class="text-white font-weight-bold bg-dark">
										<td rowspan="${fn:length(cs.class_Skill.CS_Effect)+1}" width="64px" ><img src="img/Icon/${cs.class_Skill.img_id}.png"></td>
										<td colspan="2">${cs.class_Skill.name}</td>
									</tr>

									<c:forEach var="cse" items="${cs.class_Skill.CS_Effect}">
										<tr>
											<td>${cse.ef_text}</td>
											<td>${cse.ef_para}</td>
										</tr>
									</c:forEach>

								</tbody>
							</c:forEach>
						</table>
					</div>
					<div class="col">
						<font size="5"><strong>追加情報</strong></font><br>
						<br>
						<table class="table table-bordered table-sm">
							<tbody>
								<tr class="text-white font-weight-bold bg-dark">
									<td>声優</td>
									<td>イラストレーター</td>
									<td>入手方法</td>
								</tr>
								<tr>
								 <td>${servant.cv}</td>
								 <td>${servant.illustrator}</td>
								 <td>${servant.s_rare.gacha}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</font>
		<br>
		<br>
		<br>
	</body>
</html>
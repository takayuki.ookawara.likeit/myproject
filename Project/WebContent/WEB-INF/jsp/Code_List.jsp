<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
		<script type="text/javascript" src="js/jquery.tablesorter.js"></script>
		<script type="text/javascript" src="js/FGO_DB.js"></script>
		<link rel="stylesheet" href="css/FGO_DB.css">
		<meta charset="UTF-8">
		<title>コマンドコードリスト</title>
	</head>
	<body>
		<jsp:include page="header.jsp" />
		<!-- でかでかとタイトル -->
		<div class="container content">
			<div class="row">
				<div class="col-2">

				</div>
				<div class="col-8 text-center">
					<h1>コマンドコード検索</h1>
				</div>
				<div class="col-2 text-right">

				</div>
			</div>
			<br>
			<!-- ここにフォームとか -->
			<div class="row text-center">
				<div class="col">
					コード名<br>
				</div>
				<div class="col">
					<input class="form-control form-control-sm" type="text" placeholder="コード名"><br>
				</div>
				<div class="col">
					レア<br>
				</div>
				<div class="col">
					<select class="form-control form-control-sm">
						<option>-</option>
						<option>1</option>
						<option>2</option>
						<option>3</option>
						<option>4</option>
						<option>5</option>
					</select>
					<br>
				</div>
				<div class="col">
					ID<br>
				</div>
				<div class="col">
					<input class="form-control form-control-sm" type="text" placeholder="ID"><br>
				</div>
			</div>
			<!-- ここ折り畳むマン -->
			<details>
				<summary>
					んまこし
				</summary>
				<br>
				一応残してるだけで設置予定なし
			</details>
			<br>
			<button type="submit" class="btn btn-secondary btn-lg btn-block">検索</button>
			<br>
			<!-- ここから一覧 -->
			<table id="Sort_Table" class="table table-bordered">
				<thead class="thead-dark">
					<tr>
						<th>ID</th>
						<th>Rare</th>
						<th>コード名</th>
						<th>効果</th>
						<th>倍率</th>
						<th>備考</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th scope="row">11</hd>
						<td>3</td>
						<td>ラッキービースト</td>
						<td>HP回復</td>
						<td>200</td>
						<td>しろい</td>
					</tr>
					<tr>
						<th scope="row">32</th>
						<td>4</td>
						<td>BBスロット</td>
						<td>星獲得/HP回復/弱体耐性</td>
						<td>5個/500/10%</td>
						<td>3つの中からランダム発動</td>
					<tr>
						<th scope="row">36</th>
						<td>5</td>
						<td>慈眼温容の尼僧</td>
						<td>〔秩序〕特攻/〔ルーラー〕特攻</td>
						<td>20%/20%</td>
						<td>行きつく先は殺生院</td>
					</tr>
				</tbody>
	    	</table>
		</div>
	</body>
</html>
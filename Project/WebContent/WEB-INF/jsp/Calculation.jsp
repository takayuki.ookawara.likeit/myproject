<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
		<script type="text/javascript" src="js/jquery.tablesorter.js"></script>
		<script type="text/javascript" src="js/FGO_DB.js"></script>
		<link rel="stylesheet" href="css/FGO_DB.css">
		<meta charset="UTF-8">
		<title>宝具ダメージ計算</title>
	</head>
	<body>
		<jsp:include page="header.jsp" />
		<!-- でかでかとタイトル -->
		<div class="container content">
			<div class="row">
				<div class="col-2">

				</div>
				<div class="col-8 text-center">
					<h1>宝具ダメージ計算</h1>
				</div>
				<div class="col-2 text-right">

				</div>
			</div>
			<br>
			<!-- ここから検索 -->
			<form action="Calculation_Servlet" method="get" id="list">
				<div class="row">
					<div class="col-1">
						名前
					</div>
					<div class="col-2">
						<input class="form-control form-control-sm" type="text" name ="servant_name" placeholder="おなまえ" value="${serHis }"><br>
					</div>
					<div class="col-1">
						性別
					</div>
					<div class="col-1">
						<select name="sex" class="form-control form-control-sm">
							<option value="">-</option>
							<c:forEach var="sex" items="${sexList}">
								<c:choose>
									<c:when test="${sexHis == sex.id}">
										<option value="${sex.id}" selected>${sex.name}</option>
									</c:when>
									<c:otherwise>
										<option value="${sex.id}">${sex.name}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
					</div>
					<div class="col-1">
						レア
					</div>
					<div class="col-1">
						<select name="rare" class="form-control form-control-sm">
							<option value="">-</option>
							<c:forEach var="s_rare" items="${rareList}">
									<c:choose>
										<c:when test="${rarHis == s_rare.rare&&!(rarHis == 0)}">
											<option value="${s_rare.rare}" selected>${s_rare.rare}</option>
										</c:when>
										<c:otherwise>
											<option value="${s_rare.rare}">${s_rare.rare}</option>
										</c:otherwise>
									</c:choose>
							</c:forEach>
						</select>
					</div>
					<div class="col-1">
						クラス
					</div>
					<div class="col-2">
						<select name="s_class" class="form-control form-control-sm">
							<option value="">-</option>
								<c:forEach var="s_class" items="${classList}">
									<c:choose>
										<c:when test="${claHis == s_class.id}">
											<option value="${s_class.id}" selected>${s_class.name}</option>
										</c:when>
										<c:otherwise>
											<option value="${s_class.id}">${s_class.name}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
						</select>
					</div>
					<div class="col">
						<button type="submit" class="btn btn-primary btn-sm">検索</button>
						<button class="btn btn-danger btn-sm" onclick="location.href='Calculation_Servlet'">条件リセット</button>
					</div>
				</div>
				<div class="row">
					<div class="col-2 text-center">
						検索結果
					</div>
					<div class="col-4">
						<select name="addServant" class="form-control form-control-sm">
							<c:forEach var="servant" items="${servantList}" >
								<option value="${servant.id}">${servant.name}〔${servant.s_class.name} ★${servant.s_rare.rare}〕</option>
							</c:forEach>
						</select>
					</div>
					<div class="col-6">
						<button type="submit" class="btn btn-secondary btn-sm" name="AddJudge" value="main">メイン</button>
						　　
						<button type="submit" class="btn btn-info btn-sm" name="AddJudge" value="sup1">サポ1</button>
						<button type="submit" class="btn btn-info btn-sm" name="AddJudge" value="sup2">サポ2</button>
						<button type="submit" class="btn btn-info btn-sm" name="AddJudge" value="sup3">サポ3</button>
						<button type="submit" class="btn btn-info btn-sm" name="AddJudge" value="sup4">サポ4</button>
						<button type="submit" class="btn btn-info btn-sm" name="AddJudge" value="sup5">サポ5</button>
						<button type="submit" class="btn btn-danger btn-sm" name="AddJudge" value="resetALL">オールリセット</button>
					</div>
				</div>
			</form>
			<hr noshade>
			<!-- ここから下は本体 -->
			<form action="Calculation_Servlet" method="post" id="skillcalc">
			<div class="row">
				<!-- 左側は文字を少し小さく -->
				<div class="col">
					<c:choose>
						<c:when test="${alert == 1}">
							<!-- 警告 -->
							<div class="text-center">
								<font color="red" >
									<strong>！！！ 宝具データがないので追加できません ！！！</strong>
								</font>
								<br>
							</div>
						</c:when>
						<c:when test="${alert == 2}">
							<!-- 警告 -->
							<div class="text-center">
								<font color="red" >
									<strong>！！！ 攻撃型宝具ではないので追加できません ！！！</strong>
								</font>
								<br>
							</div>
						</c:when>
						<c:when test="${alert == 3}">
							<!-- 警告 -->
							<div class="text-center">
								<font color="red" >
									<strong>！！！ 強化前と強化後のスキルが同時に選択されています ！！！</strong>
								</font>
								<br>
							</div>
						</c:when>
					</c:choose>
					<font size="2">
						<table class="table table-bordered">
							<tbody>
								<tr class="text-white font-weight-bold bg-dark">
									<td>Class</td>
									<td colspan="2">名前</td>
									<td>ATK</td>
									<td>天地人</td>
									<td>性別</td>
									<td>Rare</td>
								</tr>
								<tr>
									<td>${addServant.s_class.name}</td>
									<td colspan="2">
										<a href="ServantP_Servlet?id=${addServant.id}" target="_blank">
											<font color="#4682b4"><strong>${addServant.name}</strong></font>
										</a>
									</td>
									<td>${addServant.atk}</td>
									<td>${addServant.s_tenchijin.name}</td>
									<td>${addServant.s_sex.name}</td>
									<td>${addServant.s_rare.rare}</td>
								</tr>
								<tr>
									<td class="text-white font-weight-bold bg-dark">宝具レベル</td>
									<td colspan="2">
										<c:forEach var="i" begin="1" end="5" step="1">
											<c:choose>
												<c:when test="${i == 1&&!(npHis == i)}">
													<input type="radio" name="NP" value="${i}" checked="checked">${i}　
												</c:when>
												<c:when test="${npHis == i}">
													<input type="radio" name="NP" value="${i}" checked="checked">${i}　
												</c:when>
												<c:otherwise>
													<input type="radio" name="NP" value="${i}">${i}　
												</c:otherwise>
											</c:choose>
										</c:forEach>
									<td class="text-white font-weight-bold bg-dark">オーバーチャージ</td>
									<td colspan="2">
										<c:forEach var="i" begin="1" end="5" step="1">
											<c:choose>
												<c:when test="${i == 1&&!(ocHis == i)}">
													<input type="radio" name="OC" value="${i}" checked="checked">${i}　
												</c:when>
												<c:when test="${ocHis == i}">
													<input type="radio" name="OC" value="${i}" checked="checked">${i}　
												</c:when>
												<c:otherwise>
													<input type="radio" name="OC" value="${i}">${i}　
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</td>
								</tr>
							</tbody>
						</table>
						ATK <input class="form-control form-control-sm" type="text" placeholder="none" style="width:70px; display:inline" name="atk" value="${addServant.atk}">
						　ATKフォウ <input class="form-control form-control-sm" type="text" placeholder="none" style="width:70px; display:inline" name="fou" value="0">
						　クラス
						<select class="form-control form-control-sm" style="width:100px; display:inline" name="class_core">
							<c:forEach var="s_class" items="${classList}">
								<c:choose>
									<c:when test="${addServant.s_class.name == s_class.name}">
										<option value="${s_class.in_value}" selected>${s_class.name}</option>
									</c:when>
									<c:otherwise>
										<option value="${s_class.in_value}">${s_class.name}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
						　宝具倍率 <input class="form-control form-control-sm" type="text" placeholder="none" style="width:70px; display:inline" name="np" value="${np_bairitsu}">％
						　宝具色
						<select class="form-control form-control-sm" style="width:70px; display:inline" name="color">
							<c:forEach var="color" items="${colorList}">
								<c:choose>
									<c:when test="${addServant.s_Color.color == color.color}">
										<option value="${color.in_value}" selected>${color.color}</option>
									</c:when>
									<c:otherwise>
										<option value="${color.in_value}">${color.color}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
						<br>
						<c:if test="${fn:length(addServant.relation_skill) > 3}">
							強化前
						</c:if>
						<c:forEach var="rs" items="${addServant.relation_skill}">
							<c:if test="${rs.junban == '1'||rs.junban == '3'||rs.junban == '5'}">
								スキル：${rs.skill.name } <input type="checkbox" name="skill" value="${rs.junban}">適用　
							</c:if>
						</c:forEach>
						<br>
						<c:if test="${fn:length(addServant.relation_skill) > 3}">
							強化後
							<c:forEach var="rs" items="${addServant.relation_skill}">
								<c:if test="${rs.junban == '2'||rs.junban == '4'||rs.junban == '6'}">
									スキル：${rs.skill.name } <input type="checkbox" name="skill" value="${rs.junban}">適用　
								</c:if>
							</c:forEach>
							<br>
						</c:if>
						<c:if test="${fn:length(addServant.relation_skill_other) > 0}">
							特殊1
							<c:forEach var="rs" items="${addServant.relation_skill_other}">
								<c:if test="${rs.junban == '7'||rs.junban == '9'||rs.junban == '11'}">
									スキル：${rs.skill.name } <input type="checkbox" name="skill" value="${rs.junban}">適用　
								</c:if>
							</c:forEach>
						</c:if>
						<hr noshade>
						<h4 class="font-weight-bold">バフ</h4>
						ATK <input class="form-control form-control-sm" type="text" placeholder="none" style="width:70px; display:inline" name="atk_buff" value="${ATKbuff +0}">％
						　カード <input class="form-control form-control-sm" type="text" placeholder="none" style="width:70px; display:inline" name="card_buff" value="${Cradbuff +0}">％
						　宝具威力 <input class="form-control form-control-sm" type="text" placeholder="none" style="width:70px; display:inline" name="np_buff" value="${NPbuff +0}">％
						　特攻付与 <input class="form-control form-control-sm" type="text" placeholder="none" style="width:70px; display:inline" name="sp_skill" value="${SAbuff +0}">％
						<br>
						〔特性〕特攻（宝具） <input class="form-control form-control-sm" type="text" placeholder="none" style="width:70px; display:inline" name="sp_chara" value="${NPCbuff +100}">％
						　〔状態〕特攻（宝具） <input class="form-control form-control-sm" type="text" placeholder="none" style="width:70px; display:inline" name="sp_state" value="${NPSbuff +100}">％
						<br>
						ダメージ（クラススキルの追加ダメージもここへ） ＋<input class="form-control form-control-sm" type="text" placeholder="none" style="width:70px; display:inline" name="damage_buff" value="${Bdamage +0}">
						<hr noshade>
						<h4 class="font-weight-bold">デバフ</h4>
						敵防御力<input class="form-control form-control-sm" type="text" placeholder="none" style="width:70px; display:inline" name="def_debuff" value="${DEFdebuff +0}">％
						　カード耐性 <input class="form-control form-control-sm" type="text" placeholder="none" style="width:70px; display:inline" name="card_debuff" value="${Craddebuff +0}">％
						　敵ダメージデバフ ＋<input class="form-control form-control-sm" type="text" placeholder="none" style="width:70px; display:inline" name="damage_debuff" value="${Ddamage +0}">
						<hr noshade>
						<h4 class="font-weight-bold">相性</h4>
						クラス相性
						<select class="form-control form-control-sm" style="width:70px; display:inline" name="class_comp">
							<option value="1.0">等倍</option>
							<option value="2.0">有利</option>
							<option value="0.5">不利</option>
							<option value="1.5">有利（バーサーカー）</option>
							<option value="1.5">有利（アルターエゴ）</option>
						</select>
						　天地人相性
						<select class="form-control form-control-sm" style="width:70px; display:inline" name="tenchijin">
							<option value="1.0">等倍</option>
							<option value="1.1">有利</option>
							<option value="0.9">不利</option>
						</select>
						　<font size="1"><strong>※反映などをするとリセットされるので注意</strong></font>
						<hr noshade>
						平均<input class="form-control form-control-sm" type="text" placeholder="none" style="width:70px; display:inline" name="average" value="0" disabled>
						　最高<input class="form-control form-control-sm" type="text" placeholder="none" style="width:70px; display:inline" name="high" value="0" disabled>
						　最低<input class="form-control form-control-sm" type="text" placeholder="none" style="width:70px; display:inline" name="low" value="0" disabled>
						　<button type="button" id="calc">計算</button>
						　　　　　　　　　
						<button type="submit" class="btn btn-success btn-sm" name="SkillRef" value="go">値の反映</button>
						<br>
						<br>
						<font size="3"><strong>※注意※</strong></font><br>
						<font color="red">
							無記入欄（<font color="gray">none</font>）がある場合エラーが発生します。<br>
							反映ボタンを押した場合、入力された情報は上書きされるので注意してください。<br>
						</font>
						<br>
						<font size="3"><strong>未実装項目</strong></font><br>
						・検索条件の破棄タイミング<br>
						<br>
						時間が出来次第、実装します。<br>
						<br>
						追記(2019/08/26):<br>
						そもそもほぼ動かない<br>
						<br>
						追記(2019/08/27)<br>
						以前の挙動を再現し、若干の追加を行いました。<br>
						<br>
					</font>

				</div>
				<!-- 右側 -->
				<div class="col-4">
						<font size="2">サポート1</font>
						<br>
						<c:if test="${not empty addSup1.name}">
							<a href="ServantP_Servlet?id=${addSup1.id}" target="_blank">
								<font color="#4682b4"><strong>${addSup1.name}〔${addSup1.s_class.name} ★${addSup1.s_rare.rare}〕</strong></font>
							</a>
						</c:if>
						<br>
						<c:choose>
							<c:when test="${fn:length(addSup1.relation_skill) == 0&&!(addSup1 == null)}">
								スキルデータがありません<br>
							</c:when>
							<c:otherwise>
								<c:forEach var="rs" items="${addSup1.relation_skill}">
									<c:if test="${rs.junban == '1'||rs.junban == '3'||rs.junban == '5'}">
										スキル：${rs.skill.name } <input type="checkbox" name="s1skill" value="${rs.junban}">適用<br>
									</c:if>
								</c:forEach>
								<c:if test="${fn:length(addSup1.relation_skill) > 3}">
									<font size="2">強化後</font><br>
									<c:forEach var="rs" items="${addSup1.relation_skill}">
										<c:if test="${rs.junban == '2'||rs.junban == '4'||rs.junban == '6'}">
											スキル：${rs.skill.name } <input type="checkbox" name="s1skill" value="${rs.junban}">適用<br>
										</c:if>
									</c:forEach>
								</c:if>
								<c:if test="${fn:length(addSup1.relation_skill_other) > 0}">
									<font size="2">特殊1</font><br>
									<c:forEach var="rs" items="${addSup1.relation_skill_other}">
										<c:if test="${rs.junban == '7'||rs.junban == '9'||rs.junban == '11'}">
											スキル：${rs.skill.name } <input type="checkbox" name="s1skill" value="${rs.junban}">適用<br>
										</c:if>
									</c:forEach>
								</c:if>
							</c:otherwise>
						</c:choose>
						<br>
						<button type="submit" class="btn btn-danger btn-sm" name="AddJudge" value="resetSup1" form="list">リセット</button><br>
						<hr noshade>

						<font size="2">サポート2</font>
						<br>
						<c:if test="${not empty addSup2.name}">
							<a href="ServantP_Servlet?id=${addSup2.id}" target="_blank">
								<font color="#4682b4"><strong>${addSup2.name}〔${addSup2.s_class.name} ★${addSup2.s_rare.rare}〕</strong></font>
							</a>
						</c:if>
						<br>
						<c:choose>
							<c:when test="${fn:length(addSup2.relation_skill) == 0&&!(addSup2 == null)}">
								スキルデータがありません<br>
							</c:when>
							<c:otherwise>
								<c:forEach var="rs" items="${addSup2.relation_skill}">
									<c:if test="${rs.junban == '1'||rs.junban == '3'||rs.junban == '5'}">
										スキル：${rs.skill.name } <input type="checkbox" name="s2skill" value="${rs.junban}">適用<br>
									</c:if>
								</c:forEach>
								<c:if test="${fn:length(addSup2.relation_skill) > 3}">
									<font size="2">強化後</font><br>
									<c:forEach var="rs" items="${addSup2.relation_skill}">
										<c:if test="${rs.junban == '2'||rs.junban == '4'||rs.junban == '6'}">
											スキル：${rs.skill.name } <input type="checkbox" name="s2skill" value="${rs.junban}">適用<br>
										</c:if>
									</c:forEach>
								</c:if>
								<c:if test="${fn:length(addSup2.relation_skill_other) > 0}">
									<font size="2">特殊1</font><br>
									<c:forEach var="rs" items="${addSup2.relation_skill_other}">
										<c:if test="${rs.junban == '7'||rs.junban == '9'||rs.junban == '11'}">
											スキル：${rs.skill.name } <input type="checkbox" name="s2skill" value="${rs.junban}">適用<br>
										</c:if>
									</c:forEach>
								</c:if>
							</c:otherwise>
						</c:choose>
						<br>
						<button type="submit" class="btn btn-danger btn-sm" name="AddJudge" value="resetSup2" form="list">リセット</button><br>
						<hr noshade>

						<font size="2">サポート3</font>
						<br>
						<c:if test="${not empty addSup3.name}">
							<a href="ServantP_Servlet?id=${addSup3.id}" target="_blank">
								<font color="#4682b4"><strong>${addSup3.name}〔${addSup3.s_class.name} ★${addSup3.s_rare.rare}〕</strong></font>
							</a>
						</c:if>
						<br>
						<c:choose>
							<c:when test="${fn:length(addSup3.relation_skill) == 0&&!(addSup3 == null)}">
								スキルデータがありません<br>
							</c:when>
							<c:otherwise>
								<c:forEach var="rs" items="${addSup3.relation_skill}">
									<c:if test="${rs.junban == '1'||rs.junban == '3'||rs.junban == '5'}">
										スキル：${rs.skill.name } <input type="checkbox" name="s3skill" value="${rs.junban}">適用<br>
									</c:if>
								</c:forEach>
								<c:if test="${fn:length(addSup3.relation_skill) > 3}">
									<font size="2">強化後</font><br>
									<c:forEach var="rs" items="${addSup3.relation_skill}">
										<c:if test="${rs.junban == '2'||rs.junban == '4'||rs.junban == '6'}">
											スキル：${rs.skill.name } <input type="checkbox" name="s3skill" value="${rs.junban}">適用<br>
										</c:if>
									</c:forEach>
								</c:if>
								<c:if test="${fn:length(addSup3.relation_skill_other) > 0}">
									<font size="2">特殊1</font><br>
									<c:forEach var="rs" items="${addSup3.relation_skill_other}">
										<c:if test="${rs.junban == '7'||rs.junban == '9'||rs.junban == '11'}">
											スキル：${rs.skill.name } <input type="checkbox" name="s3skill" value="${rs.junban}">適用<br>
										</c:if>
									</c:forEach>
								</c:if>
							</c:otherwise>
						</c:choose>
						<br>
						<button type="submit" class="btn btn-danger btn-sm" name="AddJudge" value="resetSup3" form="list">リセット</button><br>
						<hr noshade>

						<font size="2">サポート4</font><br>
						<c:if test="${not empty addSup4.name}">
							<a href="ServantP_Servlet?id=${addSup4.id}" target="_blank">
								<font color="#4682b4"><strong>${addSup4.name}〔${addSup4.s_class.name} ★${addSup4.s_rare.rare}〕</strong></font>
							</a>
						</c:if>
						<br>
						<c:choose>
							<c:when test="${fn:length(addSup4.relation_skill) == 0&&!(addSup4 == null)}">
								スキルデータがありません<br>
							</c:when>
							<c:otherwise>
								<c:forEach var="rs" items="${addSup4.relation_skill}">
									<c:if test="${rs.junban == '1'||rs.junban == '3'||rs.junban == '5'}">
										スキル：${rs.skill.name } <input type="checkbox" name="s4skill" value="${rs.junban}">適用<br>
									</c:if>
								</c:forEach>
								<c:if test="${fn:length(addSup4.relation_skill) > 3}">
									<font size="2">強化後</font><br>
									<c:forEach var="rs" items="${addSup4.relation_skill}">
										<c:if test="${rs.junban == '2'||rs.junban == '4'||rs.junban == '6'}">
											スキル：${rs.skill.name } <input type="checkbox" name="s4skill" value="${rs.junban}">適用<br>
										</c:if>
									</c:forEach>
								</c:if>
								<c:if test="${fn:length(addSup4.relation_skill_other) > 0}">
									<font size="2">特殊1</font><br>
									<c:forEach var="rs" items="${addSup4.relation_skill_other}">
										<c:if test="${rs.junban == '7'||rs.junban == '9'||rs.junban == '11'}">
											スキル：${rs.skill.name } <input type="checkbox" name="s4skill" value="${rs.junban}">適用<br>
										</c:if>
									</c:forEach>
								</c:if>
							</c:otherwise>
						</c:choose>
						<br>
						<button type="submit" class="btn btn-danger btn-sm" name="AddJudge" value="resetSup4" form="list">リセット</button><br>
						<hr noshade>

						<font size="2">サポート5</font>
						<br>
						<c:if test="${not empty addSup5.name}">
							<a href="ServantP_Servlet?id=${addSup5.id}" target="_blank">
								<font color="#4682b4"><strong>${addSup5.name}〔${addSup5.s_class.name} ★${addSup5.s_rare.rare}〕</strong></font>
							</a>
						</c:if>
						<br>
						<c:choose>
							<c:when test="${fn:length(addSup5.relation_skill) == 0&&!(addSup5 == null)}">
								スキルデータがありません<br>
							</c:when>
							<c:otherwise>
								<c:forEach var="rs" items="${addSup5.relation_skill}">
									<c:if test="${rs.junban == '1'||rs.junban == '3'||rs.junban == '5'}">
										スキル：${rs.skill.name } <input type="checkbox" name="s5skill" value="${rs.junban}">適用<br>
									</c:if>
								</c:forEach>
								<c:if test="${fn:length(addSup5.relation_skill) > 3}">
									<font size="2">強化後</font><br>
									<c:forEach var="rs" items="${addSup5.relation_skill}">
										<c:if test="${rs.junban == '2'||rs.junban == '4'||rs.junban == '6'}">
											スキル：${rs.skill.name } <input type="checkbox" name="s5skill" value="${rs.junban}">適用<br>
										</c:if>
									</c:forEach>
								</c:if>
								<c:if test="${fn:length(addSup5.relation_skill_other) > 0}">
									<font size="2">特殊1</font><br>
									<c:forEach var="rs" items="${addSup5.relation_skill_other}">
										<c:if test="${rs.junban == '7'||rs.junban == '9'||rs.junban == '11'}">
											スキル：${rs.skill.name } <input type="checkbox" name="s5skill" value="${rs.junban}">適用<br>
										</c:if>
									</c:forEach>
								</c:if>
							</c:otherwise>
						</c:choose>
						<br>
						<button type="submit" class="btn btn-danger btn-sm" name="AddJudge" value="resetSup5" form="list">リセット</button><br>
						<hr noshade>
				</div>
			</div>
		</div>
		</form>
		<script>
			$("#calc").click(function(){
				console.log("計算開始");
				<!-- 基礎ATKの計算 -->
				var atk = parseInt($('[name = "atk"]').val());
				var fou = parseInt($('[name = "fou"]').val());
				var class_core = parseFloat($('[name = "class_core"]').val());
				var class_comp = parseFloat($('[name = "class_comp"]').val());
				var tenchijin = parseFloat($('[name = "tenchijin"]').val());
				var base_ATK = (atk + fou) * class_core * class_comp * tenchijin;
				console.log("基礎ATKは" + base_ATK);
				<!-- 基礎バフの計算 -->
				var atk_buff = parseFloat($('[name = "atk_buff"]').val());
				var def_debuff = parseFloat($('[name = "def_debuff"]').val());
				var ATK = (atk_buff + def_debuff)/100 + 1;
				var card_buff = parseFloat($('[name = "card_buff"]').val());
				var card_debuff = parseFloat($('[name = "card_debuff"]').val());
				var CARD = (card_buff + card_debuff)/100 + 1;
				var np_buff = parseFloat($('[name = "np_buff"]').val());
				var sp_skill = parseFloat($('[name = "sp_skill"]').val());
				var NPandSP = (np_buff + sp_skill)/100 + 1;
				var sp_chara = parseFloat($('[name = "sp_chara"]').val());
				var sp_state = parseFloat($('[name = "sp_state"]').val());
				var SPforNP = (sp_chara/100) * (sp_state/100);
				var base_BUFF = CARD * ATK * NPandSP * SPforNP;
				console.log("基礎バフは" + base_BUFF);
				<!-- 基礎倍率の計算 -->
				var color = parseFloat($('[name = "color"]').val());
				var np = parseFloat($('[name = "np"]').val());
				var base_NP = color * (np/100);
				console.log("基礎倍率は" + base_NP);
				<!-- プラスダメージの計算 -->
				var damage_buff = parseInt($('[name = "damage_buff"]').val());
				var damage_debuff = parseInt($('[name = "damage_debuff"]').val());
				var plus_damage = damage_buff + damage_debuff;
				console.log("プラスダメージは" + plus_damage);
				<!-- 結果の表示表示 -->
				var ave = base_ATK * base_BUFF * base_NP;
				console.log("計算は「"+ base_ATK +"×"+ base_BUFF +"×"+ base_NP +"="+ ave +"」");
				console.log(ave +"に乱数の上限（0.253）と下限(0.207)とそれの平均(0.23)を掛けた値に")
				console.log(plus_damage +"（プラスダメージ）を最後に足して終わりだよ")
				$('[name = "average"]').val(parseInt(ave * 0.23 + plus_damage));
				$('[name = "high"]').val(parseInt(ave * 0.253 + plus_damage));
				$('[name = "low"]').val(parseInt(ave * 0.207 + plus_damage));
				console.log("計算終了");
			});
		</script>
	</body>
</html>
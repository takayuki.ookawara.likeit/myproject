<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
		<script type="text/javascript" src="js/jquery.tablesorter.js"></script>
		<script type="text/javascript" src="js/FGO_DB.js"></script>
		<link rel="stylesheet" href="css/FGO_DB.css">
		<meta charset="UTF-8">
		<title>鯖リスト</title>
	</head>
	<body>
		<jsp:include page="header.jsp" />
		<!-- でかでかとタイトル -->
		<div class="container content">
			<div class="row">
				<div class="col">

				</div>
				<div class="col-8 text-center">
					<h1>サーヴァント検索</h1>
				</div>
				<div class="col">

				</div>
			</div>
			<br>
			<!-- ここにフォームとか -->
			<form action="ServantL_Servlet" method="post">
				<div class="row text-center">
					<div class="col-2">
						名前<br>
					</div>
					<div class="col-3">
						<input name="servant_name" class="form-control form-control-sm" type="text" placeholder="おなまえ"><br>
					</div>
					<div class="col-1">
						性別<br>
					</div>
					<div class="col-2">
						<select name="sex" class="form-control form-control-sm">
							<option value="">-</option>
								<c:forEach var="sex" items="${sexList}">
									<option value="${sex.id}">${sex.name}</option>
								</c:forEach>
						</select>
						<br>
					</div>
					<div class="col-1">
						レア<br>
					</div>
					<div class="col-1">
						<select name="rare" class="form-control form-control-sm">
							<option value="">-</option>
							<option value="0">0</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
						</select>
						<br>
					</div>
					<div class="col-1">
						ID<br>
					</div>
					<div class="col-1">
						<input name="servant_id" class="form-control form-control-sm" type="text" placeholder="ID"><br>
					</div>
				</div>
				<div class="row text-center">
					<div class="col-2">
						クラス<br>
					</div>
					<div class="col-2">
						<select name="s_class" class="form-control form-control-sm">
							<option value="">-</option>
								<c:forEach var="s_class" items="${classList}">
									<option value="${s_class.id}">${s_class.name}</option>
								</c:forEach>
						</select>
						<br>
					</div>
					<div class="col-2">
						入手方法<br>
					</div>
					<div class="col-2">
						<select name="gacha" class="form-control form-control-sm">
							<option value="">-</option>
							<option value="フレポ">フレポ</option>
							<option value="イベント">イベント</option>
							<option value="恒常">恒常</option>
							<option value="期間限定">期間限定</option>
							<option value="スト限">スト限</option>
						</select>
						<br>
					</div>
					<div class="col-4">

					</div>
				</div>
				<!-- ここ折り畳むマン -->
				<details>
					<summary>
						もっと入力したいならここを
					</summary>
					<br>
					<!-- ここから折り畳み -->
					<div class="row text-center">
						<div class="col">
							宝具・色
						</div>
						<div class="col">
							<select name="NPcolor" class="form-control form-control-sm">
								<option value="">-</option>
								<c:forEach var="color" items="${colorList}">
									<option value="${color.id}">${color.color}</option>
								</c:forEach>
							</select>
						</div>
						<div class="col">
							宝具・種類<br>
						</div>
						<div class="col">
							<select name="description" class="form-control form-control-sm">
								<option value="">-</option>
								<option value="単体">単体</option>
								<option value="全体">全体</option>
								<option value="補助">補助</option>
							</select>
							<br>
						</div>
						<div class="col">

						</div>
					</div>
					<div class="row text-center">
						<div class="col-1">
							枚数<br>
						</div>
						<div class="col-1">
							B<br>
						</div>
						<div class="col-1">
							<select name="Bcard" class="form-control form-control-sm">
								<option value="">-</option>
								<option value="AB">1</option>
								<option value="ABB">2</option>
								<option value="ABBB">3</option>
							</select>
							<br>
						</div>
						<div class="col-1">
							A<br>
						</div>
						<div class="col-1">
							<select name="Acard" class="form-control form-control-sm">
								<option value="">-</option>
								<option value="QAB">1</option>
								<option value="QAAB">2</option>
								<option value="QAAAB">3</option>
							</select>
							<br>
						</div>
						<div class="col-1">
							Q<br>
						</div>
						<div class="col-1">
							<select name="Qcard" class="form-control form-control-sm">
								<option value="">-</option>
								<option value="QA">1</option>
								<option value="QQA">2</option>
								<option value="QQQA">3</option>
							</select>
							<br>
						</div>
						<div class="col">
							<font size=2>
								※合計で5以下にしてください
							</font>
						</div>
					</div>
					<div class="row text-center">
						<div class="col">
							イラストレーター<br>
						</div>
						<div class="col">
							<input name="illustrator" class="form-control form-control-sm" type="text" placeholder="おなまえ"><br>
						</div>
						<div class="col">
							声優<br>
						</div>
						<div class="col">
							<input name="cv" class="form-control form-control-sm" type="text" placeholder="おなまえ"><br>
						</div>
					</div>
				</details>
				<!-- ここ、特性折り畳むマン -->
				<details>
					<summary>
						特性で検索したいならここを
					</summary>
					<br>
					<!-- 以下、チェックボックス地獄 -->
					<div class="row">
						<div class="col-2 text-center">
							<strong>方針</strong>
						</div>
						<div class="col">
							<c:forEach var="houshin" items="${houshinList}">
								<input type="checkbox" name="houshin" value="${houshin.id}">${houshin.name}　
							</c:forEach>
						</div>
					</div>
					<hr noshade>
					<div class="row">
						<div class="col-2 text-center">
							<strong>性格</strong>
						</div>
						<div class="col">
							<c:forEach var="seikaku" items="${seikakuList}">
								<input type="checkbox" name="seikaku" value="${seikaku.id}">${seikaku.name}　
							</c:forEach>
						</div>
					</div>
					<hr noshade>
					<div class="row">
						<div class="col-2 text-center">
							<strong>天地人</strong>
						</div>
						<div class="col">
							<c:forEach var="ten" items="${tenchijinList}">
								<input type="checkbox" name="tenchijin" value="${ten.id}">${ten.name}　
							</c:forEach>
						</div>
					</div>
					<hr noshade>
					<div class="row">
						<div class="col-2 text-center">
							<strong>特性</strong>
						</div>
						<div class="col">
							<c:forEach var="state" items="${stateList}">
								<c:choose>
									<c:when test="${state.id == '7'||state.id == '14'||state.id == '21'}">
										<input type="checkbox" name="chara" value="${state.id}">${state.state}<br>
									</c:when>
									<c:otherwise>
										<input type="checkbox" name="chara" value="${state.id}">${state.state}　
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</div>
					</div>
				</details>
				<br>
				<button type="submit" class="btn btn-secondary btn-lg btn-block">検索</button>
			</form>
			<div class="row">
				<div class="col">
					<font size="1">
						<strong>
							※一部キャラは特定の検索を行うと表示されなくなりますが、データが未入力なためであり、バグではありません。
						</strong>
					</font>
				</div>
				<div class="col text-right">
					${fn:length(servant_countList)}件中 ${fn:length(servantList)}件がヒットしました。
				</div>
			</div>
			<!-- ここから一覧 -->
			<font size="2">
				<table id="Sort_Table" class="table table-bordered table-sm table-hover">
					<thead class="thead-dark">
						<tr>
							<th scope="col">ID</th>
							<th scope="col">名前</th>
							<th scope="col">クラス</th>
							<th scope="col">Rare</th>
							<th scope="col">HP</th>
							<th scope="col">ATK</th>
							<th scope="col" class="text-white bg-danger">B</th>
							<th scope="col" class="text-white bg-primary">A</th>
							<th scope="col" class="text-white bg-success">Q</th>
							<th scope="col">宝具種類</th>
							<th scope="col">入手</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="servant" items="${servantList}">
							<tr>
								<td>${servant.id}</td>
								<th scope="row">
									<a href="ServantP_Servlet?id=${servant.id}">
										<font color="black">${servant.name}</font>
									</a>
									<a href="ServantP_Servlet?id=${servant.id}" target="_blank">
										<font color="#ffffff">別タブで開く</font>
									</a>
								</th>
								<td>${servant.s_class.name}</td>
								<td>${servant.s_rare.rare}</td>
								<td>${servant.hp}</td>
								<td>${servant.atk}</td>
								<td>${servant.b}</td>
								<td>${servant.a}</td>
								<td>${servant.q}</td>
								<c:choose>
									<c:when test="${servant.color_id >= '1'&&servant.relation_npONE1 == null}">
										<td>未入力</td>
									</c:when>
									<c:when test="${servant.color_id == '0'}">
										<td>no date</td>
									</c:when>
									<c:when test="${servant.color_id == '1'}">
										<td class="text-white bg-danger">${servant.relation_npONE1.noblePhantasm.description}</td>
									</c:when>
									<c:when test="${servant.color_id == '2'}">
										<td class="text-white bg-primary">${servant.relation_npONE1.noblePhantasm.description}</td>
									</c:when>
									<c:when test="${servant.color_id == '3'}">
										<td class="text-white bg-success">${servant.relation_npONE1.noblePhantasm.description}</td>
									</c:when>
								</c:choose>
								<td>${servant.s_rare.gacha}</td>
							</tr>
						</c:forEach>
					</tbody>
		    	</table>
	    	</font>
		</div>
	</body>
</html>